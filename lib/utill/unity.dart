// ignore_for_file: deprecated_member_use

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class App {
  static double width(BuildContext context) {
    return MediaQuery.of(context).size.width / 100;
  }

  static double height(BuildContext context) {
    return MediaQuery.of(context).size.height / 100;
  }

  static void launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static String getDayByNumber(int number) {
    switch (number) {
      case 1:
        return 'monday';
        break;
      case 2:
        return 'tuesday';
        break;
      case 3:
        return 'wednesday';
        break;
      case 4:
        return 'thursday';
        break;
      case 5:
        return 'friday';
        break;
      case 6:
        return 'saturday';
        break;
      default:
        return 'sunday';
    }
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

Widget caNoResults(
    BuildContext context, String title, Color color, double fontSize) {
  return Container(
    padding: EdgeInsets.all(20),
    child: Center(
      child: Text(
        title,
        style: TextStyle(color: color, fontSize: fontSize, fontFamily: ""),
        textAlign: TextAlign.center,
      ),
    ),
  );
}
