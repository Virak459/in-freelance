import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleSignInProvider with ChangeNotifier {
  GoogleSignIn _googleSignIn = GoogleSignIn();
  GoogleSignInAccount googleAccount;
  GoogleSignInAuthentication auth;

  Future<void> login() async {
    try {
      this.googleAccount = await _googleSignIn.signIn();
      auth = await googleAccount.authentication;
    } catch (e) {
      print('_googleSignInError $e');
    }
    notifyListeners();
  }

  logout() async {
    this.googleAccount = await _googleSignIn.signOut();
    notifyListeners();
  }
}
