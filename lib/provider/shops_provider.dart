import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/base/api_response.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/category.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/schedule_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/repository/shops_repo.dart';
import 'package:flutter_sixvalley_ecommerce/helper/api_checker.dart';

class ShopsProvider extends ChangeNotifier {
  final ShopsRepo shopsRepo;

  ShopsProvider({@required this.shopsRepo});

  ShopModel _shopModel;
  ShopModel get shop => _shopModel;

  List<ShopModel> _tempShopList = [];
  int _topSellerSelectedIndex;
  List<ShopModel> _shopList = [];
  List<ShopModel> get shopList => _shopList;

  List<Category> _shopCategoryList = [];
  List<Category> get shopCategoryList => _shopCategoryList;

  List<ScheduleModel> _shopScheduleList = [];
  List<ScheduleModel> get shopScheduleList => _shopScheduleList;

  List<Product> _shopCategoryProductList = [];
  List<Product> get shopCategoryProductList => _shopCategoryProductList;

  List<ShopModel> get tempShopList => _tempShopList;
  int get topSellerSelectedIndex => _topSellerSelectedIndex;

  Future<void> getShopsList(bool reload, BuildContext context) async {
    if (_shopList.length == 0 || reload) {
      ApiResponse apiResponse = await shopsRepo.getAllShops();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _tempShopList.clear();
        _shopList.clear();
        apiResponse.response.data['data'].forEach((category) {
          _shopList.add(ShopModel.fromJson(category));
        });
        _tempShopList = _shopList;
        _topSellerSelectedIndex = 0;
      } else {
        ApiChecker.checkApi(context, apiResponse);
      }
      notifyListeners();
    }
  }

  void changeSelectedIndex(int selectedIndex) {
    _topSellerSelectedIndex = selectedIndex;
    notifyListeners();
  }

  void filterData(String search) {
    print(_shopList.length);
    _tempShopList.clear();
    if (search.isEmpty) {
      _tempShopList = _shopList;
    } else {
      // ignore: non_constant_identifier_names
      _shopList.forEach((ShopModel ShopModel) {
        if (ShopModel.name.toLowerCase().contains(search.toLowerCase())) {
          _tempShopList.add(ShopModel);
        }
      });
    }

    notifyListeners();
  }

  void setShop(ShopModel shopModel) {
    _shopModel = shopModel;
    //notifyListeners();
  }

  Future<void> getShopCategoryProductList(
      String categoryId, String sellerId, BuildContext context) async {
    ApiResponse apiResponse =
        await shopsRepo.getShopCategoryProducts(categoryId, sellerId);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _shopCategoryProductList.clear();

      apiResponse.response.data['products'].forEach(
        (category) => _shopCategoryProductList.add(
          Product.fromJson(category),
        ),
      );
    } else {
      ApiChecker.checkApi(context, apiResponse);
    }
    notifyListeners();
  }

  Future<void> getShop(BuildContext context) async {
    ApiResponse apiResponse = await shopsRepo.getShop(_shopModel.id.toString());
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _shopModel = ShopModel.fromJson(apiResponse.response.data['data']);
      _shopCategoryList.clear();
      _shopScheduleList.clear();

      apiResponse.response.data['data']['schedules'].forEach(
        (schedule) => _shopScheduleList.add(
          ScheduleModel.fromJson(schedule),
        ),
      );
      apiResponse.response.data['categories'].forEach(
        (category) => _shopCategoryList.add(
          Category.fromJson(category),
        ),
      );
    } else {
      ApiChecker.checkApi(context, apiResponse);
    }
    notifyListeners();
  }
}
