import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/base/api_response.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/flash_deal_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/repository/flash_deal_repo.dart';
import 'package:flutter_sixvalley_ecommerce/helper/api_checker.dart';

class FlashDealProvider extends ChangeNotifier {
  final FlashDealRepo megaDealRepo;
  FlashDealProvider({@required this.megaDealRepo});

  FlashDealModel _flashDeal;
  List<FlashDealModel> _flashDeals = [];
  List<Product> _flashDealProductList = [];
  List<FlashDealModel> get flashDeals => _flashDeals;
  List<Product> get flashDealProductList => _flashDealProductList;

  Future<void> getMegaDealList(bool reload, BuildContext context) async {
    if (reload) {
      ApiResponse apiResponse = await megaDealRepo.getFlashDeal();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _flashDeals.clear();
        print('_flashDeals ${_flashDeals.length}');
        apiResponse.response.data.forEach((flashDeal) {
          _flashDeals.add(FlashDealModel.fromJson(flashDeal));
        });
      } else {
        ApiChecker.checkApi(context, apiResponse);
      }
      notifyListeners();
    }
  }

  void setFlashDeal(FlashDealModel flashDealModel) {
    _flashDeal = flashDealModel;
    notifyListeners();
  }

  Future<void> getFlashDealProductList(
      bool reload, BuildContext context) async {
    if (reload) {
      ApiResponse apiResponse =
          await megaDealRepo.getFlashDealProductList(_flashDeal.id.toString());
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _flashDealProductList.clear();

        apiResponse.response.data['products'].forEach((product) {
          _flashDealProductList.add(Product.fromJson(product));
        });
      } else {
        ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
