class SellerModel {
  int _id;
  String _fName;
  String _lName;
  String _phone;
  String _image;
  String _productOrderType;
  String _mapLink;
  Shop _shop;

  SellerModel(int id, String fName, String lName, String phone, String image,
      String productOrderType, String mapLink, Shop shop) {
    this._id = id;
    this._mapLink = mapLink;
    this._fName = fName;
    this._lName = lName;
    this._phone = phone;
    this._image = image;
    this._shop = shop;
    this._productOrderType = productOrderType;
  }

  int get id => _id;
  String get fName => _fName;
  String get mapLink => _mapLink;
  String get lName => _lName;
  String get phone => _phone;
  String get image => _image;
  String get productOrderType => _productOrderType;
  // ignore: unnecessary_getters_setters
  Shop get shop => _shop;
  // ignore: unnecessary_getters_setters
  set shop(Shop value) {
    _shop = value;
  }

  SellerModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _fName = json['f_name'];
    _lName = json['l_name'];
    _phone = json['phone'];
    _image = json['image'];
    _mapLink = json['google_map_location_link'];
    _productOrderType = json['product_order_type'];
    _shop = json['shop'] != null ? new Shop.fromJson(json['shop']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['f_name'] = this._fName;
    data['l_name'] = this._lName;
    data['phone'] = this._phone;
    data['google_map_location_link'] = this._mapLink;
    data['image'] = this._image;
    if (this._shop != null) {
      data['shop'] = this._shop.toJson();
    }
    return data;
  }
}

class Shop {
  int _id;
  String _name;
  String _address;
  String _contact;
  String _image;
  String _banner;
  String _createdAt;
  String _updatedAt;
  String _open;

  Shop({
    int id,
    String name,
    String address,
    String contact,
    String image,
    String banner,
    String productOrderType,
    String createdAt,
    String updatedAt,
    String open,
  }) {
    this._id = id;
    this._name = name;
    this._address = address;
    this._contact = contact;
    this._image = image;
    this._banner = banner;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  String get name => _name;
  String get address => _address;
  String get contact => _contact;
  String get image => _image;
  String get banner => _banner;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get open => _open;

  Shop.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _address = json['address'];
    _contact = json['contact'];
    _image = json['image'];
    _banner = json['banner'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _open = json['open'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['address'] = this._address;
    data['contact'] = this._contact;
    data['image'] = this._image;
    data['banner'] = this._banner;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['open'] = this._open;
    return data;
  }
}
