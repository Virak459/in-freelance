class ScheduleModel {
  int id;
  int isOpen;
  int day;
  int store_id; //old String
  String openingTime;
  String closingTime;

  ScheduleModel(
      {this.id,
      this.isOpen,
      this.day,
      this.store_id,
      this.openingTime,
      this.closingTime});

  ScheduleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 0;
    isOpen = json['open'] ?? 0;
    day = json['day'] ?? 0;
    store_id = json['store_id'] ?? 0;
    openingTime = json['opening_time'] ?? 'null';
    closingTime = json['closing_time'] ?? 'null';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id ?? 0;
    data['day'] = this.day ?? 0;
    data['store_id'] = this.store_id ?? "";
    data['open'] = this.isOpen ?? 0;
    data['opening_time'] = this.openingTime ?? "";
    data['closing_time'] = this.closingTime ?? "";
    return data;
  }
}
