class BecomeSellerPlanModel {
  int id;

  String name;
  int price;
  String description;
  String createdAt;
  String updatedAt;

  BecomeSellerPlanModel(
      {this.id,
      this.name,
      this.price,
      this.description,
      this.createdAt,
      this.updatedAt});

  BecomeSellerPlanModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['description'] = this.description;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
