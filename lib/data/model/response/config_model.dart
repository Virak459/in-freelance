// ignore_for_file: unnecessary_statements, duplicate_ignore

class ConfigModel {
  int _systemDefaultCurrency;
  bool _digitalPayment;
  bool _cashOnDelivery;
  BaseUrls _baseUrls;
  StaticUrls _staticUrls;
  String _aboutUs;
  String _privacyPolicy;
  List<Faq> _faq;
  String _termsConditions;
  List<CurrencyList> _currencyList;
  String _currencySymbolPosition;
  bool _maintenanceMode;
  List<String> _language;
  List<ColorsModal> _colors;
  List<String> _unit;
  String _shippingMethod;
  bool _shippingMethodEnable;
  String _currencyModel;
  bool _emailVerification;
  bool _phoneVerification;
  String _countryCode;
  List<SocialLogin> _socialLogin;
  String _forgetPasswordVerification;
  Pusher _pusher;
  String _androidVersion;
  String _iosVersion;
  String _shopTitile;

  String _androidUrl;
  String _iosUrl;
  bool _allowAppUpdate;
  bool _allowDeleteAccount;
  bool _showProductOnHomeMobile;
  bool _showProductFeaturedOnHomeMobile;
  bool _showProductAdminOnHomeMobile;
  int _showNotificationStyle;
  int _showShopStyle;
  bool _showBrandOnHomeMobile;

  ConfigModel({
    int systemDefaultCurrency,
    bool digitalPayment,
    bool cashOnDelivery,
    BaseUrls baseUrls,
    StaticUrls staticUrls,
    String aboutUs,
    String privacyPolicy,
    List<Faq> faq,
    String termsConditions,
    List<CurrencyList> currencyList,
    String currencySymbolPosition,
    bool maintenanceMode,
    List<String> language,
    List<ColorsModal> colors,
    List<String> unit,
    String shippingMethod,
    bool shippingMethodEnable,
    String currencyModel,
    bool emailVerification,
    bool phoneVerification,
    String countryCode,
    List<SocialLogin> socialLogin,
    String forgetPasswordVerification,
    Pusher pusher,
    String androidVersion,
    String iosVersion,
    String androidUrl,
    String shopTitle,
    String iosUrl,
    bool allowAppUpdate,
    bool allowDeleteAccount,
    bool showProductOnHomeMobile,
    bool showProductFeaturedOnHomeMobile,
    bool showProductAdminOnHomeMobile,
    int showNotificationStyle,
    int showShopStyle,
    int showBrandOnHomeMobile,
  }) {
    this._systemDefaultCurrency = systemDefaultCurrency;
    this._digitalPayment = digitalPayment;
    this._cashOnDelivery = cashOnDelivery;
    this._baseUrls = baseUrls;
    this._staticUrls = staticUrls;
    this._aboutUs = aboutUs;
    this._privacyPolicy = privacyPolicy;
    this._faq = faq;
    this._termsConditions = termsConditions;
    this._currencyList = currencyList;
    this._currencySymbolPosition = currencySymbolPosition;
    this._maintenanceMode = maintenanceMode;
    this._language = language;
    this._colors = colors;
    this._unit = unit;
    this._shippingMethod = shippingMethod;
    this._shippingMethodEnable = shippingMethodEnable;
    this._currencyModel = currencyModel;
    this._emailVerification = emailVerification;
    this._phoneVerification = phoneVerification;
    this._countryCode = countryCode;
    this._socialLogin = socialLogin;
    this._forgetPasswordVerification = forgetPasswordVerification;
    this._pusher = pusher;
    // ignore: unnecessary_statements
    this._androidVersion;
    // ignore: unnecessary_statements
    this._iosVersion;
    // ignore: unnecessary_statements
    this._androidUrl;
    // ignore: unnecessary_statements
    this._iosUrl;
    // ignore: unnecessary_statements
    this._allowAppUpdate;
    // ignore: unnecessary_statements
    this._shopTitile;
    // ignore: unnecessary_statements
    this._allowDeleteAccount;
    // ignore: unnecessary_statements
    this._showProductOnHomeMobile;
    // ignore: unnecessary_statements
    this._showProductFeaturedOnHomeMobile;
    // ignore: unnecessary_statements
    this._showProductAdminOnHomeMobile;
    this._showNotificationStyle;
    this._showShopStyle;
    this._showBrandOnHomeMobile;
  }

  int get systemDefaultCurrency => _systemDefaultCurrency;
  bool get digitalPayment => _digitalPayment;
  bool get cashOnDelivery => _cashOnDelivery;
  BaseUrls get baseUrls => _baseUrls;
  StaticUrls get staticUrls => _staticUrls;
  String get aboutUs => _aboutUs;
  String get privacyPolicy => _privacyPolicy;
  List<Faq> get faq => _faq;
  String get termsConditions => _termsConditions;
  List<CurrencyList> get currencyList => _currencyList;
  String get currencySymbolPosition => _currencySymbolPosition;
  bool get maintenanceMode => _maintenanceMode;
  List<String> get language => _language;
  List<ColorsModal> get colors => _colors;
  List<String> get unit => _unit;
  String get shippingMethod => _shippingMethod;
  bool get shippingMethodEnable => _shippingMethodEnable ?? true;
  String get currencyModel => _currencyModel;
  bool get emailVerification => _emailVerification;
  bool get phoneVerification => _phoneVerification;
  String get countryCode => _countryCode;
  List<SocialLogin> get socialLogin => _socialLogin;
  String get forgetPasswordVerification => _forgetPasswordVerification;
  Pusher get pusher => _pusher;
  String get androidVersion => _androidVersion;
  String get shopTitle => _shopTitile;
  String get iosVersion => _iosVersion;
  String get androidUrl => _androidUrl;
  String get iosUrl => _iosUrl;
  bool get allowAppUpdate => _allowAppUpdate ?? false;
  bool get allowDeleteAccount => _allowDeleteAccount ?? false;
  bool get showProductOnHomeMobile => _showProductOnHomeMobile;
  bool get showProductFeaturedOnHomeMobile => _showProductFeaturedOnHomeMobile;
  bool get showProductAdminOnHomeMobile => _showProductAdminOnHomeMobile;
  int get showNotificationStyle => _showNotificationStyle;
  int get showShopStyle => _showShopStyle;
  bool get showBrandOnHomeMobile => _showBrandOnHomeMobile;

  ConfigModel.fromJson(Map<String, dynamic> json) {
    _systemDefaultCurrency = json['system_default_currency'];
    _digitalPayment = json['digital_payment'];
    _cashOnDelivery = json['cash_on_delivery'];
    _baseUrls = json['base_urls'] != null
        ? new BaseUrls.fromJson(json['base_urls'])
        : null;
    _staticUrls = json['static_urls'] != null
        ? new StaticUrls.fromJson(json['static_urls'])
        : null;
    _aboutUs = json['about_us'];
    _privacyPolicy = json['privacy_policy'];
    if (json['faq'] != null) {
      _faq = [];
      json['faq'].forEach((v) {
        _faq.add(new Faq.fromJson(v));
      });
    }
    _termsConditions = json['terms_&_conditions'];
    if (json['currency_list'] != null) {
      _currencyList = [];
      json['currency_list'].forEach((v) {
        _currencyList.add(new CurrencyList.fromJson(v));
      });
    }
    _currencySymbolPosition = json['currency_symbol_position'];
    _maintenanceMode = json['maintenance_mode'];
    _language = json['language'].cast<String>();
    if (json['colors'] != null) {
      _colors = [];
      json['colors'].forEach((v) {
        _colors.add(new ColorsModal.fromJson(v));
      });
    }
    _shopTitile = json['all_shop_text'];
    _unit = json['unit'].cast<String>();
    _shippingMethod = json['shipping_method'];
    _shippingMethodEnable = json['shipping_method_enable'];
    _currencyModel = json['currency_model'];
    _emailVerification = json['email_verification'];
    _phoneVerification = json['phone_verification'];
    _countryCode = json['country_code'];
    if (json['social_login'] != null) {
      _socialLogin = [];
      json['social_login'].forEach((v) {
        _socialLogin.add(new SocialLogin.fromJson(v));
      });
    }
    _forgetPasswordVerification = json['forgot_password_verification'];
    _pusher = Pusher.fromJson(json['pusher']);
    _androidVersion = json['android_version'];
    _iosVersion = json['ios_version'];
    _androidUrl = json['android_url'];
    _iosUrl = json['ios_url'];
    _allowAppUpdate = json['allow_app_update'];
    _allowDeleteAccount = json['allow_delete_account'];
    _showProductOnHomeMobile = json['show_product_on_home_mobile'];
    _showProductFeaturedOnHomeMobile =
        json['show_product_featured_on_home_mobile'];
    _showProductAdminOnHomeMobile = json['show_product_admin_on_home_mobile'];
    _showNotificationStyle = json['show_notification_style'];
    _showShopStyle = json['show_shop_style'];
    _showBrandOnHomeMobile = json['show_brand_on_home_mobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['system_default_currency'] = this._systemDefaultCurrency;
    data['digital_payment'] = this._digitalPayment;
    data['cash_on_delivery'] = this._cashOnDelivery;

    if (this._baseUrls != null) {
      data['base_urls'] = this._baseUrls.toJson();
    }
    if (this._staticUrls != null) {
      data['static_urls'] = this._staticUrls.toJson();
    }
    data['about_us'] = this._aboutUs;
    data['all_shop_text'] = this._shopTitile;
    data['privacy_policy'] = this._privacyPolicy;
    if (this._faq != null) {
      data['faq'] = this._faq.map((v) => v.toJson()).toList();
    }
    data['terms_&_conditions'] = this._termsConditions;
    if (this._currencyList != null) {
      data['currency_list'] =
          this._currencyList.map((v) => v.toJson()).toList();
    }
    data['currency_symbol_position'] = this._currencySymbolPosition;
    data['maintenance_mode'] = this._maintenanceMode;
    data['language'] = this._language;
    if (this._colors != null) {
      data['colors'] = this._colors.map((v) => v.toJson()).toList();
    }
    data['unit'] = this._unit;
    data['shipping_method'] = this._shippingMethod;
    data['shipping_method_enable'] = this._shippingMethodEnable;
    data['currency_model'] = this._currencyModel;
    data['email_verification'] = this._emailVerification;
    data['phone_verification'] = this._phoneVerification;
    data['country_code'] = this._countryCode;
    if (this._socialLogin != null) {
      data['social_login'] = this._socialLogin.map((v) => v.toJson()).toList();
    }
    data['forgot_password_verification'] = this._forgetPasswordVerification;
    data['pusher'] = this._pusher;
    data['android_version'] = this.androidVersion;
    data['ios_version'] = this.iosVersion;
    data['android_url'] = this.androidUrl;
    data['ios_url'] = this.iosUrl;
    data['allow_app_update'] = this._allowAppUpdate;
    data['allow_delete_account'] = this.allowDeleteAccount;
    data['show_product_on_home_mobile'] = this.showProductOnHomeMobile;
    data['show_product_featured_on_home_mobile'] =
        this._showProductFeaturedOnHomeMobile;
    data['show_product_admin_on_home_mobile'] =
        this._showProductAdminOnHomeMobile;
    data['show_brand_on_home_mobile'] = this._showBrandOnHomeMobile;
    data['show_notification_style'] = this._showNotificationStyle;
    data['show_shop_style'] = this._showShopStyle;
    return data;
  }
}

class BaseUrls {
  String _productImageUrl;
  String _productThumbnailUrl;
  String _brandImageUrl;
  String _customerImageUrl;
  String _bannerImageUrl;
  String _categoryImageUrl;
  String _reviewImageUrl;
  String _sellerImageUrl;
  String _shopImageUrl;
  String _notificationImageUrl;

  BaseUrls(
      {String productImageUrl,
      String productThumbnailUrl,
      String brandImageUrl,
      String customerImageUrl,
      String bannerImageUrl,
      String categoryImageUrl,
      String reviewImageUrl,
      String sellerImageUrl,
      String shopImageUrl,
      String notificationImageUrl}) {
    this._productImageUrl = productImageUrl;
    this._productThumbnailUrl = productThumbnailUrl;
    this._brandImageUrl = brandImageUrl;
    this._customerImageUrl = customerImageUrl;
    this._bannerImageUrl = bannerImageUrl;
    this._categoryImageUrl = categoryImageUrl;
    this._reviewImageUrl = reviewImageUrl;
    this._sellerImageUrl = sellerImageUrl;
    this._shopImageUrl = shopImageUrl;
    this._notificationImageUrl = notificationImageUrl;
  }

  String get productImageUrl => _productImageUrl;
  String get productThumbnailUrl => _productThumbnailUrl;
  String get brandImageUrl => _brandImageUrl;
  String get customerImageUrl => _customerImageUrl;
  String get bannerImageUrl => _bannerImageUrl;
  String get categoryImageUrl => _categoryImageUrl;
  String get reviewImageUrl => _reviewImageUrl;
  String get sellerImageUrl => _sellerImageUrl;
  String get shopImageUrl => _shopImageUrl;
  String get notificationImageUrl => _notificationImageUrl;

  BaseUrls.fromJson(Map<String, dynamic> json) {
    _productImageUrl = json['product_image_url'];
    _productThumbnailUrl = json['product_thumbnail_url'];
    _brandImageUrl = json['brand_image_url'];
    _customerImageUrl = json['customer_image_url'];
    _bannerImageUrl = json['banner_image_url'];
    _categoryImageUrl = json['category_image_url'];
    _reviewImageUrl = json['review_image_url'];
    _sellerImageUrl = json['seller_image_url'];
    _shopImageUrl = json['shop_image_url'];
    _notificationImageUrl = json['notification_image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_image_url'] = this._productImageUrl;
    data['product_thumbnail_url'] = this._productThumbnailUrl;
    data['brand_image_url'] = this._brandImageUrl;
    data['customer_image_url'] = this._customerImageUrl;
    data['banner_image_url'] = this._bannerImageUrl;
    data['category_image_url'] = this._categoryImageUrl;
    data['review_image_url'] = this._reviewImageUrl;
    data['seller_image_url'] = this._sellerImageUrl;
    data['shop_image_url'] = this._shopImageUrl;
    data['notification_image_url'] = this._notificationImageUrl;
    return data;
  }
}

class StaticUrls {
  String _contactUs;
  String _brands;
  String _categories;
  String _customerAccount;

  StaticUrls(
      {String contactUs,
      String brands,
      String categories,
      String customerAccount}) {
    this._contactUs = contactUs;
    this._brands = brands;
    this._categories = categories;
    this._customerAccount = customerAccount;
  }

  String get contactUs => _contactUs;
  String get brands => _brands;
  String get categories => _categories;
  String get customerAccount => _customerAccount;

  StaticUrls.fromJson(Map<String, dynamic> json) {
    _contactUs = json['contact_us'];
    _brands = json['brands'];
    _categories = json['categories'];
    _customerAccount = json['customer_account'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['contact_us'] = this._contactUs;
    data['brands'] = this._brands;
    data['categories'] = this._categories;
    data['customer_account'] = this._customerAccount;
    return data;
  }
}

class SocialLogin {
  String _loginMedium;
  bool _status;

  SocialLogin({String loginMedium, bool status}) {
    this._loginMedium = loginMedium;
    this._status = status;
  }

  String get loginMedium => _loginMedium;
  bool get status => _status;

  SocialLogin.fromJson(Map<String, dynamic> json) {
    _loginMedium = json['login_medium'];
    _status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login_medium'] = this._loginMedium;
    data['status'] = this._status;
    return data;
  }
}

class Faq {
  int _id;
  String _question;
  String _answer;
  int _ranking;
  int _status;
  String _createdAt;
  String _updatedAt;

  Faq(
      {int id,
      String question,
      String answer,
      int ranking,
      int status,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._question = question;
    this._answer = answer;
    this._ranking = ranking;
    this._status = status;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  String get question => _question;
  String get answer => _answer;
  int get ranking => _ranking;
  int get status => _status;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  Faq.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _question = json['question'];
    _answer = json['answer'];
    _ranking = json['ranking'];
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['question'] = this._question;
    data['answer'] = this._answer;
    data['ranking'] = this._ranking;
    data['status'] = this._status;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}

class CurrencyList {
  int _id;
  String _name;
  String _symbol;
  String _code;
  double _exchangeRate;
  int _status;
  String _createdAt;
  String _updatedAt;

  CurrencyList(
      {int id,
      String name,
      String symbol,
      String code,
      double exchangeRate,
      int status,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._name = name;
    this._symbol = symbol;
    this._code = code;
    this._exchangeRate = exchangeRate;
    this._status = status;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  String get name => _name;
  String get symbol => _symbol;
  String get code => _code;
  double get exchangeRate => _exchangeRate;
  int get status => _status;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  CurrencyList.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _symbol = json['symbol'];
    _code = json['code'];
    _exchangeRate = json['exchange_rate'].toDouble();
    _status = json['status'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['symbol'] = this._symbol;
    data['code'] = this._code;
    data['exchange_rate'] = this._exchangeRate;
    data['status'] = this._status;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}

class Pusher {
  String _key;
  String _secret;
  String _appId;
  String _cluster;

  Pusher({String key, String secret, String appId, String cluster}) {
    this._key = key;
    this._secret = secret;
    this._appId = appId;
    this._cluster = cluster;
  }

  String get key => _key;
  String get secret => _secret;
  String get appId => _appId;
  String get cluster => _cluster;

  Pusher.fromJson(Map<String, dynamic> json) {
    _key = json['key'];
    _secret = json['secret'];
    _appId = json['app_id'];
    _cluster = json['cluster'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this._key;
    data['secret'] = this._secret;
    data['app_id'] = this._appId;
    data['cluster'] = this._cluster;

    return data;
  }
}

class ColorsModal {
  int _id;
  String _name;
  String _code;
  String _createdAt;
  String _updatedAt;

  ColorsModal(
      {int id, String name, String code, String createdAt, String updatedAt}) {
    this._id = id;
    this._name = name;
    this._code = code;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  String get name => _name;
  String get code => _code;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  ColorsModal.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _code = json['code'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['code'] = this._code;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
