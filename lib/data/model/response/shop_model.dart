import 'package:flutter_sixvalley_ecommerce/data/model/response/schedule_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/seller_model.dart';

class ShopModel {
  int _id;
  String _name;
  String _address;
  String _contact;
  String _image;
  String _createdAt;
  String _updatedAt;
  String _banner;
  double _avgReview;
  int _totalReview;
  int _totalOrder;
  String _open;
  bool _active;
  String _about;
  SellerModel _seller;
  List<ScheduleModel> _schedules;

  ShopModel(
      {int id,
      String name,
      String address,
      String about,
      String open,
      bool active,
      double avgReview,
      int totalReview,
      int totalOrder,
      String contact,
      String image,
      String createdAt,
      String updatedAt,
      SellerModel seller,
      List<ScheduleModel> schedules,
      String banner}) {
    this._id = id;
    this._name = name;
    this._open = open;
    this._avgReview = avgReview;
    this._totalReview = totalReview;
    this._totalOrder = totalOrder;
    this._about = about;
    this._active = active ?? true;
    this._address = address;
    this._contact = contact;
    this._image = image;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._seller = seller;
    this._banner = banner;
    this._schedules = schedules;
  }

  int get id => _id;
  String get open => _open;
  String get name => _name;
  String get about => _about;
  bool get active => _active ?? true;
  String get address => _address;
  String get contact => _contact;
  String get image => _image;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  SellerModel get seller => _seller;
  double get avgReview => _avgReview;
  int get totalReview => _totalReview;
  int get totalOrder => _totalOrder;
  List<ScheduleModel> get schedules => _schedules;
  String get banner => _banner;

  ShopModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _open = json['open'];
    _about = json['about'];
    _active = json['active'] ?? true;
    _address = json['address'];
    _contact = json['contact'];
    _avgReview =
        json['avg_review'] != null ? double.parse(json['avg_review']) : 0.0;
    _totalReview = json['total_review'];
    _totalOrder = json['total_order'];
    _image = json['image'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _banner = json['banner'];
    _seller = SellerModel.fromJson(json['seller']);
    _schedules = [];
    if (json['schedules'] != null) {
      json['schedules'].forEach(
        (schedule) => _schedules.add(
          ScheduleModel.fromJson(schedule),
        ),
      );
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['open'] = this._open;
    data['about'] = this._about;
    data['avg_review'] = this._avgReview;
    data['total_review'] = this._totalReview;
    data['total_order'] = this._totalOrder;
    data['address'] = this._address;
    data['contact'] = this._contact;
    data['image'] = this._image;
    data['active'] = this._active;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['banner'] = this._banner;
    data['seller'] = this._seller.toJson();

    return data;
  }
}
