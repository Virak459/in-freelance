import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/datasource/remote/dio/dio_client.dart';
import 'package:flutter_sixvalley_ecommerce/data/datasource/remote/exception/api_error_handler.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/base/api_response.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';

class ShopsRepo {
  final DioClient dioClient;
  ShopsRepo({@required this.dioClient});

  Future<ApiResponse> getAllShops() async {
    try {
      final response = await dioClient.get(AppConstants.SHOPS);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getShop(String shopId,) async {
    try {
      final response = await dioClient.get(AppConstants.SHOP_INFO+shopId);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getShopCategory(String id) async {
    try {
      final response = await dioClient.get(AppConstants.SHOP_INFO + id);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getShopCategoryProducts(
      String categoryId, String sellerId) async {
    try {
      Map<String, String> _fields = {
        'category_id': categoryId,
        'seller_id': sellerId,
      };
      final response = await dioClient.get(AppConstants.SHOP_CATEGORY_PRODUCTS,
          queryParameters: _fields);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
