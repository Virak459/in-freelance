import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/notification_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/provider/chat_provider.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/shop_chat_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/notification/notification_detail_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/order_details_screen.dart';
import 'package:flutter_sixvalley_ecommerce/main.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/splash/splash_screen.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class MyNotification {
  static Future<void> initialize(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin) async {
    var androidInitialize =
        new AndroidInitializationSettings('notification_icon');
    var iOSInitialize = new IOSInitializationSettings();
    var initializationsSettings = new InitializationSettings(
        android: androidInitialize, iOS: iOSInitialize);
    flutterLocalNotificationsPlugin.initialize(initializationsSettings,
        onSelectNotification: (String payload) async {
      try {
        if (payload != null) {
          var message = jsonDecode(payload);
          switch (message['type']) {
            case 'order':
              MyApp.navigatorKey.currentState.push(
                MaterialPageRoute(
                  builder: (context) => OrderDetailsScreen(
                    orderModel: null,
                    orderId: int.parse(message['id'].toString()),
                  ),
                ),
              );
              break;
            case 'chat':
              int shopId = Provider.of<ChatProvider>(
                      MyApp.navigatorKey.currentState.context,
                      listen: false)
                  .shopId;
              if (shopId != int.parse(message['id'].toString())) {}
              MyApp.navigatorKey.currentState.push(
                MaterialPageRoute(
                  builder: (context) => ShopChatScreen(
                    shopModel: ShopModel(
                      id: int.parse(message['id'].toString()),
                      name: message['shop_name'],
                      image: message['shop_image'],
                    ),
                  ),
                ),
              );

              break;
            case 'notification':
              MyApp.navigatorKey.currentState.push(
                MaterialPageRoute(
                  builder: (context) => NotificationDetailScreen(
                    notificationModel: NotificationModel(
                      id: int.parse(message['id'].toString()),
                      title: message['title'],
                      image: message['image'],
                      description: message['body'],
                    ),
                  ),
                ),
              );
              break;
            default:
          }
        }
      } catch (e) {}
      return;
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print(
          "onMessage: ${message.notification.title}/${message.notification.body}/${message.notification.titleLocKey}");

      showNotification(message, flutterLocalNotificationsPlugin, true);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print(
          "onOpenApp: ${message.notification.title}/${message.notification.body}/${message.notification.titleLocKey}");
      showNotify(message.data);
    });
  }

  // ignore: missing_return
  static Future<void> showNotify(Map<String, dynamic> message) {
    // ignore: unnecessary_brace_in_string_interps
    print('messagemessage ${message}');
    try {
      if (message != null) {
        switch (message['type']) {
          case 'order':
            MyApp.navigatorKey.currentState.push(
              MaterialPageRoute(
                builder: (context) => OrderDetailsScreen(
                  fromNotify: true,
                  orderModel: null,
                  orderId: int.parse(message['id'].toString()),
                ),
              ),
            );
            break;
          case 'chat':
            // ignore: unused_local_variable
            int shopId = Provider.of<ChatProvider>(
                    MyApp.navigatorKey.currentState.context,
                    listen: false)
                .shopId;
            // if (shopId != int.parse(message['id'].toString())) {}
            // MyApp.navigatorKey.currentState.push(
            //   MaterialPageRoute(
            //     builder: (context) => ShopChatScreen(
            //        fromNotify: true,
            //       shopModel: ShopModel(
            //         id: int.parse(message['id'].toString()),
            //         name: message['shop_name'],
            //         image: message['shop_image'],
            //       ),
            //     ),
            //   ),
            // );
            MyApp.navigatorKey.currentState.push(
              MaterialPageRoute(
                builder: (context) => SplashScreen(),
              ),
            );
            break;
          case 'notification':
            MyApp.navigatorKey.currentState.push(
              MaterialPageRoute(
                builder: (context) => NotificationDetailScreen(
                  fromNotify: true,
                  notificationModel: NotificationModel(
                    id: int.parse(message['id'].toString()),
                    title: message['title'],
                    image: message['image'],
                    description: message['body'],
                  ),
                ),
              ),
            );
            break;
          default:
        }
      } else {}
    } catch (e) {
      print('onMessageError $e');
    }
  }

  static Future<void> showNotification(RemoteMessage message,
      FlutterLocalNotificationsPlugin fln, bool data) async {
    String _title;
    String _body;
    String _orderID;
    String _image;
    if (data) {
      _title = message.data['title'];
      _body = message.data['body'];
      _orderID = message.data['id'];
      _image = (message.data['image'] != null &&
              message.data['image'].isNotEmpty)
          ? message.data['image'].startsWith('http')
              ? message.data['image']
              : '${AppConstants.BASE_URL}/storage/app/public/notification/${message.data['image']}'
          : null;
    } else {
      _title = message.notification.title;
      _body = message.notification.body;
      _orderID = message.notification.titleLocKey;
      if (Platform.isAndroid) {
        _image = (message.notification.android.imageUrl != null &&
                message.notification.android.imageUrl.isNotEmpty)
            ? message.notification.android.imageUrl.startsWith('http')
                ? message.notification.android.imageUrl
                : '${AppConstants.BASE_URL}/storage/app/public/notification/${message.notification.android.imageUrl}'
            : null;
      } else if (Platform.isIOS) {
        _image = (message.notification.apple.imageUrl != null &&
                message.notification.apple.imageUrl.isNotEmpty)
            ? message.notification.apple.imageUrl.startsWith('http')
                ? message.notification.apple.imageUrl
                : '${AppConstants.BASE_URL}/storage/app/public/notification/${message.notification.apple.imageUrl}'
            : null;
      }
    }

    if (_image != null && _image.isNotEmpty) {
      try {
        await showBigPictureNotificationHiddenLargeIcon(
            _title, _body, _orderID, _image, fln, message);
      } catch (e) {
        await showBigTextNotification(_title, _body, _orderID, fln, message);
      }
    } else {
      await showBigTextNotification(_title, _body, _orderID, fln, message);
    }
  }

  static Future<void> showTextNotification(
      String title,
      String body,
      String orderID,
      FlutterLocalNotificationsPlugin fln,
      RemoteMessage remoteMessage) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      playSound: true,
      importance: Importance.max,
      priority: Priority.max,
      sound: RawResourceAndroidNotificationSound('notification'),
    );
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await fln.show(0, title, body, platformChannelSpecifics,
        payload: jsonEncode(remoteMessage.data));
  }

  static Future<void> showBigTextNotification(
    String title,
    String body,
    String orderID,
    FlutterLocalNotificationsPlugin fln,
    RemoteMessage remoteMessage,
  ) async {
    BigTextStyleInformation bigTextStyleInformation = BigTextStyleInformation(
      body,
      htmlFormatBigText: true,
      contentTitle: title,
      htmlFormatContentTitle: true,
    );
    AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      importance: Importance.max,
      styleInformation: bigTextStyleInformation,
      priority: Priority.max,
      playSound: true,
      sound: RawResourceAndroidNotificationSound('notification'),
    );
    NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await fln.show(0, title, body, platformChannelSpecifics,
        payload: jsonEncode(remoteMessage.data));
  }

  static Future<void> showBigPictureNotificationHiddenLargeIcon(
    String title,
    String body,
    String orderID,
    String image,
    FlutterLocalNotificationsPlugin fln,
    RemoteMessage remoteMessage,
  ) async {
    final String largeIconPath = await _downloadAndSaveFile(image, 'largeIcon');
    final String bigPicturePath =
        await _downloadAndSaveFile(image, 'bigPicture');
    final BigPictureStyleInformation bigPictureStyleInformation =
        BigPictureStyleInformation(
      FilePathAndroidBitmap(bigPicturePath),
      hideExpandedLargeIcon: true,
      contentTitle: title,
      htmlFormatContentTitle: true,
      summaryText: body,
      htmlFormatSummaryText: true,
    );
    final AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      largeIcon: FilePathAndroidBitmap(largeIconPath),
      priority: Priority.max,
      playSound: true,
      styleInformation: bigPictureStyleInformation,
      importance: Importance.max,
      sound: RawResourceAndroidNotificationSound('notification'),
    );
    final NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await fln.show(0, title, body, platformChannelSpecifics,
        payload: jsonEncode(remoteMessage.data));
  }

  static Future<String> _downloadAndSaveFile(
      String url, String fileName) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final String filePath = '${directory.path}/$fileName';
    final Response response = await Dio()
        .get(url, options: Options(responseType: ResponseType.bytes));
    final File file = File(filePath);
    await file.writeAsBytes(response.data);
    return filePath;
  }
}

Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
  print(
      "onBackground: ${message.notification.title}/${message.notification.body}/${message.notification.titleLocKey}");
}
