import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_orderby_app_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_screen.dart';
import 'package:provider/provider.dart';

class ShopListWidget extends StatelessWidget {
  final ShopModel shopModel;
  const ShopListWidget({Key key, @required this.shopModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (shopModel.seller.productOrderType == AppConstants.DIRECT_TO_SHOP) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ShopScreen(shopModel: shopModel),
            ),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ShopOrderByAppScreen(shopModel: shopModel),
            ),
          );
        }
      },
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 216, 196, 255),
              border: Border.all(color: Colors.grey.withOpacity(.1)),
              borderRadius: BorderRadius.circular(
                Dimensions.PADDING_SIZE_SMALL,
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 5,
                )
              ],
            ),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(
                      Dimensions.PADDING_SIZE_SMALL,
                    ),
                    bottomLeft: Radius.circular(
                      Dimensions.PADDING_SIZE_SMALL,
                    ),
                  ),
                  child: CachedNetworkImage(
                    height: Responsive.isMobile(context) ? 100 : 150,
                    width: Responsive.isMobile(context) ? 100 : 150,
                    fit: BoxFit.cover,
                    imageUrl:
                        '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${shopModel.image}',
                    placeholder: (c, o) => Image.asset(
                      Images.placeholder,
                      height: Responsive.isMobile(context) ? 100 : 150,
                      width: Responsive.isMobile(context) ? 100 : 150,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(
                  width: Dimensions.PADDING_SIZE_SMALL,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width -
                      (Responsive.isMobile(context) ? 140 : 240),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        shopModel.name,
                        overflow: TextOverflow.ellipsis,
                        style: titilliumBold.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_LARGE
                              : Dimensions.FONT_SIZE_LARGE_IPAD,
                        ),
                      ),
                      Visibility(
                        visible: shopModel.seller.productOrderType ==
                            AppConstants.DIRECT_TO_SHOP,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.location_pin,
                              color: Colors.red,
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width -
                                  (Responsive.isMobile(context) ? 170 : 270),
                              child: Text(
                                shopModel.address,
                                overflow: TextOverflow.ellipsis,
                                style: titilliumRegular.copyWith(
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_DEFAULT
                                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        getTranslated(
                            shopModel.seller.productOrderType, context),
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          color: ColorResources.getSecondary(context),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: Dimensions.PADDING_SIZE_SMALL,
          ),
        ],
      ),
    );
  }
}
