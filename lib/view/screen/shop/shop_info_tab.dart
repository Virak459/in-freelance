import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/schedule_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/about_shop.dart';
import 'package:provider/provider.dart';

class ShopInfoTab extends StatelessWidget {
  final ShopModel shopModel;
  const ShopInfoTab({Key key, @required this.shopModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ShopsProvider>(builder: (context, shopsProvider, child) {
      return Container(
        margin: EdgeInsets.symmetric(
          horizontal: Dimensions.MARGIN_SIZE_SMALL,
        ),
        child: DefaultTabController(
            length: 3, // length of tabs
            initialIndex: 0,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    child: TabBar(
                      labelColor: ColorResources.getSecondary(context),
                      unselectedLabelColor: Colors.black,
                      indicatorColor: ColorResources.getSecondary(context),
                      labelStyle: titilliumRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                      ),
                      physics: NeverScrollableScrollPhysics(),
                      tabs: [
                        Tab(text: getTranslated('Open hour', context)),
                        Tab(text: getTranslated('Contact', context)),
                        Tab(text: getTranslated('About Shop', context)),
                        // Tab(text: getTranslated('Review', context)),
                      ],
                    ),
                  ),
                  Container(
                      height: Responsive.isMobile(context)
                          ? 340
                          : 450, //height of TabBarView
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(color: Colors.grey, width: 0.5),
                        ),
                      ),
                      child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          Container(
                              child: Column(
                            children: [
                              Visibility(
                                visible: shopModel.active ? false : true,
                                child: Text(
                                  getTranslated(
                                      'Shop Is Temporarily Closed', context),
                                  style: titilliumSemiBold.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  ),
                                ),
                              ),
                              Column(
                                  children: List.generate(
                                shopsProvider.shopScheduleList.length,
                                ((index) {
                                  ScheduleModel scheduleModel =
                                      shopsProvider.shopScheduleList[index];
                                  return InkWell(
                                    onTap: () {},
                                    child: Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: 3.0, horizontal: 3.0),
                                        padding: EdgeInsets.symmetric(
                                            vertical: Dimensions
                                                .PADDING_SIZE_EXTRA_SMALL,
                                            horizontal:
                                                Dimensions.PADDING_SIZE_SMALL),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                SizedBox(
                                                  child: Text(
                                                    getTranslated(
                                                        App.getDayByNumber(
                                                            scheduleModel.day),
                                                        context),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      color:
                                                          ColorResources.BLACK,
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_DEFAULT
                                                          : Dimensions
                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            scheduleModel.id != null
                                                ? Row(
                                                    children: [
                                                      Text(
                                                        scheduleModel
                                                                .openingTime ??
                                                            '',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: titilliumRegular
                                                            .copyWith(
                                                          color: ColorResources
                                                              .BLACK,
                                                          fontSize: Responsive
                                                                  .isMobile(
                                                                      context)
                                                              ? Dimensions
                                                                  .FONT_SIZE_DEFAULT
                                                              : Dimensions
                                                                  .FONT_SIZE_DEFAULT_IPAD,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        child: Text(' → '),
                                                      ),
                                                      Text(
                                                        scheduleModel
                                                                .closingTime ??
                                                            '',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: titilliumRegular
                                                            .copyWith(
                                                          color: ColorResources
                                                              .BLACK,
                                                          fontSize: Responsive
                                                                  .isMobile(
                                                                      context)
                                                              ? Dimensions
                                                                  .FONT_SIZE_DEFAULT
                                                              : Dimensions
                                                                  .FONT_SIZE_DEFAULT_IPAD,
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                : Text(
                                                    getTranslated(
                                                        'dayoff', context),
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_LARGE
                                                          : Dimensions
                                                              .FONT_SIZE_SMALL_IPAD,
                                                    )),
                                          ],
                                        )),
                                  );
                                }),
                              )),
                            ],
                          )),
                          Container(
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    App.launchUrl('tel:' + shopModel.contact);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.symmetric(
                                      vertical: Dimensions.PADDING_SIZE_DEFAULT,
                                    ),
                                    padding: EdgeInsets.all(
                                      Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                    ),
                                    decoration: BoxDecoration(
                                        color: ColorResources.getSecondary(
                                            context),
                                        borderRadius: BorderRadius.circular(
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                        )),
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.phone,
                                          size: Responsive.isMobile(context)
                                              ? Dimensions.ICON_SIZE_DEFAULT
                                              : Dimensions
                                                  .ICON_SIZE_DEFAULT_IPAD,
                                          color: Colors.white,
                                        ),
                                        SizedBox(
                                          width: Dimensions
                                              .PADDING_SIZE_EXTRA_SMALL,
                                        ),
                                        Text(
                                          shopModel.contact,
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions.FONT_SIZE_LARGE
                                                    : Dimensions
                                                        .FONT_SIZE_LARGE_IPAD,
                                            color: Colors.white,
                                          ),
                                        ),
                                        SizedBox(
                                          width: Dimensions
                                              .PADDING_SIZE_EXTRA_SMALL,
                                        ),
                                        Text(
                                          getTranslated(
                                              'direct_call_to_shop', context),
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions.FONT_SIZE_LARGE
                                                    : Dimensions
                                                        .FONT_SIZE_LARGE_IPAD,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(
                                          Icons.location_on_rounded,
                                          size: Responsive.isMobile(context)
                                              ? Dimensions.ICON_SIZE_DEFAULT
                                              : Dimensions
                                                  .ICON_SIZE_DEFAULT_IPAD,
                                          color: ColorResources.getSecondary(
                                              context),
                                        ),
                                        SizedBox(
                                          width: Dimensions
                                              .PADDING_SIZE_EXTRA_SMALL,
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              (Responsive.isMobile(context)
                                                  ? 180
                                                  : 280),
                                          child: Text(
                                            shopModel.address,
                                            style: titilliumRegular.copyWith(
                                              fontSize:
                                                  Responsive.isMobile(context)
                                                      ? Dimensions
                                                          .FONT_SIZE_LARGE
                                                      : Dimensions
                                                          .FONT_SIZE_SMALL_IPAD,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Visibility(
                                      visible:
                                          shopModel.seller?.mapLink != null,
                                      child: Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            App.launchUrl(
                                              shopModel.seller.mapLink,
                                            );
                                          },
                                          child: Text(
                                            getTranslated(
                                                'Get direction', context),
                                            style: titilliumBold.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                                color:
                                                    ColorResources.getSecondary(
                                                        context)),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: InkWell(
                              onTap: () {
                                if (shopModel?.about != null) {
                                  Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                        transitionDuration:
                                            Duration(milliseconds: 1000),
                                        pageBuilder: (context, anim1, anim2) =>
                                            AboutShopScreen(
                                                about: shopModel.about),
                                      ));
                                }
                              },
                              child: (shopModel?.about ?? '').isNotEmpty
                                  ? Text((shopModel?.about ?? ''),
                                      maxLines: 12,
                                      style: titilliumRegular.copyWith(
                                        overflow: TextOverflow.ellipsis,
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_LARGE
                                            : Dimensions.FONT_SIZE_SMALL_IPAD,
                                      ))
                                  : NoInternetOrDataScreen(isNoInternet: false),
                            ),
                          ),
                          // Container(
                          //   child: Column(
                          //     children: [
                          //       Container(
                          //         padding: EdgeInsets.symmetric(
                          //             vertical:
                          //                 Dimensions.PADDING_SIZE_DEFAULT),
                          //         child: RatingBar(
                          //             rating:
                          //                 shopsProvider.shop?.avgReview ?? 0),
                          //       ),
                          //       Row(
                          //         children: [
                          //           Text(
                          //             getTranslated('total_review', context) +
                          //                 ' :',
                          //             style: titilliumRegular.copyWith(
                          //               fontSize: Responsive.isMobile(context)
                          //                   ? Dimensions.FONT_SIZE_DEFAULT
                          //                   : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          //             ),
                          //           ),
                          //           Text(
                          //             shopsProvider.shop?.totalReview
                          //                     .toString() ??
                          //                 '',
                          //             style: titilliumRegular.copyWith(
                          //               fontSize: Responsive.isMobile(context)
                          //                   ? Dimensions.FONT_SIZE_DEFAULT
                          //                   : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          //             ),
                          //           ),
                          //         ],
                          //       ),
                          //       Row(
                          //         children: [
                          //           Text(
                          //             getTranslated('total_order', context) +
                          //                 ' :',
                          //             style: titilliumRegular.copyWith(
                          //               fontSize: Responsive.isMobile(context)
                          //                   ? Dimensions.FONT_SIZE_DEFAULT
                          //                   : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          //             ),
                          //           ),
                          //           Text(
                          //             shopsProvider.shop?.totalOrder
                          //                     .toString() ??
                          //                 '',
                          //             style: titilliumRegular.copyWith(
                          //               fontSize: Responsive.isMobile(context)
                          //                   ? Dimensions.FONT_SIZE_DEFAULT
                          //                   : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          //             ),
                          //           ),
                          //         ],
                          //       ),
                          //     ],
                          //   ),
                          // ),
                        ],
                      ))
                ])),
      );
    });
  }
}
