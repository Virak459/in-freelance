import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/category.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/shop/shop_list_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/wishlist/wishlist_screen.dart';
import 'package:provider/provider.dart';

class ShopByCategory extends StatelessWidget {
  final Category category;
  const ShopByCategory({Key key, @required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<CategoryProvider>(context, listen: false).setCategory(category);
    Provider.of<CategoryProvider>(context, listen: false)
        .getShopByCategory(true, context);

    // final TextEditingController _controller = TextEditingController();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Provider.of<ThemeProvider>(context).darkTheme
            ? Colors.black
            : ColorResources.getSecondary(context),
        leading: IconButton(
          icon:
              Icon(Icons.arrow_back_ios, size: 20, color: ColorResources.WHITE),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Center(
          child: Text(getTranslated('all_shopping', context),
              style: titilliumRegular.copyWith(
                  fontSize: 20, color: ColorResources.WHITE)),
        ),
        actions: [
          IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => WishListScreen(),
                ),
              );
            },
            icon: CircleAvatar(
              backgroundColor: Color.fromRGBO(238, 230, 254, 0.973),
              child: Stack(clipBehavior: Clip.none, children: [
                Image.asset(
                  Images.heart,
                  height: Dimensions.ICON_SIZE_DEFAULT,
                  width: Dimensions.ICON_SIZE_DEFAULT,
                  color: ColorResources.getSecondary(context),
                ),
              ]),
            ),
          ),
          IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => CartScreen()));
            },
            icon: CircleAvatar(
              backgroundColor: Color.fromRGBO(225, 211, 255, 0.973),
              child: Stack(clipBehavior: Clip.none, children: [
                Image.asset(
                  Images.cart_image,
                  height: Dimensions.ICON_SIZE_DEFAULT,
                  width: Dimensions.ICON_SIZE_DEFAULT,
                  color: ColorResources.getSecondary(context),
                ),
                Positioned(
                  top: -4,
                  right: -4,
                  child:
                      Consumer<CartProvider>(builder: (context, cart, child) {
                    return CircleAvatar(
                      radius: 7,
                      backgroundColor: Colors.black,
                      child: Text(cart.cartList.length.toString(),
                          style: titilliumSemiBold.copyWith(
                            color: ColorResources.WHITE,
                            fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                          )),
                    );
                  }),
                ),
              ]),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: Dimensions.PADDING_SIZE_DEFAULT),
                      color: Colors.white,
                      child: Text(
                        getTranslated('Shops that sell', context) +
                            ': ' +
                            category.name,
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                    ),
                    Expanded(
                      child: RefreshIndicator(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        backgroundColor: ColorResources.getSecondary(context),
                        onRefresh: () async {
                          await Provider.of<CategoryProvider>(context,
                                  listen: false)
                              .getShopByCategory(true, context);
                        },
                        child: Consumer<CategoryProvider>(
                            builder: (context, categoryProvider, child) {
                          if (categoryProvider.shopByCategory.length > 0) {
                            return ListView.builder(
                              itemCount: categoryProvider.shopByCategory.length,
                              itemBuilder: ((context, index) {
                                ShopModel shopModel =
                                    categoryProvider.shopByCategory[index];
                                return ShopListWidget(shopModel: shopModel);
                              }),
                            );
                          }

                          return NoInternetOrDataScreen(isNoInternet: false);
                        }),
                      ),
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
