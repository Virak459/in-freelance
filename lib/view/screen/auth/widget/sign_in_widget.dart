import 'dart:async';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/body/login_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/sms_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_password_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/forget_password_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/widget/code_picker_widget.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/widget/social_login_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:provider/provider.dart';

class SignInWidget extends StatefulWidget {
  @override
  _SignInWidgetState createState() => _SignInWidgetState();
}

class _SignInWidgetState extends State<SignInWidget> {
  TextEditingController _emailController;
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController;
  GlobalKey<FormState> _formKeyLogin;
  FocusNode _phoneFocus = FocusNode();
  String _countryDialCode = "+855";
  Timer _timer;
  int _start = 10;
  @override
  void initState() {
    Provider.of<AuthProvider>(context, listen: false).updateLoading(false);
    super.initState();
    _formKeyLogin = GlobalKey<FormState>();

    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    _emailController.text =
        Provider.of<AuthProvider>(context, listen: false).getUserEmail() ??
            null;
    _passwordController.text =
        Provider.of<AuthProvider>(context, listen: false).getUserPassword() ??
            null;

    _countryDialCode = CountryCode.fromCountryCode(
            Provider.of<SplashProvider>(context, listen: false)
                .configModel
                .countryCode)
        .dialCode;
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    if (_timer != null) {
      _timer.cancel();
    }

    super.dispose();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        print("countdown:$_start");
        if (_start == 0) {
          Provider.of<AuthProvider>(context, listen: false)
              .updateLoading(false);
          alertMessage("Login fail");
          setState(() {
            timer.cancel();
            _start = 10;
          });
        } else {
          setState(() {
            _start--;
          });
        }
        if (Provider.of<AuthProvider>(context, listen: false).isLoading ==
            false) {
          setState(() {
            timer.cancel();
            _start = 10;
          });
        }
      },
    );
  }

  // ignore: unused_field
  FocusNode _emailNode = FocusNode();
  FocusNode _passNode = FocusNode();
  LoginModel loginBody = LoginModel();

  void loginUser() async {
    if (_formKeyLogin.currentState.validate()) {
      _formKeyLogin.currentState.save();

      String _email = _phoneController.text.trim();
      String _password = _passwordController.text.trim();

      if (_email.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              getTranslated('PHONE_MUST_BE_REQUIRED', context),
              style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD),
            ),
            backgroundColor: Colors.red[400],
            duration: Duration(seconds: 2),
          ),
        );
      } else if (_password.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              getTranslated('PASSWORD_MUST_BE_REQUIRED', context),
              style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD),
            ),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 2),
          ),
        );
      } else {
        _email = _countryDialCode + _email;

        if (_phoneController.text[0].toString() == '0') {
          _email = _countryDialCode + _phoneController.text.trim().substring(1);
        }

        if (Provider.of<AuthProvider>(context, listen: false).isRemember) {
          Provider.of<AuthProvider>(context, listen: false)
              .saveUserEmail(_email, _password);
        } else {
          Provider.of<AuthProvider>(context, listen: false)
              .clearUserEmailAndPassword();
        }

        loginBody.email = _email;
        loginBody.password = _password;
        print(loginBody.toJson());
        startTimer();
        await Provider.of<AuthProvider>(context, listen: false)
            .login(loginBody, route);
      }
    }
  }

  route(bool isRoute, String token, String temporaryToken,
      String errorMessage) async {
    if (isRoute) {
      if (token == null || token.isEmpty) {
        if (Provider.of<SplashProvider>(context, listen: false)
            .configModel
            .emailVerification) {
          print("CheckLoginPhoneVerify1");

          Provider.of<AuthProvider>(context, listen: false)
              .checkEmail(_emailController.text.toString(), temporaryToken)
              .then((value) async {
            print("CheckLoginPhoneVerify2");

            if (value.isSuccess) {
              Provider.of<AuthProvider>(context, listen: false)
                  .updateEmail(_emailController.text.toString());
              // Navigator.pushAndRemoveUntil(
              //     context,
              //     MaterialPageRoute(
              //         builder: (_) => VerificationScreen(temporaryToken, '',
              //             _emailController.text.toString(), "", "signIn")),
              //     (route) => false);
              SMSModel().sendOTP(
                context,
                alertMessage,
                "signIn",
                temporaryToken,
                loginBody.email,
                loginBody.password,
              );
            }
          });
        } else if (Provider.of<SplashProvider>(context, listen: false)
            .configModel
            .phoneVerification) {
          print("CheckLoginPhoneVerify3");

          SMSModel().sendOTP(
            context,
            alertMessage,
            "signIn",
            temporaryToken,
            loginBody.email,
            loginBody.password,
          );
          // Navigator.pushAndRemoveUntil(
          //     context,
          //     MaterialPageRoute(
          //       builder: (_) =>
          //           MobileVerificationScreen(temporaryToken, "signIn"),
          //       settings: RouteSettings(
          //         arguments: _phoneController.text,
          //       ),
          //     ),
          //     (route) => false);
        }
      } else {
        await Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context);

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => DashBoardScreen()),
            (route) => false);

        // Navigator.pushAndRemoveUntil(
        //     context,
        //     MaterialPageRoute(builder: (_) => ProfileScreen()),
        //     (route) => false);
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            errorMessage,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 5),
        ),
      );
    }
  }

  void alertMessage(title) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        title,
        style: titilliumRegular.copyWith(
          fontSize: Responsive.isMobile(context)
              ? Dimensions.FONT_SIZE_DEFAULT
              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
        ),
      ),
      backgroundColor: Colors.red,
      duration: const Duration(seconds: 3),
    ));
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<AuthProvider>(context, listen: false).isRemember;
    return Form(
      key: _formKeyLogin,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL),
        children: [
          // for Email
          // Container(
          //   margin: EdgeInsets.only(
          //       left: Dimensions.MARGIN_SIZE_LARGE,
          //       right: Dimensions.MARGIN_SIZE_LARGE,
          //       bottom: Dimensions.MARGIN_SIZE_SMALL),
          //   child: CustomTextField(
          //     hintText: getTranslated('ENTER_YOUR_EMAIL', context),
          //     focusNode: _emailNode,
          //     nextNode: _passNode,
          //     textInputType: TextInputType.emailAddress,
          //     controller: _emailController,
          //     onChanged: (value) {

          //     },
          //   ),
          // ),

          //For Phone
          Container(
            margin: EdgeInsets.only(
                left: Dimensions.MARGIN_SIZE_LARGE,
                right: Dimensions.MARGIN_SIZE_LARGE,
                bottom: Dimensions.MARGIN_SIZE_SMALL),
            child: Row(children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: CodePickerWidget(
                  onChanged: (CountryCode countryCode) {
                    _countryDialCode = countryCode.dialCode;
                  },
                  initialSelection: _countryDialCode,
                  countryFilter: ['kh', 'th', 'vn', 'us'],
                  favorite: [_countryDialCode],
                  showDropDownButton: true,
                  padding: EdgeInsets.zero,
                  showFlagMain: true,
                  textStyle: TextStyle(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                    color: ColorResources.BLACK,
                  ),
                ),
              ),
              SizedBox(
                width: 5.0,
              ),
              Expanded(
                  child: CustomTextField(
                hintText: getTranslated('ENTER_MOBILE_NUMBER', context),
                controller: _phoneController,
                focusNode: _phoneFocus,
                nextNode: _passNode,
                isPhoneNumber: true,
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.phone,
                fillColor:
                    Provider.of<ThemeProvider>(context, listen: false).darkTheme
                        ? HexColor('ed1848')
                        : HexColor('eee6fe'),
              )),
            ]),
          ),

          // for Password
          Container(
            margin: EdgeInsets.only(
                left: Dimensions.MARGIN_SIZE_LARGE,
                right: Dimensions.MARGIN_SIZE_LARGE,
                bottom: Dimensions.MARGIN_SIZE_DEFAULT),
            child: CustomPasswordTextField(
              hintTxt: getTranslated('PASSWORD', context),
              textInputAction: TextInputAction.done,
              focusNode: _passNode,
              controller: _passwordController,
            ),
          ),

          // for remember and forgetpassword
          Container(
            margin: EdgeInsets.only(
                left: Dimensions.MARGIN_SIZE_SMALL,
                right: Dimensions.MARGIN_SIZE_SMALL),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Consumer<AuthProvider>(
                      builder: (context, authProvider, child) =>
                          Transform.scale(
                        scale: Responsive.isMobile(context) ? 1 : 1.3,
                        child: Checkbox(
                          checkColor: Theme.of(context).primaryColor,
                          side: BorderSide(
                            color: Theme.of(context).primaryColor,
                          ),
                          activeColor: Colors.white,
                          value: authProvider.isRemember,
                          onChanged: authProvider.updateRemember,
                        ),
                      ),
                    ),
                    //

                    Text(
                      getTranslated('REMEMBER', context),
                      style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ForgetPasswordScreen(),
                    ),
                  ),
                  child: Text(
                    getTranslated('FORGET_PASSWORD', context),
                    style: titilliumRegular.copyWith(
                        color: ColorResources.getPrimary(context),
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD),
                  ),
                ),
              ],
            ),
          ),

          // for signin button
          Container(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 30),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(6),
            ),
            child: Provider.of<AuthProvider>(context).isLoading
                ? Center(
                    heightFactor: 1.34,
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        ColorResources.getPrimary(context),
                      ),
                    ),
                  )
                : CustomButton(
                    onTap: loginUser,
                    buttonText: getTranslated('SIGN_IN', context)),
          ),

          SizedBox(width: 25),

          // SocialLoginWidget(),

          SizedBox(height: 20),
          Center(
            child: Text(
              getTranslated('OR', context),
              style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_DEFAULT
                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
              ),
            ),
          ),

          //for order as guest
          GestureDetector(
            onTap: () {
              if (!Provider.of<AuthProvider>(context, listen: false)
                  .isLoading) {
                Provider.of<CartProvider>(context, listen: false).getCartData();
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => DashBoardScreen()),
                    (route) => false);
              }
            },
            child: Container(
              margin: EdgeInsets.only(left: 50, right: 50, top: 30),
              width: double.infinity,
              height: Responsive.isMobile(context) ? 40 : 60,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(6),
                border: Border.all(
                  color: ColorResources.getPrimary(context),
                  width: 1.0,
                ),
              ),
              child: Text(
                getTranslated('CONTINUE_AS_GUEST', context),
                style: titilliumSemiBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  color: ColorResources.getPrimary(context),
                ),
              ),
            ),
          ),
        ],
      ),
    );
    //   Consumer<GoogleSignInProvider>(builder: (ctx,model,child){
    //   if(model.googleAccount != null){
    //     return googleLoggedInUI(model);
    //   }else{
    //     return loginControls(context);
    //   }
    //
    // });
  }
}
