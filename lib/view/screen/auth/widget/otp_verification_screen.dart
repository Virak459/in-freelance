import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/body/login_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/auth_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/widget/reset_password_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class VerificationScreen extends StatelessWidget {
  final String tempToken;
  final String mobileNumber;
  final String email;
  final String verificationId;
  final String checkP;
  final String password;

  VerificationScreen(this.tempToken, this.mobileNumber, this.email,
      this.verificationId, this.checkP, this.password);

  LoginModel loginBody = LoginModel();

  @override
  Widget build(BuildContext context) {
    print('=======Temp Token=====>$tempToken');
    print('=======Mobile Number=====>$mobileNumber');
    print('=======Password=====>$password');
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    // ignore: unused_element
    void alertMessage(title) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          title,
          style: titilliumRegular.copyWith(
            fontSize: Responsive.isMobile(context)
                ? Dimensions.FONT_SIZE_DEFAULT
                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
          ),
        ),
        duration: const Duration(seconds: 3),
      ));
    }

    route(bool isRoute, String token, String temporaryToken,
        String errorMessage) async {
      if (isRoute) {
        if (token != null) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => DashBoardScreen()),
              (route) => false);
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            errorMessage,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 2),
        ));
      }
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: ColorResources.getHomeBg(context),
        leading: BackButton(
          color: Theme.of(context).textTheme.bodyText1.color,
          onPressed: () {
            Provider.of<AuthProvider>(context, listen: false)
                .updateSelectedIndex(0);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AuthScreen(initialPage: 0),
              ),
            );
          },
        ),
      ),
      body: SafeArea(
        child: Scrollbar(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Center(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Consumer<AuthProvider>(
                  builder: (context, authProvider, child) => Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 55),
                      Image.asset(
                        Images.login,
                        width: Responsive.isMobile(context) ? 100 : 200,
                        height: Responsive.isMobile(context) ? 100 : 200,
                        color: ColorResources.getSecondary(context),
                      ),
                      SizedBox(height: 40),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        child: Center(
                            child: Text(
                          email == null
                              ? '${getTranslated('please_enter_6_digit_code', context)}\n$mobileNumber'
                              : '${getTranslated('please_enter_6_digit_code', context)}\n$email',
                          textAlign: TextAlign.center,
                          style: titilliumRegular.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          ),
                        )),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: Responsive.isMobile(context) ? 40 : 80,
                          vertical: Dimensions.PADDING_SIZE_SMALL,
                        ),
                        child: PinCodeTextField(
                          textStyle: titilliumRegular.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_EXTRA_LARGE
                                : Dimensions.FONT_SIZE_EXTRA_LARGE_IPAD,
                          ),
                          length: 6,
                          appContext: context,
                          obscureText: false,
                          showCursor: true,
                          keyboardType: TextInputType.number,
                          animationType: AnimationType.fade,
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            fieldHeight:
                                Responsive.isMobile(context) ? 60 : 120,
                            fieldWidth: Responsive.isMobile(context) ? 45 : 100,
                            borderWidth: .8,
                            borderRadius: BorderRadius.circular(10),
                            selectedColor: ColorResources.colorMap[200],
                            selectedFillColor: Colors.white,
                            inactiveFillColor:
                                ColorResources.getSearchBg(context),
                            inactiveColor: ColorResources.colorMap[200],
                            activeColor: ColorResources.colorMap[400],
                            activeFillColor:
                                ColorResources.getSearchBg(context),
                          ),
                          animationDuration: Duration(milliseconds: 300),
                          backgroundColor: Colors.transparent,
                          enableActiveFill: true,
                          onChanged: authProvider.updateVerificationCode,
                          beforeTextPaste: (text) {
                            return true;
                          },
                        ),
                      ),
                      Center(
                          child: Text(
                        getTranslated('i_didnt_receive_the_code', context),
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      )),
                      Center(
                        child: InkWell(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => VerificationScreen(
                                  tempToken,
                                  mobileNumber,
                                  email,
                                  verificationId,
                                  checkP,
                                  password,
                                ),
                              ),
                            );
                            Provider.of<AuthProvider>(context, listen: false)
                                .checkPhone(mobileNumber, tempToken)
                                .then((value) {
                              if (value.isSuccess) {
                                showCustomSnackBar(
                                    getTranslated(
                                        'Resent code successful', context),
                                    context,
                                    isError: false);
                              } else {
                                showCustomSnackBar(value.message, context,
                                    isError: false);
                              }
                            });
                          },
                          child: Padding(
                            padding: EdgeInsets.all(
                                Dimensions.PADDING_SIZE_EXTRA_SMALL),
                            child: Text(
                              getTranslated('resend_code', context),
                              style: titilliumRegular.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 48),
                      authProvider.isEnableVerificationCode
                          ? !authProvider.isPhoneNumberVerificationButtonLoading
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal:
                                          Dimensions.PADDING_SIZE_LARGE),
                                  child: CustomButton(
                                    buttonText:
                                        getTranslated('verify', context),
                                    onTap: () {
                                      // String phoneVerification =
                                      //     Provider.of<SplashProvider>(context,
                                      //             listen: false)
                                      //         .configModel
                                      //         .forgetPasswordVerification;

                                      if (checkP
                                          .toLowerCase()
                                          .contains("forget")) {
                                        Provider.of<AuthProvider>(context,
                                                listen: false)
                                            .verifyOtp(
                                                mobileNumber,
                                                tempToken,
                                                authProvider.verificationCode,
                                                verificationId,
                                                checkP)
                                            .then((value) {
                                          if (value.isSuccess) {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      ResetPasswordWidget(
                                                          mobileNumber:
                                                              mobileNumber,
                                                          tmpToken: tempToken)),
                                            );
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              SnackBar(
                                                content: Text(
                                                  getTranslated(
                                                      'input_invalid_otp',
                                                      context),
                                                ),
                                                backgroundColor: Colors.red,
                                              ),
                                            );
                                          }
                                        });
                                      } else {
                                        if (Provider.of<SplashProvider>(context,
                                                listen: false)
                                            .configModel
                                            .phoneVerification) {
                                          Provider.of<AuthProvider>(context,
                                                  listen: false)
                                              .verifyOtp(
                                                  mobileNumber,
                                                  tempToken,
                                                  authProvider.verificationCode,
                                                  verificationId,
                                                  checkP)
                                              .then((value) {
                                            if (value.isSuccess) {
                                              if (password != null) {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(SnackBar(
                                                  content: Text(
                                                    getTranslated(
                                                        'sign_up_successfully_now_login',
                                                        context),
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_DEFAULT
                                                          : Dimensions
                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                    ),
                                                  ),
                                                  backgroundColor: Colors.green,
                                                ));

                                                loginBody.email = mobileNumber;
                                                loginBody.password = password;
                                                Provider.of<AuthProvider>(
                                                        context,
                                                        listen: false)
                                                    .login(loginBody, route);
                                              } else {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(SnackBar(
                                                  content: Text(
                                                    getTranslated(
                                                        'sign_up_successfully_now_login',
                                                        context),
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_DEFAULT
                                                          : Dimensions
                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                    ),
                                                  ),
                                                  backgroundColor: Colors.green,
                                                ));

                                                Provider.of<AuthProvider>(
                                                        context,
                                                        listen: false)
                                                    .updateSelectedIndex(0);

                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (_) => AuthScreen(
                                                        initialPage: 0),
                                                  ),
                                                );
                                              }
                                            } else {
                                              showCustomSnackBar(
                                                  getTranslated(
                                                      'input_valid_otp',
                                                      context),
                                                  context);
                                            }
                                          });
                                        }
                                        // else {
                                        //   Provider.of<AuthProvider>(context,
                                        //           listen: false)
                                        //       .verifyEmail(email, tempToken)
                                        //       .then((value) {
                                        //     if (value.isSuccess) {
                                        //       ScaffoldMessenger.of(context)
                                        //           .showSnackBar(
                                        //         SnackBar(
                                        //           content: Text(
                                        //             getTranslated(
                                        //                 'sign_up_successfully_now_login',
                                        //                 context),
                                        //           ),
                                        //           backgroundColor: Colors.green,
                                        //         ),
                                        //       );
                                        //       Navigator.pushAndRemoveUntil(
                                        //           context,
                                        //           MaterialPageRoute(
                                        //             builder: (_) => AuthScreen(
                                        //                 initialPage: 0),
                                        //           ),
                                        //           (route) => false);
                                        //     } else {
                                        //       ScaffoldMessenger.of(context)
                                        //           .showSnackBar(SnackBar(
                                        //               content:
                                        //                   Text(value.message),
                                        //               backgroundColor:
                                        //                   Colors.red));
                                        //     }
                                        //   });
                                        // }
                                      }
                                    },
                                  ),
                                )
                              : Center(
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Theme.of(context).primaryColor),
                                  ),
                                )
                          : SizedBox.shrink()
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
