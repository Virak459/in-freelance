import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_password_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/auth_screen.dart';
import 'package:provider/provider.dart';

class ResetPasswordWidget extends StatefulWidget {
  final String mobileNumber;
  final String tmpToken;
  const ResetPasswordWidget(
      {Key key, @required this.mobileNumber, @required this.tmpToken})
      : super(key: key);

  @override
  _ResetPasswordWidgetState createState() => _ResetPasswordWidgetState();
}

class _ResetPasswordWidgetState extends State<ResetPasswordWidget> {
  TextEditingController _passwordController;
  TextEditingController _confirmPasswordController;
  FocusNode _newPasswordNode = FocusNode();
  FocusNode _confirmPasswordNode = FocusNode();
  GlobalKey<FormState> _formKeyReset;

  @override
  void initState() {
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();
    super.initState();
  }

  void resetPassword() async {
    // if (_formKeyReset.currentState.validate()) {
    //   _formKeyReset.currentState.save();

    String _password = _passwordController.text.trim();
    String _confirmPassword = _confirmPasswordController.text.trim();

    if (_password.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          getTranslated('PASSWORD_MUST_BE_REQUIRED', context),
          style: titilliumRegular.copyWith(
            fontSize: Responsive.isMobile(context)
                ? Dimensions.FONT_SIZE_DEFAULT
                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
          ),
        ),
        backgroundColor: Colors.red,
      ));
    } else if (_confirmPassword.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content:
            Text(getTranslated('CONFIRM_PASSWORD_MUST_BE_REQUIRED', context)),
        backgroundColor: Colors.red,
      ));
    } else if (_password != _confirmPassword) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          getTranslated('PASSWORD_DID_NOT_MATCH', context),
          style: titilliumRegular.copyWith(
            fontSize: Responsive.isMobile(context)
                ? Dimensions.FONT_SIZE_DEFAULT
                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
          ),
        ),
        backgroundColor: Colors.red,
      ));
    } else {
      //reset Condition here
      Provider.of<AuthProvider>(context, listen: false)
          .resetPassword(
              widget.mobileNumber, widget.tmpToken, _password, _confirmPassword)
          .then((value) {
        if (value.isSuccess) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              getTranslated('password_reset_successfully', context),
              style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_DEFAULT
                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
              ),
            ),
            backgroundColor: Colors.green,
          ));
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => AuthScreen()),
              (route) => false);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              value.message,
              style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_DEFAULT
                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
              ),
            ),
            backgroundColor: Colors.red,
          ));
        }
      });
    }
    // }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: BackButton(
          color: Colors.black,
        ),
        backgroundColor: ColorResources.getHomeBg(context),
      ),
      body: Form(
        key: _formKeyReset,
        child: ListView(
          padding:
              EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL),
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                vertical: Dimensions.PADDING_SIZE_LARGE,
              ),
              child: Image(
                width: 160.0,
                height: 160.0,
                image: AssetImage(Images.logo_with_name_image),
                errorBuilder: (c, o, s) => Image.asset(
                  Images.placeholder,
                  fit: BoxFit.cover,
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(Dimensions.MARGIN_SIZE_LARGE),
              child: Text(getTranslated('password_reset', context),
                  style: titilliumSemiBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  )),
            ),
            // for new password
            Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_LARGE,
                    right: Dimensions.MARGIN_SIZE_LARGE,
                    bottom: Dimensions.MARGIN_SIZE_SMALL),
                child: CustomPasswordTextField(
                  hintTxt: getTranslated('new_password', context),
                  focusNode: _newPasswordNode,
                  nextNode: _confirmPasswordNode,
                  controller: _passwordController,
                )),

            // for confirm Password
            Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_LARGE,
                    right: Dimensions.MARGIN_SIZE_LARGE,
                    bottom: Dimensions.MARGIN_SIZE_DEFAULT),
                child: CustomPasswordTextField(
                  hintTxt: getTranslated('confirm_password', context),
                  textInputAction: TextInputAction.done,
                  focusNode: _confirmPasswordNode,
                  controller: _confirmPasswordController,
                )),

            // for reset button
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 30),
              child: Provider.of<AuthProvider>(context).isLoading
                  ? Center(
                      child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor,
                        ),
                      ),
                    )
                  : CustomButton(
                      onTap: resetPassword,
                      buttonText: getTranslated('reset_password', context)),
            ),
          ],
        ),
      ),
    );
  }
}
