import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/social_login_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/apple_sign_in_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/facebook_login_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/google_sign_in_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:provider/provider.dart';

import 'mobile_verify_screen.dart';
import 'otp_verification_screen.dart';

class SocialLoginWidget extends StatefulWidget {
  @override
  _SocialLoginWidgetState createState() => _SocialLoginWidgetState();
}

class _SocialLoginWidgetState extends State<SocialLoginWidget> {
  SocialLoginModel socialLogin = SocialLoginModel();
  route(bool isRoute, String token, String temporaryToken,
      String errorMessage) async {
    if (isRoute) {
      if (token != null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => DashBoardScreen()),
            (route) => false);
      } else if (temporaryToken != null && temporaryToken.isNotEmpty) {
        if (Provider.of<SplashProvider>(context, listen: false)
            .configModel
            .emailVerification) {
          Provider.of<AuthProvider>(context, listen: false)
              .checkEmail(socialLogin.email.toString(), temporaryToken)
              .then((value) async {
            if (value.isSuccess) {
              Provider.of<AuthProvider>(context, listen: false)
                  .updateEmail(socialLogin.email.toString());
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (_) => VerificationScreen(temporaryToken, '',
                          socialLogin.email.toString(), "", "signIn", '')),
                  (route) => false);
            }
          });
        }
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (_) =>
                    MobileVerificationScreen(temporaryToken, "signIn")),
            (route) => false);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              errorMessage,
              style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_DEFAULT
                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
              ),
            ),
            backgroundColor: Colors.red));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            errorMessage,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: Colors.red));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Provider.of<SplashProvider>(context, listen: false)
                .configModel
                .socialLogin[0]
                .status
            ? Provider.of<SplashProvider>(context, listen: false)
                    .configModel
                    .socialLogin[1]
                    .status
                ? Center(
                    child: Text(
                    getTranslated('social_login', context),
                    style: titilliumRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD),
                  ))
                : Center(
                    child: Text(
                    getTranslated('social_login', context),
                    style: titilliumRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD),
                  ))
            : SizedBox(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Provider.of<SplashProvider>(context, listen: false)
                    .configModel
                    .socialLogin[0]
                    .status
                ? InkWell(
                    onTap: () async {
                      await Provider.of<GoogleSignInProvider>(context,
                              listen: false)
                          .login();
                      String id, token, email, medium;
                      if (Provider.of<GoogleSignInProvider>(context,
                                  listen: false)
                              .googleAccount !=
                          null) {
                        id = Provider.of<GoogleSignInProvider>(context,
                                listen: false)
                            .googleAccount
                            .id;
                        email = Provider.of<GoogleSignInProvider>(context,
                                listen: false)
                            .googleAccount
                            .email;
                        token = Provider.of<GoogleSignInProvider>(context,
                                listen: false)
                            .auth
                            .accessToken;
                        medium = 'google';
                        socialLogin.email = email;
                        socialLogin.medium = medium;
                        socialLogin.token = token;
                        socialLogin.uniqueId = id;
                        await Provider.of<AuthProvider>(context, listen: false)
                            .socialLogin(socialLogin, route);
                      }
                    },
                    child: Ink(
                      child: Padding(
                        padding: EdgeInsets.all(6),
                        child: Card(
                          // color: Color(0xFF397AF3),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    height:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    width:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    child: Image.asset(Images
                                        .google)), // <-- Use 'Image.asset(...)' here
                                // SizedBox(width: 12),
                                // Padding(
                                //   padding: const EdgeInsets.fromLTRB(0,8,8,8),
                                //   child: Text('Google',maxLines: 2, style: TextStyle(color: Colors.white),),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            Provider.of<SplashProvider>(context, listen: false)
                    .configModel
                    .socialLogin[1]
                    .status
                ? InkWell(
                    onTap: () async {
                      await Provider.of<FacebookLoginProvider>(context,
                              listen: false)
                          .login();

                      String id, token, email, medium;
                      if (Provider.of<FacebookLoginProvider>(context,
                                  listen: false)
                              .userData !=
                          null) {
                        id = Provider.of<FacebookLoginProvider>(context,
                                listen: false)
                            .result
                            .accessToken
                            .userId;
                        email = Provider.of<FacebookLoginProvider>(context,
                                listen: false)
                            .userData['email'];
                        token = Provider.of<FacebookLoginProvider>(context,
                                listen: false)
                            .result
                            .accessToken
                            .token;
                        medium = 'facebook';

                        socialLogin.email = email;
                        socialLogin.medium = medium;
                        socialLogin.token = token;
                        socialLogin.uniqueId = id;

                        await Provider.of<AuthProvider>(context, listen: false)
                            .socialLogin(socialLogin, route);
                      }
                    },
                    child: Ink(
                      child: Padding(
                        padding: EdgeInsets.all(6),
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Container(
                                    height:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    width:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    child: Image.asset(Images
                                        .facebook)), // <-- Use 'Image.asset(...)' here
                                // SizedBox(width: 8),
                                // Text('Facebook',maxLines: 2,),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            Provider.of<SplashProvider>(context, listen: false)
                        .configModel
                        .socialLogin[2]
                        .status &&
                    Platform.isIOS
                ? InkWell(
                    onTap: () async {
                      var auth = await Provider.of<AppleSignInProvider>(context,
                              listen: false)
                          .login();

                      socialLogin.name = auth.displayName;
                      socialLogin.email = auth.email;
                      socialLogin.medium = 'apple';
                      socialLogin.token = Provider.of<AppleSignInProvider>(
                              context,
                              listen: false)
                          .auth
                          .identityToken;
                      socialLogin.uniqueId = auth.uid;

                      await Provider.of<AuthProvider>(context, listen: false)
                          .socialLogin(socialLogin, route);
                    },
                    child: Ink(
                      child: Padding(
                        padding: EdgeInsets.all(6),
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Container(
                                    height:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    width:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    child: Image.asset(Images
                                        .apple)), // <-- Use 'Image.asset(...)' here
                                // SizedBox(width: 8),
                                // Text('Facebook',maxLines: 2,),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ],
    );
  }
}
