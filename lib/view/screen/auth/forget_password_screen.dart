import 'package:connectivity/connectivity.dart';
import 'package:country_code_picker/country_code.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/sms_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/widget/code_picker_widget.dart';
import 'package:provider/provider.dart';

class ForgetPasswordScreen extends StatefulWidget {
  // String tmpToken;
  // ForgetPasswordScreen(this.tmpToken);
  @override
  State<ForgetPasswordScreen> createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  final TextEditingController _controller = TextEditingController();
  final GlobalKey<ScaffoldMessengerState> _key = GlobalKey();
  final TextEditingController _numberController = TextEditingController();
  final FocusNode _numberFocus = FocusNode();
  String _countryDialCode = '+855';

  @override
  void initState() {
    _countryDialCode = CountryCode.fromCountryCode(
            Provider.of<SplashProvider>(context, listen: false)
                .configModel
                .countryCode)
        .dialCode;
    super.initState();
  }

  void alertMessage(String title) {
    showCustomSnackBar(title, context);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      key: _key,
      body: Container(
        //color: ColorResources.getPrimary(context),
        decoration: BoxDecoration(
          image: Provider.of<ThemeProvider>(context, listen: false).darkTheme
              ? null
              : DecorationImage(
                  image: AssetImage(Images.background), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            SafeArea(
              child: Align(
                alignment: Alignment.centerLeft,
                child: BackButton(
                  color: Theme.of(context).textTheme.bodyText1.color,
                  onPressed: () {
                    Provider.of<AuthProvider>(context, listen: false)
                        .updateLoading(false);
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
            Expanded(
              child: ListView(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(
                        vertical: Dimensions.PADDING_SIZE_LARGE,
                      ),
                      child: Image(
                        width: 160.0,
                        height: 160.0,
                        image: AssetImage(Images.logo_with_name_image),
                        errorBuilder: (c, o, s) => Image.asset(
                          Images.placeholder,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Text(
                      getTranslated('FORGET_PASSWORD', context),
                      style: titilliumSemiBold.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        color: ColorResources.getPrimary(context),
                      ),
                    ),
                    Row(children: [
                      Expanded(
                        flex: 1,
                        child: Divider(
                          thickness: 1,
                          color: ColorResources.getPrimary(context),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Divider(
                          thickness: 0.2,
                          color: ColorResources.getPrimary(context),
                        ),
                      ),
                    ]),
                    Provider.of<SplashProvider>(context, listen: false)
                                .configModel
                                .forgetPasswordVerification ==
                            "phone"
                        ? Text(
                            getTranslated(
                                'enter_phone_number_for_password_reset',
                                context),
                            style: titilliumRegular.copyWith(
                              color: Colors.black,
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_SMALL
                                  : Dimensions.FONT_SIZE_SMALL_IPAD,
                            ),
                          )
                        : Text(
                            getTranslated(
                                'enter_email_for_password_reset', context),
                            style: titilliumRegular.copyWith(
                                color: Theme.of(context).hintColor,
                                fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL)),
                    SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
                    Provider.of<SplashProvider>(context, listen: false)
                                .configModel
                                .forgetPasswordVerification ==
                            "phone"
                        ? Row(children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: CodePickerWidget(
                                onChanged: (CountryCode countryCode) {
                                  _countryDialCode = countryCode.dialCode;
                                },
                                countryFilter: ['kh', 'th', 'vn', 'us'],
                                initialSelection: _countryDialCode,
                                favorite: [_countryDialCode],
                                showDropDownButton: true,
                                padding: EdgeInsets.zero,
                                showFlagMain: true,
                                textStyle: TextStyle(
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_DEFAULT
                                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  color: ColorResources.BLACK,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Expanded(
                                child: CustomTextField(
                              hintText: getTranslated('number_hint', context),
                              controller: _numberController,
                              focusNode: _numberFocus,
                              isPhoneNumber: true,
                              textInputAction: TextInputAction.done,
                              textInputType: TextInputType.phone,
                            )),
                          ])
                        : CustomTextField(
                            controller: _controller,
                            hintText:
                                getTranslated('ENTER_YOUR_EMAIL', context),
                            textInputAction: TextInputAction.done,
                            textInputType: TextInputType.emailAddress,
                          ),
                    SizedBox(height: 100),
                    Builder(
                      builder: (context) => Provider.of<AuthProvider>(context)
                              .isLoading
                          ? Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Theme.of(context).primaryColor),
                              ),
                            )
                          : Container(
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              child: CustomButton(
                                buttonText: Provider.of<SplashProvider>(context,
                                                listen: false)
                                            .configModel
                                            .forgetPasswordVerification ==
                                        "phone"
                                    ? getTranslated('send_otp', context)
                                    : getTranslated('send_email', context),
                                onTap: () {
                                  if (Provider.of<SplashProvider>(context,
                                              listen: false)
                                          .configModel
                                          .forgetPasswordVerification ==
                                      "phone") {
                                    if (_numberController.text.isEmpty) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          content: Text(
                                            getTranslated(
                                                'PHONE_MUST_BE_REQUIRED',
                                                context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                            ),
                                          ),
                                          backgroundColor: Colors.red,
                                          duration: Duration(seconds: 2),
                                        ),
                                      );
                                    } else {
                                      Provider.of<AuthProvider>(context,
                                              listen: false)
                                          .forgetPassword(_countryDialCode +
                                              _numberController.text.trim())
                                          .then((value) {
                                        if (value.isSuccess) {
                                          // alertMessage("Sending success");
                                          String _phone = _countryDialCode +  _numberController.text.trim();

                                        
                                          SMSModel().sendOTP(
                                              context,
                                              alertMessage,
                                              'forget',
                                              value.forgottoken,
                                              _phone,
                                              null);
                                        } else {
                                       
                                          // ignore: unrelated_type_equality_checks
                                          if (Connectivity()
                                                  .checkConnectivity() ==
                                              ConnectivityResult.none) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                              backgroundColor: Colors.red,
                                              duration: Duration(seconds: 2),
                                              content: Text(
                                                getTranslated(
                                                    'no_connection', context),
                                                textAlign: TextAlign.center,
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ),
                                            ));
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                              content: Text(
                                                getTranslated(
                                                    'input_valid_phone_number',
                                                    context),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ),
                                              backgroundColor: Colors.red,
                                            ));
                                          }
                                        }
                                      });
                                    }
                                  }
                                  //  else {
                                  //   if (_controller.text.isEmpty) {
                                  //     ScaffoldMessenger.of(context)
                                  //         .showSnackBar(SnackBar(
                                  //       content: Text(getTranslated(
                                  //           'EMAIL_MUST_BE_REQUIRED', context)),
                                  //       backgroundColor: Colors.red,
                                  //     ));
                                  //   } else {
                                  //     Provider.of<AuthProvider>(context,
                                  //             listen: false)
                                  //         .forgetPassword(_controller.text)
                                  //         .then((value) {
                                  //       if (value.isSuccess) {
                                  //         FocusScopeNode currentFocus =
                                  //             FocusScope.of(context);
                                  //         if (!currentFocus.hasPrimaryFocus) {
                                  //           currentFocus.unfocus();
                                  //         }
                                  //         _controller.clear();

                                  //         showAnimatedDialog(
                                  //             context,
                                  //             MyDialog(
                                  //               icon: Icons.send,
                                  //               title: getTranslated(
                                  //                   'sent', context),
                                  //               description: getTranslated(
                                  //                   'recovery_link_sent',
                                  //                   context),
                                  //               rotateAngle: 5.5,
                                  //             ),
                                  //             dismissible: false);
                                  //       } else {
                                  //         ScaffoldMessenger.of(context)
                                  //             .showSnackBar(SnackBar(
                                  //           content: Text(value.message),
                                  //           backgroundColor: Colors.red,
                                  //         ));
                                  //       }
                                  //     });
                                  //   }
                                  // }
                                },
                              ),
                            ),
                    ),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
