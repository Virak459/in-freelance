import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/localization_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/wishlist_provider.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/deletion/deletion_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/home_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/widget/sign_out_confirmation_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/notification/notification_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/order_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/address_list_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/profile_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/seller/become_seller_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/setting/settings_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/version/version_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/wishlist/wishlist_screen.dart';
import 'package:provider/provider.dart';

class MoreScreen extends StatefulWidget {
  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  bool isGuestMode;
  List<Widget> _mores = [];
  @override
  void initState() {
    isGuestMode =
        !Provider.of<AuthProvider>(context, listen: false).isLoggedIn();
    if (!isGuestMode) {
      Provider.of<ProfileProvider>(context, listen: false).getUserInfo(context);
      // Provider.of<WishListProvider>(context, listen: false).initWishList(
      //   context,
      //   Provider.of<LocalizationProvider>(context, listen: false)
      //       .locale
      //       .countryCode,
      // );
      Provider.of<ProfileProvider>(context, listen: false)
          .initBecomeSellerPlan(context);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _mores = [
      TitleButton(
        name: getTranslated('Home', context),
        image: Images.home_image,
        navigateTo: HomePage(),
      ),
      TitleButton(
        name: ' Order ',
        image: Images.cart_image,
        navigateTo: OrderScreen(isBacButtonExist: true),
      ),
      Provider.of<AuthProvider>(context, listen: false).isLoggedIn()
          ? TitleButton(
              name: 'Favorite',
              image: Images.heart,
              navigateTo: AddressListScreen(),
            )
          : SizedBox.shrink(),
      Provider.of<AuthProvider>(context, listen: false).isLoggedIn()
          ? TitleButton(
              name: 'Notification',
              image: Images.bell,
              navigateTo: NotificationScreen(),
            )
          : SizedBox.shrink(),
      TitleButton(
        name: 'Setting',
        image: Images.settings,
        navigateTo: SettingsScreen(),
      ),
    ];

    if (Provider.of<SplashProvider>(context, listen: false)
            .configModel
            .allowDeleteAccount &&
        Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
      _mores.add(TitleButton(
        name: 'Edit Profile',
        image: Images.edit_profile,
        navigateTo: DeletionAccount(),
      ));
    }

    if (Provider.of<SplashProvider>(context, listen: false)
        .configModel
        .allowAppUpdate) {
      _mores.add(TitleButton(
        name: getTranslated('UPDATE_APP', context),
        image: Images.splash_logo,
        navigateTo: UpdateVersion(),
      ));
    }

    _mores.add(Provider.of<AuthProvider>(context, listen: false).isLoggedIn()
        ? TitleButton(
            name: getTranslated('sign_out', context),
            image: Images.signout,
            onTap: () {
              showAnimatedDialog(context, SignOutConfirmationDialog(),
                  isFlip: true);
            })
        : SizedBox.shrink());

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        toolbarHeight: 0,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Consumer<ProfileProvider>(
              builder: (context, profile, child) {
                return Container(
                  padding: EdgeInsets.all(Dimensions.FONT_SIZE_LARGE),
                  height: 110,
                  color: ColorResources.getSecondary(context),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 60,
                              height: 60,
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: Colors.white,
                                ),
                                borderRadius: BorderRadius.circular(100),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(55),
                                child: isGuestMode &&
                                            profile.userInfoModel == null ||
                                        profile.userInfoModel?.image == null ||
                                        profile.userInfoModel?.image ==
                                            "def.png"
                                    ? Image.asset(
                                        Images.logo_image,
                                        width: double.infinity,
                                        height: double.infinity,
                                        fit: BoxFit.cover,
                                      )
                                    : CachedNetworkImage(
                                        width: double.infinity,
                                        height: double.infinity,
                                        fit: BoxFit.cover,
                                        imageUrl:
                                            '${Provider.of<SplashProvider>(context, listen: false).baseUrls.customerImageUrl}/${profile.userInfoModel.image}',
                                        placeholder: (c, o) => CircleAvatar(
                                          backgroundColor: Colors.white,
                                          child: Image.asset(
                                            Images.logo_image,
                                            width: double.infinity,
                                            height: double.infinity,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                              ),
                            ),
                            SizedBox(
                              width: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: MediaQuery.of(context).size.width -
                                      (Responsive.isMobile(context)
                                          ? 255
                                          : 355),
                                  child: Text(
                                    !isGuestMode
                                        ? profile.userInfoModel != null
                                            ? '${profile.userInfoModel.fName} ${profile.userInfoModel.lName}'
                                            : 'Full Name'
                                        : 'Guest',
                                    maxLines: 1,
                                    style: titilliumBold.copyWith(
                                      overflow: TextOverflow.ellipsis,
                                      color: ColorResources.WHITE,
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_EXTRA_LARGE
                                          : Dimensions.FONT_SIZE_SMALL_IPAD,
                                    ),
                                  ),
                                ),
                                !isGuestMode &&
                                        profile.userInfoModel != null &&
                                        profile.userInfoModel.phone.isNotEmpty
                                    ? Text(
                                        '${profile.userInfoModel.phone}',
                                        style: titilliumRegular.copyWith(
                                          color: ColorResources.WHITE,
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_EXTRA_LARGE
                                              : Dimensions.FONT_SIZE_SMALL_IPAD,
                                        ),
                                      )
                                    : SizedBox.shrink(),
                              ],
                            ),
                          ],
                        ),
                        !isGuestMode
                            ? InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => BecomeSellerScreen()),
                                  );
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    horizontal:
                                        Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                    vertical:
                                        Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                  ),
                                  decoration:
                                      BoxDecoration(color: Colors.white),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.edit_calendar_sharp,
                                        color: ColorResources.getSecondary(
                                            context),
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                      ),
                                      SizedBox(
                                        width:
                                            Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                      ),
                                      Text(
                                        getTranslated('become_seller', context),
                                        style: titilliumSemiBold.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions.FONT_SIZE_SMALL_IPAD,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : SizedBox.shrink()
                      ]),
                );
              },
            ),
            Container(
              margin: const EdgeInsets.all(
                Dimensions.PADDING_SIZE_LARGE,
              ),
              child: GridView.count(
                physics: NeverScrollableScrollPhysics(),
                crossAxisCount: 2,
                childAspectRatio:
                    Responsive.isMobile(context) ? (1 / .3) : (1 / .2),
                mainAxisSpacing: 15,
                crossAxisSpacing: 15,
                shrinkWrap: true,
                children: _mores,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TitleButton extends StatelessWidget {
  final String name;
  final String image;
  final Widget navigateTo;
  final Function onTap;
  const TitleButton(
      {Key key,
      @required this.name,
      @required this.image,
      this.navigateTo,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap ??
          () {
            return Navigator.push(
              context,
              /*PageRouteBuilder(
            transitionDuration: Duration(seconds: 1),
            pageBuilder: (context, animation, secondaryAnimation) => navigateTo,
            transitionsBuilder: (context, animation, secondaryAnimation, child) {
              animation = CurvedAnimation(parent: animation, curve: Curves.bounceInOut);
              return ScaleTransition(scale: animation, child: child, alignment: Alignment.center);
            },
          ),*/
              MaterialPageRoute(builder: (_) => navigateTo),
            );
          },
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: Dimensions.PADDING_SIZE_DEFAULT,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius:
              BorderRadius.circular(Dimensions.PADDING_SIZE_EXTRA_SMALL),
          border: Border.all(
            color: Colors.grey.withOpacity(.2),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 2,
              blurRadius: 5,
            )
          ],
        ),
        child: Row(
          children: [
            Image.asset(
              image,
              width: Responsive.isMobile(context)
                  ? Dimensions.ICON_SIZE_DEFAULT
                  : Dimensions.ICON_SIZE_DEFAULT_IPAD,
              color: ColorResources.getSecondary(context),
            ),
            SizedBox(
              width: Dimensions.PADDING_SIZE_EXTRA_SMALL,
            ),
            SizedBox(
              width: Responsive.isMobile(context)
                  ? MediaQuery.of(context).size.width / (2 * 2)
                  : MediaQuery.of(context).size.width / (2 * 1.4),
              child: Text(
                name,
                overflow: TextOverflow.ellipsis,
                style: titilliumBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_EXTRA_LARGE
                      : Dimensions.FONT_SIZE_EXTRA_LARGE_IPAD,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
