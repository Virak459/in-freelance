// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/product_type.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/banner_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/brand_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/featured_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/home_category_product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/brand/all_brand_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/category/all_category_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/flashdeal/all_flash_deal_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/admin_product_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/banners_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/brand_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/category_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/featured_product_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/products_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/promotion_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/shop_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/shop_view1.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/view_all_product_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/search_shop_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/all_shop_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/wishlist/wishlist_screen.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ScrollController _scrollController = ScrollController();
  ConfigModel configModel;
  Future<void> _loadData(BuildContext context, bool reload) async {
    await Provider.of<SplashProvider>(context, listen: false)
        .initConfig(context);
    await Provider.of<BannerProvider>(context, listen: false)
        .getBannerList(reload, context);

    await Provider.of<BannerProvider>(context, listen: false)
        .getFooterBannerList(context);
    await Provider.of<CategoryProvider>(context, listen: false)
        .getCategoryList(reload, context);
    await Provider.of<HomeCategoryProductProvider>(context, listen: false)
        .getHomeCategoryProductList(reload, context);
    await Provider.of<ShopsProvider>(context, listen: false)
        .getShopsList(reload, context);

    // if (configModel.showProductFeaturedOnHomeMobile) {
    //   await Provider.of<ProductProvider>(context, listen: false)
    //       .getFeaturedProductList('1', context, reload: reload);
    //   await Provider.of<FeaturedDealProvider>(context, listen: false)
    //       .getFeaturedDealList(reload, context);
    // }
    // if (configModel.showProductFeaturedOnHomeMobile) {
    //   await Provider.of<ProductProvider>(context, listen: false)
    //       .getAdminProductList('1', context, reload: reload);
    // }
    if (configModel.showProductOnHomeMobile) {
      await Provider.of<ProductProvider>(context, listen: false)
          .getLatestProductList(1, context, reload: reload);
      await Provider.of<ProductProvider>(context, listen: false)
          .getLProductList('1', context, reload: reload);
    }
    if (configModel.showBrandOnHomeMobile) {
      await Provider.of<BrandProvider>(context, listen: false)
          .getBrandList(reload, context);
    }
  }

  void passData(int index, String title) {
    index = index;
    title = title;
  }

  @override
  void initState() {
    super.initState();
    Provider.of<FlashDealProvider>(context, listen: false)
        .getMegaDealList(true, context);
    configModel =
        Provider.of<SplashProvider>(context, listen: false).configModel;
    _loadData(context, false);
    if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
      Provider.of<CartProvider>(context, listen: false).uploadToServer(context);
      Provider.of<CartProvider>(context, listen: false).getCartDataAPI(context);
    } else {
      Provider.of<CartProvider>(context, listen: false).getCartData();
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    List<String> types = [
      getTranslated('new_arrival', context),
      getTranslated('top_product', context),
      getTranslated('best_selling', context)
    ];
    return Scaffold(
      backgroundColor: ColorResources.getHomeBg(context),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorResources.getSecondary(context),
        toolbarHeight: 0,
      ),
      body: SafeArea(
        child: RefreshIndicator(
          backgroundColor: ColorResources.getSecondary(context),
          color: Colors.white,
          onRefresh: () async {
            await _loadData(context, true);
            await Provider.of<FlashDealProvider>(context, listen: false)
                .getMegaDealList(true, context);

            return true;
          },
          child: CustomScrollView(
            controller: _scrollController,
            slivers: [
              // App Bar
              SliverAppBar(
                // pinned: true,
                toolbarHeight: Responsive.isMobile(context) ? 70 : 100,
                floating: true,
                elevation: 0,
                centerTitle: false,
                automaticallyImplyLeading: false,
                backgroundColor: ColorResources.getSecondary(context),
                title: InkWell(
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (_) => SearchShopScreen())),
                  child: Container(
                    padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      color: ColorResources.getGrey(context),
                      borderRadius:
                          BorderRadius.circular(Dimensions.PADDING_SIZE_SMALL),
                    ),
                    child: Row(children: [
                      Image.asset(
                        Images.search,
                        color: ColorResources.BLACK,
                        scale: Responsive.isMobile(context) ? 3 : 2,
                      ),
                      SizedBox(width: 5),
                      Text(
                        getTranslated('SEARCH_HINT', context),
                        style: robotoRegular.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_SMALL
                                : Dimensions.FONT_SIZE_SMALL_IPAD,
                            color: Theme.of(context).hintColor),
                      ),
                    ]),
                  ),
                ),

                actions: [
                  IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => WishListScreen(),
                        ),
                      );
                    },
                    icon: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Stack(clipBehavior: Clip.none, children: [
                        Image.asset(
                          Images.heart,
                          height: Dimensions.ICON_SIZE_DEFAULT,
                          width: Dimensions.ICON_SIZE_DEFAULT,
                          color: ColorResources.getTextTitle(context),
                        ),
                      ]),
                    ),
                  ),
                  IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => CartScreen()));
                    },
                    icon: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Stack(clipBehavior: Clip.none, children: [
                        Image.asset(
                          Images.cart_image,
                          height: Dimensions.ICON_SIZE_DEFAULT,
                          width: Dimensions.ICON_SIZE_DEFAULT,
                          color: ColorResources.getTextTitle(context),
                        ),
                        Positioned(
                          top: -4,
                          right: -4,
                          child: Consumer<CartProvider>(
                              builder: (context, cart, child) {
                            return CircleAvatar(
                              radius: 7,
                              backgroundColor: Colors.black,
                              child: Text(cart.cartList.length.toString(),
                                  style: titilliumSemiBold.copyWith(
                                    color: ColorResources.WHITE,
                                    fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                                  )),
                            );
                          }),
                        ),
                      ]),
                    ),
                  ),
                ],
              ),

              SliverToBoxAdapter(
                child: Column(
                  children: [
                    BannersView(),

                    // Category
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          Dimensions.PADDING_SIZE_SMALL,
                          10,
                          Dimensions.PADDING_SIZE_SMALL,
                          Dimensions.PADDING_SIZE_SMALL),
                      child: TitleRow(
                          title: getTranslated('ITEMS', context),
                          // title: "sdsd",
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => AllCategoryScreen()));
                          }),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          Dimensions.PADDING_SIZE_SMALL,
                          0,
                          Dimensions.PADDING_SIZE_SMALL,
                          Dimensions.PADDING_SIZE_SMALL),
                      child: CategoryView(isHomePage: true),
                    ),

                    // // Brand
                    // configModel.showBrandOnHomeMobile
                    //     ? Consumer<BrandProvider>(
                    //         builder: (context, brandProvider, child) {
                    //         return brandProvider.brandList.length > 0
                    //             ? Column(
                    //                 children: [
                    //                   Padding(
                    //                     padding: EdgeInsets.fromLTRB(
                    //                         Dimensions.PADDING_SIZE_SMALL,
                    //                         20,
                    //                         Dimensions.PADDING_SIZE_SMALL,
                    //                         Dimensions.PADDING_SIZE_SMALL),
                    //                     child: TitleRow(
                    //                       title:
                    //                           getTranslated('brand', context),
                    //                       onTap:
                    //                           brandProvider.brandList.length < 5
                    //                               ? null
                    //                               : () {
                    //                                   Navigator.push(
                    //                                       context,
                    //                                       MaterialPageRoute(
                    //                                           builder: (_) =>
                    //                                               AllBrandScreen()));
                    //                                 },
                    //                     ),
                    //                   ),
                    //                   Padding(
                    //                     padding: EdgeInsets.symmetric(
                    //                         horizontal:
                    //                             Dimensions.PADDING_SIZE_SMALL),
                    //                     child: BrandView(isHomePage: true),
                    //                   )
                    //                 ],
                    //               )
                    //             : SizedBox.shrink();
                    //       })
                    //     : SizedBox(),

                    // //top seller
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          Dimensions.PADDING_SIZE_SMALL,
                          20,
                          Dimensions.PADDING_SIZE_SMALL,
                          Dimensions.PADDING_SIZE_SMALL),
                      child: Column(
                        children: [
                          TitleRow(
                            title: Provider.of<SplashProvider>(context,
                                                listen: false)
                                            .configModel
                                            .shopTitle !=
                                        null &&
                                    Provider.of<SplashProvider>(context,
                                            listen: false)
                                        .configModel
                                        .shopTitle
                                        .toString()
                                        .isNotEmpty
                                ? Provider.of<SplashProvider>(context,
                                        listen: false)
                                    .configModel
                                    .shopTitle
                                : getTranslated('All Shops', context),
                            // AppConstants.APP_NAME,
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => AllShopScreen(),
                                ),
                              );
                            },
                          ),
                          Divider(),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: Dimensions.PADDING_SIZE_SMALL),
                      child: Builder(
                        builder: (context) {
                          switch (configModel.showShopStyle) {
                            case 1:
                              return ShopView1(isHomePage: true);
                              break;
                            default:
                              return ShopView(isHomePage: true);
                          }
                        },
                      ),
                    ),

                    // // Promotions
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          Dimensions.PADDING_SIZE_SMALL,
                          10,
                          Dimensions.PADDING_SIZE_SMALL,
                          Dimensions.PADDING_SIZE_SMALL),
                      child: TitleRow(
                          title: getTranslated('promotions', context),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => AllFlashDealScreen()));
                          }),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          Dimensions.PADDING_SIZE_SMALL,
                          0,
                          Dimensions.PADDING_SIZE_SMALL,
                          Dimensions.PADDING_SIZE_SMALL),
                      child: PromotionView(isHomePage: true),
                    ),
                    // //Admin Products
                    // configModel.showProductAdminOnHomeMobile
                    //     ? Column(
                    //         children: [
                    //           Padding(
                    //             padding: EdgeInsets.fromLTRB(
                    //                 Dimensions.PADDING_SIZE_SMALL,
                    //                 50,
                    //                 Dimensions.PADDING_SIZE_SMALL,
                    //                 Dimensions.PADDING_SIZE_SMALL),
                    //             child: TitleRow(
                    //                 title: getTranslated(
                    //                     'eshop_products', context),
                    //                 onTap: () {
                    //                   Navigator.push(
                    //                       context,
                    //                       MaterialPageRoute(
                    //                           builder: (_) => AllProductScreen(
                    //                               productType: ProductType
                    //                                   .ADMIN_PRODUCT)));
                    //                 }),
                    //           ),
                    //           Padding(
                    //             padding: EdgeInsets.symmetric(
                    //                 horizontal: Dimensions.PADDING_SIZE_SMALL),
                    //             child: AdminProductView(
                    //               scrollController: _scrollController,
                    //               isHome: true,
                    //             ),
                    //           ),
                    //         ],
                    //       )
                    //     : SizedBox(),
                    // //Featured Products
                    // configModel.showProductFeaturedOnHomeMobile
                    //     ? Column(
                    //         children: [
                    //           Padding(
                    //             padding: EdgeInsets.fromLTRB(
                    //                 Dimensions.PADDING_SIZE_SMALL,
                    //                 50,
                    //                 Dimensions.PADDING_SIZE_SMALL,
                    //                 Dimensions.PADDING_SIZE_SMALL),
                    //             child: TitleRow(
                    //                 title: getTranslated(
                    //                     'featured_products', context),
                    //                 onTap: () {
                    //                   Navigator.push(
                    //                       context,
                    //                       MaterialPageRoute(
                    //                           builder: (_) => AllProductScreen(
                    //                               productType: ProductType
                    //                                   .FEATURED_PRODUCT)));
                    //                 }),
                    //           ),
                    //           Padding(
                    //             padding: EdgeInsets.symmetric(
                    //                 horizontal: Dimensions.PADDING_SIZE_SMALL),
                    //             child: FeaturedProductView(
                    //               scrollController: _scrollController,
                    //               isHome: true,
                    //             ),
                    //           ),
                    //         ],
                    //       )
                    //     : SizedBox(),
                    // configModel.showProductOnHomeMobile
                    //     ? Consumer<ProductProvider>(
                    //         builder: (ctx, prodProvider, child) {
                    //         return Column(
                    //           children: [
                    //             Padding(
                    //               padding: EdgeInsets.fromLTRB(
                    //                   Dimensions.PADDING_SIZE_SMALL,
                    //                   50,
                    //                   Dimensions.PADDING_SIZE_SMALL,
                    //                   Dimensions.PADDING_SIZE_SMALL),
                    //               child: Consumer<ProductProvider>(
                    //                   builder: (ctx, prodProvider, child) {
                    //                 return Row(
                    //                   children: [
                    //                     Expanded(
                    //                       child: Container(
                    //                         padding: const EdgeInsets.only(
                    //                             right: Dimensions
                    //                                 .PADDING_SIZE_DEFAULT),
                    //                         child: Text(
                    //                             prodProvider.title == 'xyz'
                    //                                 ? getTranslated(
                    //                                     'explore_more_products',
                    //                                     context)
                    //                                 : prodProvider.title,
                    //                             style: robotoBold.copyWith(
                    //                               fontSize: Responsive.isMobile(
                    //                                       context)
                    //                                   ? Dimensions
                    //                                       .FONT_SIZE_DEFAULT
                    //                                   : Dimensions
                    //                                       .FONT_SIZE_DEFAULT_IPAD,
                    //                             )),
                    //                       ),
                    //                     ),
                    //                     prodProvider.latestProductList != null
                    //                         ? PopupMenuButton(
                    //                             itemBuilder: (context) {
                    //                               return [
                    //                                 PopupMenuItem(
                    //                                     value: ProductType
                    //                                         .NEW_ARRIVAL,
                    //                                     child: Text(
                    //                                         getTranslated(
                    //                                             'new_arrival',
                    //                                             context)),
                    //                                     textStyle: robotoRegular
                    //                                         .copyWith(
                    //                                       color: Colors.black,
                    //                                       fontSize: Responsive
                    //                                               .isMobile(
                    //                                                   context)
                    //                                           ? Dimensions
                    //                                               .FONT_SIZE_SMALL
                    //                                           : Dimensions
                    //                                               .FONT_SIZE_SMALL_IPAD,
                    //                                     )),
                    //                                 PopupMenuItem(
                    //                                     value: ProductType
                    //                                         .TOP_PRODUCT,
                    //                                     child: Text(
                    //                                         getTranslated(
                    //                                             'top_product',
                    //                                             context)),
                    //                                     textStyle: robotoRegular
                    //                                         .copyWith(
                    //                                       color: Colors.black,
                    //                                       fontSize: Responsive
                    //                                               .isMobile(
                    //                                                   context)
                    //                                           ? Dimensions
                    //                                               .FONT_SIZE_SMALL
                    //                                           : Dimensions
                    //                                               .FONT_SIZE_SMALL_IPAD,
                    //                                     )),
                    //                                 PopupMenuItem(
                    //                                     value: ProductType
                    //                                         .BEST_SELLING,
                    //                                     child: Text(
                    //                                         getTranslated(
                    //                                             'best_selling',
                    //                                             context)),
                    //                                     textStyle: robotoRegular
                    //                                         .copyWith(
                    //                                       color: Colors.black,
                    //                                       fontSize: Responsive
                    //                                               .isMobile(
                    //                                                   context)
                    //                                           ? Dimensions
                    //                                               .FONT_SIZE_SMALL
                    //                                           : Dimensions
                    //                                               .FONT_SIZE_SMALL_IPAD,
                    //                                     )),
                    //                               ];
                    //                             },
                    //                             shape: RoundedRectangleBorder(
                    //                                 borderRadius: BorderRadius
                    //                                     .circular(Dimensions
                    //                                         .PADDING_SIZE_SMALL)),
                    //                             child: Padding(
                    //                               padding: EdgeInsets.symmetric(
                    //                                   horizontal: Dimensions
                    //                                       .PADDING_SIZE_SMALL),
                    //                               child: Icon(
                    //                                 Icons.filter_list,
                    //                                 size: Responsive.isMobile(
                    //                                         context)
                    //                                     ? Dimensions
                    //                                         .FONT_SIZE_SMALL
                    //                                     : Dimensions
                    //                                         .FONT_SIZE_SMALL_IPAD,
                    //                               ),
                    //                             ),
                    //                             onSelected: (value) {
                    //                               if (value ==
                    //                                   ProductType.NEW_ARRIVAL) {
                    //                                 Provider.of<ProductProvider>(
                    //                                         context,
                    //                                         listen: false)
                    //                                     .changeTypeOfProduct(
                    //                                         value, types[0]);
                    //                               } else if (value ==
                    //                                   ProductType.TOP_PRODUCT) {
                    //                                 Provider.of<ProductProvider>(
                    //                                         context,
                    //                                         listen: false)
                    //                                     .changeTypeOfProduct(
                    //                                         value, types[1]);
                    //                               } else if (value ==
                    //                                   ProductType
                    //                                       .BEST_SELLING) {
                    //                                 Provider.of<ProductProvider>(
                    //                                         context,
                    //                                         listen: false)
                    //                                     .changeTypeOfProduct(
                    //                                         value, types[2]);
                    //                               }

                    //                               Padding(
                    //                                 padding: EdgeInsets.symmetric(
                    //                                     horizontal: Dimensions
                    //                                         .PADDING_SIZE_SMALL),
                    //                                 child: ProductView(
                    //                                     isHomePage: false,
                    //                                     productType: value,
                    //                                     scrollController:
                    //                                         _scrollController),
                    //                               );
                    //                               Provider.of<ProductProvider>(
                    //                                       context,
                    //                                       listen: false)
                    //                                   .getLatestProductList(
                    //                                       1, context,
                    //                                       reload: true);
                    //                             })
                    //                         : SizedBox(),
                    //                   ],
                    //                 );
                    //               }),
                    //             ),
                    //             Padding(
                    //               padding: EdgeInsets.symmetric(
                    //                   horizontal:
                    //                       Dimensions.PADDING_SIZE_SMALL),
                    //               child: ProductView(
                    //                   isHomePage: false,
                    //                   productType: ProductType.NEW_ARRIVAL,
                    //                   scrollController: _scrollController),
                    //             ),
                    //           ],
                    //         );
                    //       })
                    //     : SizedBox(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class SliverDelegate extends SliverPersistentHeaderDelegate {
  Widget child;
  SliverDelegate({@required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => 50;

  @override
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverDelegate oldDelegate) {
    return oldDelegate.maxExtent != 50 ||
        oldDelegate.minExtent != 50 ||
        child != oldDelegate.child;
  }
}
