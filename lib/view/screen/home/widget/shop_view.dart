// ignore_for_file: non_constant_identifier_names

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_orderby_app_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_screen.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class ShopView extends StatelessWidget {
  final bool isHomePage;
  ShopView({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<ShopsProvider>(
      builder: (context, shopProvider, child) {
        return shopProvider.shopList != null &&
                shopProvider.shopList.length != 0
            ? Container(
                // color: Colors.black,
                height: Responsive.isMobile(context)
                    ? App.height(context) * 18
                    : App.height(context) * 30,
                width: MediaQuery.of(context).size.width,
                child: GridView.count(
                  shrinkWrap: true,
                  physics: AlwaysScrollableScrollPhysics(),
                  primary: false,
                  childAspectRatio: 1 / .8,
                  scrollDirection: Axis.horizontal,
                  addAutomaticKeepAlives: true,
                  crossAxisSpacing: Dimensions.PADDING_SIZE_SMALL,
                  mainAxisSpacing: Dimensions.PADDING_SIZE_SMALL,
                  crossAxisCount: 1,
                  children:
                      new List.generate(shopProvider.shopList.length, (index) {
                    return ShopStyleItems(context,
                        shopProvider: shopProvider, index: index);
                  }),
                ),
              )
            : CategoryShimmer();
      },
    );
  }
}

Widget ShopStyleItems(BuildContext context,
    {ShopsProvider shopProvider, int index}) {
  return InkWell(
    onTap: () {
      ShopModel shopModel = shopProvider.shopList[index];
      if (shopModel.seller.productOrderType == AppConstants.DIRECT_TO_SHOP) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => ShopScreen(shopModel: shopProvider.shopList[index]),
          ),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) =>
                ShopOrderByAppScreen(shopModel: shopProvider.shopList[index]),
          ),
        );
      }
    },
    child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius:
                BorderRadius.circular(Dimensions.PADDING_SIZE_EXTRA_SMALL),
            color: Theme.of(context).highlightColor,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 5)
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(
              Dimensions.PADDING_SIZE_EXTRA_SMALL,
            ),
            child: CachedNetworkImage(
              height: Responsive.isMobile(context)
                  ? App.height(context) * 14
                  : App.height(context) * 24,
              width: Responsive.isMobile(context)
                  ? App.height(context) * 14
                  : App.height(context) * 24,
              imageUrl:
                  '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}'
                  '/${shopProvider.shopList[index].image}',
              placeholder: (c, o) => Image.asset(
                Images.placeholder,
                fit: BoxFit.cover,
              ),
              fit: BoxFit.fitHeight,
            ),
          ),
        ),
        SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
        Text(
          shopProvider.shopList[index].name,
          overflow: TextOverflow.ellipsis,
          style: titilliumRegular.copyWith(
            fontSize: Responsive.isMobile(context)
                ? Dimensions.FONT_SIZE_SMALL
                : Dimensions.FONT_SIZE_SMALL_IPAD,
          ),
        ),
      ],
    ),
  );
}

class CategoryShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: (1 / 1),
      ),
      itemCount: 3,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: Colors.grey[
                    Provider.of<ThemeProvider>(context).darkTheme ? 700 : 200],
                spreadRadius: 2,
                blurRadius: 5)
          ]),
          margin: EdgeInsets.all(3),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              flex: 7,
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled:
                    Provider.of<ShopsProvider>(context).shopList.length == 0,
                child: Container(
                    decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                )),
              ),
            ),
            Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: ColorResources.getTextBg(context),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                  ),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    enabled:
                        Provider.of<ShopsProvider>(context).shopList.length ==
                            0,
                    child: Container(
                        height: 10,
                        color: Colors.white,
                        margin: EdgeInsets.only(left: 15, right: 15)),
                  ),
                )),
          ]),
        );
      },
    );
  }
}
