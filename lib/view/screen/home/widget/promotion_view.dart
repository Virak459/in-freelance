// ignore_for_file: non_constant_identifier_names

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/flash_deal_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/flashdeal/flash_deal_screen.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class PromotionView extends StatelessWidget {
  final bool isHomePage;
  PromotionView({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<FlashDealProvider>(
      builder: (context, flashDealProvider, child) {
        return flashDealProvider.flashDeals != null &&
                flashDealProvider.flashDeals.length != 0
            ? Container(
                height: Responsive.isMobile(context)
                    ? App.height(context) * 20
                    : MediaQuery.of(context).size.width * (5 / 13),
                width: MediaQuery.of(context).size.width,
                // alignment: Alignment.center,
                // color: Colors.red,
                child: GridView.count(
                  // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //   crossAxisCount: 5,
                  //   childAspectRatio: (1 / 1),
                  // ),
                  // itemCount: isHomePage
                  //     ? categoryProvider.categoryList.length > 10
                  //         ? 10
                  //         : categoryProvider.categoryList.length
                  //     : categoryProvider.categoryList.length,
                  shrinkWrap: true,
                  physics: AlwaysScrollableScrollPhysics(),
                  // padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  primary: false,
                  childAspectRatio: 1 / .8,
                  scrollDirection: Axis.horizontal,
                  addAutomaticKeepAlives: true,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 1,
                  children: new List.generate(
                      flashDealProvider.flashDeals.length, (index) {
                    return PromotionStyleItems(context,
                        flashDealModel: flashDealProvider.flashDeals[index]);
                  }),
                  // itemBuilder: (BuildContext context, int index) {

                  // },
                ),
              )
            : CategoryShimmer();
      },
    );
  }
}

Widget PromotionStyleItems(BuildContext context,
    {FlashDealModel flashDealModel}) {
  return InkWell(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => FlashDealScreen(flashDealModel: flashDealModel),
        ),
      );
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Theme.of(context).highlightColor,
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 5)
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(
          Dimensions.PADDING_SIZE_SMALL,
        ),
        child: CachedNetworkImage(
          imageUrl:
              '${Provider.of<SplashProvider>(context, listen: false).baseUrls.reviewImageUrl}/deal/${flashDealModel.banner}',
          placeholder: (c, o) => Image.asset(
            Images.placeholder,
            fit: BoxFit.cover,
          ),
          fit: BoxFit.fitHeight,
        ),
      ),
    ),
  );
}

class CategoryShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: (1 / 1),
      ),
      itemCount: 3,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: Colors.grey[
                    Provider.of<ThemeProvider>(context).darkTheme ? 700 : 200],
                spreadRadius: 2,
                blurRadius: 5)
          ]),
          margin: EdgeInsets.all(3),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              flex: 7,
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled:
                    Provider.of<FlashDealProvider>(context).flashDeals.length ==
                        0,
                child: Container(
                    decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                )),
              ),
            ),
            Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: ColorResources.getTextBg(context),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                  ),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    enabled: Provider.of<FlashDealProvider>(context)
                            .flashDeals
                            .length ==
                        0,
                    child: Container(
                        height: 10,
                        color: Colors.white,
                        margin: EdgeInsets.only(left: 15, right: 15)),
                  ),
                )),
          ]),
        );
      },
    );
  }
}
