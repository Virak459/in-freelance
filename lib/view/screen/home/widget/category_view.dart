// ignore_for_file: non_constant_identifier_names

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/shop/shop_by_category_screen.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class CategoryView extends StatelessWidget {
  final bool isHomePage;
  CategoryView({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<CategoryProvider>(
      builder: (context, categoryProvider, child) {
        return categoryProvider.categoryList.length != 0
            ? Container(
                height: Responsive.isMobile(context)
                    ? App.height(context) * 32
                    : MediaQuery.of(context).size.width * (4 / 5.5),
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                // color: Colors.red,
                child: GridView.count(
                  // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //   crossAxisCount: 5,
                  //   childAspectRatio: (1 / 1),
                  // ),
                  // itemCount: isHomePage
                  //     ? categoryProvider.categoryList.length > 10
                  //         ? 10
                  //         : categoryProvider.categoryList.length
                  //     : categoryProvider.categoryList.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  // padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                  primary: false,
                  childAspectRatio: 1 / 1,
                  scrollDirection: Axis.horizontal,
                  addAutomaticKeepAlives: true,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 3,

                  children: new List.generate(
                      isHomePage && categoryProvider.categoryList.length > 12
                          ? 12
                          : categoryProvider.categoryList.length, (index) {
                    return CategoryStyleItems(context,
                        categoryProvider: categoryProvider, index: index);
                  }),
                  // itemBuilder: (BuildContext context, int index) {

                  // },
                ),
              )
            : CategoryShimmer();
      },
    );
  }
}

Widget CategoryStyleItems(BuildContext context,
    {CategoryProvider categoryProvider, int index}) {
  return InkWell(
    onTap: () {
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //     builder: (_) => BrandAndCategoryProductScreen(
      //       isBrand: false,
      //       id: categoryProvider.categoryList[index].id.toString(),
      //       name: categoryProvider.categoryList[index].name,
      //     ),
      //   ),
      // );
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => ShopByCategory(
            category: categoryProvider.categoryList[index],
          ),
        ),
      );
    },
    child: Container(
      // padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(Dimensions.PADDING_SIZE_SMALL),
      //   color: Theme.of(context).highlightColor,
      //   boxShadow: [BoxShadow(
      //     color: Colors.grey.withOpacity(0.3),
      //     spreadRadius: 1,
      //     blurRadius: 3,
      //     offset: Offset(0, 3), // changes position of shadow
      //   )],
      // ),
      // padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
      // color: Colors.red,
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Expanded(
          flex: 3,
          child: Container(
            decoration: BoxDecoration(
              color:
                  Provider.of<ThemeProvider>(context, listen: false).darkTheme
                      ? HexColor('ed1848')
                      : HexColor('eee6fe'),
              borderRadius: BorderRadius.circular(
                Dimensions.PADDING_SIZE_SMALL,
              ),
            ),
            //padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
            child: CachedNetworkImage(
              fit: BoxFit.contain,
              imageUrl:
                  '${Provider.of<SplashProvider>(context, listen: false).baseUrls.categoryImageUrl}'
                  '/${categoryProvider.categoryList[index].icon}',
              placeholder: (c, o) => Image.asset(
                Images.placeholder,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
              ),
              alignment: Alignment.center,
              child: Container(
                width: double.infinity,
                child: Text(
                  categoryProvider.categoryList.length != 0
                      ? categoryProvider.categoryList[index].name
                      : getTranslated('CATEGORY', context),
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_SMALL
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                  ),
                ),
              ),

              // Text(
              //   categoryProvider.categoryList.length != 0
              //       ? categoryProvider.categoryList[index].name
              //       : getTranslated('CATEGORY', context),
              //   textAlign: TextAlign.center,
              //   maxLines: 1,
              //   overflow: TextOverflow.ellipsis,
              //   style: titilliumSemiBold.copyWith(
              //       fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL),
              // ),
            )),
      ]),
    ),
  );
}

class CategoryShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        childAspectRatio: (1 / 1),
      ),
      itemCount: 12,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: Colors.grey[
                    Provider.of<ThemeProvider>(context).darkTheme ? 700 : 200],
                spreadRadius: 2,
                blurRadius: 5)
          ]),
          margin: EdgeInsets.all(3),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              flex: 7,
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: Provider.of<CategoryProvider>(context)
                        .categoryList
                        .length ==
                    0,
                child: Container(
                    decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                )),
              ),
            ),
            Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: ColorResources.getTextBg(context),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                  ),
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    enabled: Provider.of<CategoryProvider>(context)
                            .categoryList
                            .length ==
                        0,
                    child: Container(
                        height: 10,
                        color: Colors.white,
                        margin: EdgeInsets.only(left: 15, right: 15)),
                  ),
                )),
          ]),
        );
      },
    );
  }
}
