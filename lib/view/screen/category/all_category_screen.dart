import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/category.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/shop/shop_by_category_screen.dart';
import 'package:provider/provider.dart';

class AllCategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('All_CATEGORY', context)),
      ),
      body: Column(
        children: [
          Expanded(child: Consumer<CategoryProvider>(
            builder: (context, categoryProvider, child) {
              print(
                  "CheckCategoryLength ${categoryProvider.categoryList.length}");
              return categoryProvider.categoryList.length != 0
                  ? Row(children: [
                      Container(
                        width: Responsive.isMobile(context) ? 120 : 200,
                        margin: EdgeInsets.only(top: 3),
                        height: double.infinity,
                        decoration: BoxDecoration(
                          color: Theme.of(context).highlightColor,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[
                                    Provider.of<ThemeProvider>(context)
                                            .darkTheme
                                        ? 700
                                        : 200],
                                spreadRadius: 1,
                                blurRadius: 1)
                          ],
                        ),
                        child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: categoryProvider.categoryList.length,
                          padding: EdgeInsets.all(0),
                          itemBuilder: (context, index) {
                            Category _category =
                                categoryProvider.categoryList[index];
                            return InkWell(
                              onTap: () {
                                Provider.of<CategoryProvider>(context,
                                        listen: false)
                                    .changeSelectedIndex(index);
                              },
                              child: CategoryItem(
                                title: _category.name,
                                icon: _category.icon,
                                isSelected:
                                    categoryProvider.categorySelectedIndex ==
                                        index,
                              ),
                            );
                          },
                        ),
                      ),
                      Expanded(
                          child: ListView.builder(
                        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                        itemCount: categoryProvider
                                .categoryList[
                                    categoryProvider.categorySelectedIndex]
                                .subCategories
                                .length +
                            1,
                        itemBuilder: (context, index) {
                          print(
                              "CheckCategoryLength ${categoryProvider.categoryList[categoryProvider.categorySelectedIndex].subCategories}");
                          SubCategory _subCategory;
                          if (index != 0) {
                            _subCategory = categoryProvider
                                .categoryList[
                                    categoryProvider.categorySelectedIndex]
                                .subCategories[index - 1];
                          }
                          if (index == 0) {
                            return Ink(
                              color: Theme.of(context).highlightColor,
                              child: ListTile(
                                title: Text(getTranslated('VIEW_SHOP', context),
                                    style: titilliumSemiBold.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis),
                                trailing: Icon(Icons.navigate_next),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => ShopByCategory(
                                          category:
                                              categoryProvider.categoryList[
                                                  categoryProvider
                                                      .categorySelectedIndex]),
                                    ),
                                  );
                                },
                              ),
                            );
                          } else if (_subCategory.subSubCategories.length !=
                              0) {
                            return Ink(
                              color: Theme.of(context).highlightColor,
                              child: Theme(
                                data: Provider.of<ThemeProvider>(context)
                                        .darkTheme
                                    ? ThemeData.dark()
                                    : ThemeData.light(),
                                child: ExpansionTile(
                                  key: Key(
                                      '${Provider.of<CategoryProvider>(context).categorySelectedIndex}$index'),
                                  title: Text(_subCategory.name,
                                      style: titilliumSemiBold.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis),
                                  children: _getSubSubCategories(
                                      context, _subCategory),
                                ),
                              ),
                            );
                          } else {
                            return Ink(
                              color: Theme.of(context).highlightColor,
                              child: ListTile(
                                title: Text(_subCategory.name,
                                    style: titilliumSemiBold.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis),
                                trailing: Icon(Icons.navigate_next,
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .color),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => ShopByCategory(
                                        category: Category(
                                          id: _subCategory.id,
                                          name: _subCategory.name,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                          }
                        },
                      )),
                    ])
                  : Center(
                      child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Theme.of(context).primaryColor)));
            },
          )),
        ],
      ),
    );
  }

  List<Widget> _getSubSubCategories(
      BuildContext context, SubCategory subCategory) {
    List<Widget> _subSubCategories = [];
    _subSubCategories.add(Container(
      color: ColorResources.getIconBg(context),
      margin:
          EdgeInsets.symmetric(horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
      child: ListTile(
        title: Row(
          children: [
            Container(
              height: 7,
              width: 7,
              decoration: BoxDecoration(
                  color: ColorResources.getPrimary(context),
                  shape: BoxShape.circle),
            ),
            SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
            Flexible(
                child: Text(
              getTranslated('VIEW_SHOP', context),
              style: titilliumSemiBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  color: Theme.of(context).textTheme.bodyText1.color),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            )),
          ],
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ShopByCategory(
                category: Category(
                  id: subCategory.id,
                  name: subCategory.name,
                ),
              ),
            ),
          );
        },
      ),
    ));
    for (int index = 0; index < subCategory.subSubCategories.length; index++) {
      _subSubCategories.add(Container(
        color: ColorResources.getIconBg(context),
        margin: EdgeInsets.symmetric(
            horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
        child: ListTile(
          title: Row(
            children: [
              Container(
                height: 7,
                width: 7,
                decoration: BoxDecoration(
                    color: ColorResources.getPrimary(context),
                    shape: BoxShape.circle),
              ),
              SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
              Flexible(
                  child: Text(
                subCategory.subSubCategories[index].name,
                style: titilliumSemiBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                    color: Theme.of(context).textTheme.bodyText1.color),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
            ],
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => ShopByCategory(
                  category: Category(
                    id: subCategory.subSubCategories[index].id,
                    name: subCategory.subSubCategories[index].name,
                  ),
                ),
              ),
            );
          },
        ),
      ));
    }
    return _subSubCategories;
  }
}

class CategoryItem extends StatelessWidget {
  final String title;
  final String icon;
  final bool isSelected;
  CategoryItem(
      {@required this.title, @required this.icon, @required this.isSelected});

  Widget build(BuildContext context) {
    return Container(
      width: Responsive.isMobile(context) ? 100 : 150,
      height: Responsive.isMobile(context) ? 100 : 150,
      margin: EdgeInsets.symmetric(
          vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL, horizontal: 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: isSelected ? ColorResources.getPrimary(context) : null,
      ),
      child: Center(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Container(
            height: Responsive.isMobile(context) ? 50 : 80,
            width: Responsive.isMobile(context) ? 50 : 80,
            decoration: BoxDecoration(
              // border: Border.all(
              //     width: 2,
              //     color: isSelected
              //         ? Theme.of(context).highlightColor
              //         : Theme.of(context).hintColor),
              borderRadius: BorderRadius.circular(10),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                fit: BoxFit.contain,
                imageUrl:
                    '${Provider.of<SplashProvider>(context, listen: false).baseUrls.categoryImageUrl}/$icon',
                placeholder: (c, o) =>
                    Image.asset(Images.placeholder, fit: BoxFit.cover),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
            child: Text(title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: titilliumSemiBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  color: isSelected
                      ? Theme.of(context).highlightColor
                      : Theme.of(context).hintColor,
                )),
          ),
        ]),
      ),
    );
  }
}
