import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/become_seller_plan_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class BecomeSellerScreen extends StatelessWidget {
  const BecomeSellerScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<ProfileProvider>(context, listen: false)
        .initBecomeSellerPlan(context);
    return Scaffold(
      appBar: AppBar(
        //centerTitle: true,
        elevation: 0,
        leading: Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
          child: CircleAvatar(
            backgroundColor: Colors.white,
            child: BackButton(
              color: ColorResources.getSecondary(context),
            ),
          ),
        ),
        toolbarHeight: 100,
        title: Container(
          child: Column(
            children: [
              Icon(
                Icons.shopping_basket_rounded,
                size: 100,
              ),
            ],
          ),
        ),
        backgroundColor: ColorResources.getSecondary(context),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              vertical: Dimensions.PADDING_SIZE_DEFAULT,
            ),
            width: MediaQuery.of(context).size.width,
            color: ColorResources.getSecondary(context),
            child: Center(
              child: Text(
                getTranslated(
                    "Get access to the best plan deal of Marketplace", context),
                textAlign: TextAlign.center,
                style: titilliumRegular.copyWith(
                  color: Colors.white,
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                ),
              ),
            ),
          ),
          Expanded(
            child: RefreshIndicator(
              color: Colors.white,
              backgroundColor: ColorResources.getSecondary(context),
              onRefresh: () async {
                await Provider.of<ProfileProvider>(context, listen: false)
                    .initBecomeSellerPlan(context);
              },
              child:
                  Consumer<ProfileProvider>(builder: (context, profile, child) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    profile.becomeSellerPlanList.length > 0
                        ? Expanded(
                            child: ListView.builder(
                                itemCount: 1,
                                itemBuilder: ((context, i) {
                                  return Container(
                                    margin: EdgeInsets.symmetric(
                                      vertical:
                                          Dimensions.MARGIN_SIZE_EXTRA_LARGE,
                                    ),
                                    child: StaggeredGridView.countBuilder(
                                        itemCount:
                                            profile.becomeSellerPlanList.length,
                                        crossAxisCount: 2,
                                        mainAxisSpacing:
                                            Dimensions.PADDING_SIZE_DEFAULT,
                                        crossAxisSpacing:
                                            Dimensions.PADDING_SIZE_DEFAULT,
                                        addAutomaticKeepAlives: true,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: Dimensions
                                                .PADDING_SIZE_DEFAULT),
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        staggeredTileBuilder: (int index) =>
                                            StaggeredTile.fit(1),
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          BecomeSellerPlanModel
                                              becomeSellerPlanModel = profile
                                                  .becomeSellerPlanList[index];
                                          return Container(
                                            padding: EdgeInsets.all(Dimensions
                                                .PADDING_SIZE_DEFAULT),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(
                                                Dimensions
                                                    .FONT_SIZE_EXTRA_SMALL,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.2),
                                                    spreadRadius: 1,
                                                    blurRadius: 5)
                                              ],
                                            ),
                                            child: Column(
                                              children: [
                                                Text(
                                                  becomeSellerPlanModel.name
                                                      .toUpperCase(),
                                                  style: titilliumBold.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_DEFAULT
                                                        : Dimensions
                                                            .FONT_SIZE_DEFAULT_IPAD,
                                                  ),
                                                ),
                                                Container(
                                                  padding: EdgeInsets.symmetric(
                                                    horizontal: Dimensions
                                                        .PADDING_SIZE_LARGE,
                                                  ),
                                                  child: Divider(
                                                    thickness: 2,
                                                    color: ColorResources
                                                        .getSecondary(context),
                                                  ),
                                                ),
                                                Text(
                                                  PriceConverter.convertPrice(
                                                    context,
                                                    becomeSellerPlanModel.price
                                                        .toDouble(),
                                                    discount: 0.0,
                                                    discountType: 'amount',
                                                  ),
                                                  style: titilliumBold.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_LARGE
                                                        : Dimensions
                                                            .FONT_SIZE_LARGE_IPAD,
                                                  ),
                                                ),
                                                Html(
                                                  data: becomeSellerPlanModel
                                                      .description,
                                                  style: {
                                                    "body": Style(
                                                      fontSize: FontSize(
                                                        Responsive.isMobile(
                                                                context)
                                                            ? Dimensions
                                                                .FONT_SIZE_DEFAULT
                                                            : Dimensions
                                                                .FONT_SIZE_DEFAULT_IPAD,
                                                      ),
                                                    ),
                                                  },
                                                ),
                                                TextButton(
                                                  onPressed: () {
                                                    App.launchUrl(AppConstants
                                                            .REGISTER_VEDOR +
                                                        '?package_id=' +
                                                        becomeSellerPlanModel.id
                                                            .toString());
                                                  },
                                                  child: Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                      vertical: Dimensions
                                                          .PADDING_SIZE_EXTRA_SMALL,
                                                      horizontal: Dimensions
                                                          .PADDING_SIZE_DEFAULT,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        color: ColorResources
                                                            .getPrimary(
                                                                context),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                          Dimensions
                                                              .PADDING_SIZE_EXTRA_SMALL,
                                                        )),
                                                    child: Text(
                                                      getTranslated(
                                                          'Buy Now', context),
                                                      style: titilliumSemiBold.copyWith(
                                                          fontSize: Responsive
                                                                  .isMobile(
                                                                      context)
                                                              ? Dimensions
                                                                  .FONT_SIZE_DEFAULT
                                                              : Dimensions
                                                                  .FONT_SIZE_DEFAULT_IPAD,
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        }),
                                  );
                                })),
                          )
                        : NoInternetOrDataScreen(
                            isNoInternet: false,
                          )
                  ],
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
