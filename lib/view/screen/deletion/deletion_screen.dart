
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/auth_screen.dart';
import 'package:provider/provider.dart';

class DeletionAccount extends StatelessWidget {
  const DeletionAccount({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _confirmController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('Deletion', context)),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
          child: Column(
            children: [
              Container(
                child: Image.asset(
                  Images.splash_logo,
                  fit: BoxFit.fill,
                ),
              ),
              Text(
                getTranslated('delete_your_account', context),
                style: titilliumBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_OVER_LARGE
                      : Dimensions.FONT_SIZE_OVER_LARGE_IPAD,
                ),
              ),
              SizedBox(
                height: Dimensions.PADDING_SIZE_SMALL,
              ),
              Text(
                getTranslated('delete_account_info', context),
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_LARGE
                      : Dimensions.FONT_SIZE_LARGE_IPAD,
                ),
              ),
              SizedBox(
                height: Dimensions.PADDING_SIZE_SMALL,
              ),
              Text(
                getTranslated('to_delete_account_info', context),
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_LARGE
                      : Dimensions.FONT_SIZE_LARGE_IPAD,
                ),
              ),
              SizedBox(
                height: Dimensions.PADDING_SIZE_DEFAULT,
              ),
              CustomTextField(
                textInputType: TextInputType.name,
                hintText: getTranslated('type_here', context),
                controller: _confirmController,
              ),
              SizedBox(
                height: Dimensions.PADDING_SIZE_DEFAULT,
              ),
              CustomButton(
                buttonText: getTranslated('delete_my_account', context),
                onTap: () async {
                  if (_confirmController.text.isEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          getTranslated('type_word_to', context),
                          style: titilliumRegular.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          ),
                        ),
                        backgroundColor: ColorResources.RED));
                  } else if (_confirmController.text != 'CONFIRM_DELETE') {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          getTranslated('incorrect_word', context),
                          style: titilliumRegular.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          ),
                        ),
                        backgroundColor: ColorResources.RED));
                  } else {
                    await Provider.of<AuthProvider>(context, listen: false)
                        .deletion()
                        .then((isSuccess) {
                      Provider.of<AuthProvider>(context, listen: false)
                          .clearSharedData()
                          .then((condition) {
                        Navigator.pop(context);
                        Provider.of<ProfileProvider>(context, listen: false)
                            .clearHomeAddress();
                        Provider.of<ProfileProvider>(context, listen: false)
                            .clearOfficeAddress();
                        Provider.of<AuthProvider>(context, listen: false)
                            .clearSharedData();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => AuthScreen()),
                            (route) => false);
                      });
                    });
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
