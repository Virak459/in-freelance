import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/cart_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/widget/cart_widget.dart';
import 'package:provider/provider.dart';

class ListCart extends StatefulWidget {
  final bool fromCheckout;
  final int sellerId;
  ListCart({this.fromCheckout = false, this.sellerId = 1});

  @override
  _ListCartState createState() => _ListCartState();
}

class _ListCartState extends State<ListCart> {
  @override
  void initState() {
    if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
      Provider.of<CartProvider>(context, listen: false).getCartDataAPI(context);
      Provider.of<CartProvider>(context, listen: false).setCartData();

      if (Provider.of<SplashProvider>(context, listen: false)
              .configModel
              .shippingMethod !=
          'sellerwise_shipping') {
        Provider.of<CartProvider>(context, listen: false)
            .getAdminShippingMethodList(context);
      }
    }
    //Provider.of<CartProvider>(context, listen: false).getChosenShippingMethod(context);
    // Provider.of<CartProvider>(context, listen: false).getShippingMethod(context,widget.sellerId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(builder: (context, cart, child) {
      // ignore: unused_local_variable
      double amount = 0.0;
      // ignore: unused_local_variable
      double shippingAmount = 0.0;
      // ignore: unused_local_variable
      double discount = 0.0;
      // ignore: unused_local_variable
      double tax = 0.0;
      List<CartModel> cartList = [];
      cartList.addAll(cart.cartList);

      List<String> sellerList = [];
      List<CartModel> sellerGroupList = [];
      List<List<CartModel>> cartProductList = [];
      List<List<int>> cartProductIndexList = [];
      cartList.forEach((cart) {
        if (!sellerList.contains(cart.cartGroupId)) {
          sellerList.add(cart.cartGroupId);
          sellerGroupList.add(cart);
        }
      });

      sellerList.forEach((seller) {
        List<CartModel> cartLists = [];
        List<int> indexList = [];
        cartList.forEach((cart) {
          if (seller == cart.cartGroupId) {
            cartLists.add(cart);
            indexList.add(cartList.indexOf(cart));
          }
        });
        cartProductList.add(cartLists);
        cartProductIndexList.add(indexList);
      });

      if (cart.getData &&
          Provider.of<AuthProvider>(context, listen: false).isLoggedIn() &&
          Provider.of<SplashProvider>(context, listen: false)
                  .configModel
                  .shippingMethod ==
              'sellerwise_shipping') {
        Provider.of<CartProvider>(context, listen: false)
            .getShippingMethod(context, cartProductList);
      }

      for (int i = 0; i < cart.cartList.length; i++) {
        amount += (cart.cartList[i].price - cart.cartList[i].discount) *
            cart.cartList[i].quantity;
        discount += cart.cartList[i].discount * cart.cartList[i].quantity;
        tax += cart.cartList[i].tax * cart.cartList[i].quantity;
      }
      for (int i = 0; i < cart.chosenShippingList.length; i++) {
        shippingAmount += cart.chosenShippingList[i].shippingCost;
      }
      // amount += shippingAmount;

      return Column(children: [
        cart.isLoading
            ? Padding(
                padding: const EdgeInsets.only(top: 200.0),
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                      Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              )
            : sellerList.length != 0
                ? Column(
                    children: List.generate(
                      sellerList.length,
                      (index) {
                        return Padding(
                          padding: EdgeInsets.only(
                              bottom: Dimensions.PADDING_SIZE_LARGE),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                sellerGroupList[index].shopInfo.isNotEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                            sellerGroupList[index].shopInfo,
                                            textAlign: TextAlign.end,
                                            style: titilliumSemiBold.copyWith(
                                              fontSize:
                                                  Responsive.isMobile(context)
                                                      ? Dimensions
                                                          .FONT_SIZE_LARGE
                                                      : Dimensions
                                                          .FONT_SIZE_LARGE_IPAD,
                                            )),
                                      )
                                    : SizedBox(),
                                Container(
                                  padding: EdgeInsets.only(
                                      bottom: Dimensions.PADDING_SIZE_LARGE),
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).highlightColor,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(0.3),
                                            spreadRadius: 1,
                                            blurRadius: 3,
                                            offset: Offset(0, 3)),
                                      ]),
                                  child: Column(
                                    children: [
                                      ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        padding: EdgeInsets.all(0),
                                        itemCount:
                                            cartProductList[index].length,
                                        itemBuilder: (context, i) => CartWidget(
                                          cartModel: cartProductList[index][i],
                                          index: cartProductIndexList[index][i],
                                          fromCheckout: widget.fromCheckout,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ]),
                        );
                      },
                    ),
                  )
                : Expanded(child: NoInternetOrDataScreen(isNoInternet: false)),
      ]);
    });
  }
}
