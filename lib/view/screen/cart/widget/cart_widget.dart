import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/cart_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:provider/provider.dart';

class CartWidget extends StatelessWidget {
  final CartModel cartModel;
  final int index;
  final bool fromCheckout;
  const CartWidget(
      {Key key,
      this.cartModel,
      @required this.index,
      @required this.fromCheckout});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      // padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      decoration: BoxDecoration(
        border: Border.all(
          color: ColorResources.getSecondary(context),
        ),
        borderRadius: BorderRadius.circular(Dimensions.PADDING_SIZE_SMALL),
        color: Theme.of(context).highlightColor,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: Responsive.isMobile(context) ? 22 : 32,
                padding: EdgeInsets.symmetric(
                    horizontal: Dimensions.PADDING_SIZE_SMALL),
                decoration: BoxDecoration(
                    color: ColorResources.getSecondary(context),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                      bottomRight:
                          Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                    )),
                child: Text(
                  cartModel.brandName,
                  style: robotoRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                    color: Colors.white,
                  ),
                ),
              ),
              !fromCheckout
                  ? Container(
                      padding: EdgeInsets.symmetric(
                        vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                        horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                      ),
                      child: InkWell(
                        onTap: () {
                          if (Provider.of<AuthProvider>(context, listen: false)
                              .isLoggedIn()) {
                            Provider.of<CartProvider>(context, listen: false)
                                .removeFromCartAPI(context, cartModel.id);
                          } else {
                            Provider.of<CartProvider>(context, listen: false)
                                .removeFromCart(index);
                          }

                          if (Provider.of<CartProvider>(context, listen: false)
                                      .cartList
                                      .length ==
                                  0 ||
                              Provider.of<CartProvider>(context, listen: false)
                                      .cartList
                                      .length ==
                                  1) {
                            return Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (_) => CartScreen()),
                            );
                          }
                        },
                        child: Icon(
                          Icons.close,
                          color: ColorResources.getSecondary(context),
                          size: Responsive.isMobile(context)
                              ? Dimensions.ICON_SIZE_DEFAULT + 8
                              : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                    )
                  : SizedBox.shrink(),
            ],
          ),
          Container(
            padding: EdgeInsets.only(
              left: Dimensions.PADDING_SIZE_SMALL,
              bottom: Dimensions.PADDING_SIZE_SMALL,
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius:
                        BorderRadius.circular(Dimensions.PADDING_SIZE_SMALL),
                    child: CachedNetworkImage(
                      height: Responsive.isMobile(context) ? 100 : 150,
                      width: Responsive.isMobile(context) ? 100 : 150,
                      fit: BoxFit.cover,
                      imageUrl:
                          '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${cartModel.thumbnail}',
                      placeholder: (c, o) => Image.asset(
                        Images.placeholder,
                        height: Responsive.isMobile(context) ? 100 : 150,
                        width: Responsive.isMobile(context) ? 100 : 150,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: Dimensions.PADDING_SIZE_LARGE,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              cartModel.name,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: titilliumBold.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_LARGE
                                    : Dimensions.FONT_SIZE_LARGE_IPAD,
                                color: ColorResources.getHint(context),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              PriceConverter.convertPrice(context,
                                  cartModel.price - cartModel.discount),
                              style: titilliumSemiBold.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_LARGE
                                    : Dimensions.FONT_SIZE_LARGE_IPAD,
                                color: ColorResources.GREEN,
                              ),
                            ),
                          ),
                          cartModel.discount > 0 && cartModel.discount != null
                              ? Container(
                                  child: Text(
                                    PriceConverter.convertPrice(
                                        context, cartModel.price),
                                    maxLines: 1,
                                    style: robotoBold.copyWith(
                                      color: Theme.of(context).hintColor,
                                      decoration: TextDecoration.lineThrough,
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                  ),
                                )
                              : SizedBox.shrink(),
                          Provider.of<AuthProvider>(context, listen: false)
                                  .isLoggedIn()
                              ? Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      getTranslated('quantity', context),
                                      style: titilliumRegular.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_LARGE
                                            : Dimensions.FONT_SIZE_LARGE_IPAD,
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: Dimensions
                                                  .PADDING_SIZE_SMALL),
                                          child: QuantityButton(
                                              isIncrement: false,
                                              index: index,
                                              quantity: cartModel.quantity,
                                              maxQty: 20,
                                              cartModel: cartModel),
                                        ),
                                        Text(
                                          cartModel.quantity.toString(),
                                          style: titilliumRegular.copyWith(
                                            fontSize: Responsive.isMobile(
                                                    context)
                                                ? Dimensions
                                                    .FONT_SIZE_EXTRA_LARGE
                                                : Dimensions
                                                    .FONT_SIZE_EXTRA_LARGE_IPAD,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: Dimensions
                                                  .PADDING_SIZE_SMALL),
                                          child: QuantityButton(
                                              index: index,
                                              isIncrement: true,
                                              quantity: cartModel.quantity,
                                              maxQty: 20,
                                              cartModel: cartModel),
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              : SizedBox.shrink(),
                          (cartModel.variant != null &&
                                  cartModel.variant.isNotEmpty)
                              ? Padding(
                                  padding: EdgeInsets.only(
                                      top: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                                  child: Row(children: [
                                    Text(
                                        '${getTranslated('variations', context)}: ',
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_SMALL
                                              : Dimensions.FONT_SIZE_SMALL_IPAD,
                                        )),
                                    Flexible(
                                        child: Text(cartModel.variant,
                                            style: robotoBold.copyWith(
                                              fontSize:
                                                  Responsive.isMobile(context)
                                                      ? Dimensions
                                                          .FONT_SIZE_SMALL
                                                      : Dimensions
                                                          .FONT_SIZE_SMALL_IPAD,
                                            ))),
                                  ]),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

class QuantityButton extends StatelessWidget {
  final CartModel cartModel;
  final bool isIncrement;
  final int quantity;
  final int index;
  final int maxQty;
  QuantityButton(
      {@required this.isIncrement,
      @required this.quantity,
      @required this.index,
      @required this.maxQty,
      @required this.cartModel});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (!isIncrement && quantity > 1) {
          // Provider.of<CartProvider>(context, listen: false).setQuantity(false, index);
          Provider.of<CartProvider>(context, listen: false)
              .updateCartProductQuantity(
                  cartModel.id, cartModel.quantity - 1, context)
              .then((value) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                value.message,
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                ),
              ),
              backgroundColor: value.isSuccess ? Colors.green : Colors.red,
            ));
          });
        } else if (isIncrement && quantity < maxQty) {
          // Provider.of<CartProvider>(context, listen: false).setQuantity(true, index);
          Provider.of<CartProvider>(context, listen: false)
              .updateCartProductQuantity(
                  cartModel.id, cartModel.quantity + 1, context)
              .then((value) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                value.message,
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                ),
              ),
              backgroundColor: value.isSuccess ? Colors.green : Colors.red,
            ));
          });
        }
      },
      child: Container(
        height: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
        width: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
        decoration: BoxDecoration(
          border: Border.all(color: ColorResources.getSecondary(context)),
          borderRadius: BorderRadius.circular(100),
        ),
        child: Icon(
          isIncrement ? Icons.add : Icons.remove,
          color: isIncrement
              ? quantity >= maxQty
                  ? ColorResources.getGrey(context)
                  : ColorResources.getSecondary(context)
              : quantity > 1
                  ? ColorResources.getSecondary(context)
                  : ColorResources.getGrey(context),
          size: Responsive.isMobile(context)
              ? Dimensions.ICON_SIZE_SMALL
              : Dimensions.ICON_SIZE_SMALL_IPAD,
        ),
      ),
    );
  }
}
