import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/cart_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/guest_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/widget/cart_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/checkout/checkout_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/checkout/widget/shipping_method_bottom_sheet.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  final bool fromCheckout;
  final int sellerId;
  CartScreen({this.fromCheckout = false, this.sellerId = 1});

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  void initState() {
    if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
      Provider.of<CartProvider>(context, listen: false).getCartDataAPI(context);
      Provider.of<CartProvider>(context, listen: false).setCartData();

      // if (Provider.of<SplashProvider>(context, listen: false)
      //         .configModel
      //         .shippingMethod !=
      //     'sellerwise_shipping') {
      //   Provider.of<CartProvider>(context, listen: false)
      //       .getAdminShippingMethodList(context);
      // }
      Provider.of<ProfileProvider>(context, listen: false)
          .initAddressList(context);
    }
    //Provider.of<CartProvider>(context, listen: false).getChosenShippingMethod(context);
    // Provider.of<CartProvider>(context, listen: false).getShippingMethod(context,widget.sellerId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(builder: (context, cart, child) {
      double amount = 0.0;
      double shippingAmount = 0.0;
      // ignore: unused_local_variable
      double discount = 0.0;
      // ignore: unused_local_variable
      double tax = 0.0;
      List<CartModel> cartList = [];
      cartList.addAll(cart.cartList);
      List<String> sellerList = [];
      List<CartModel> sellerGroupList = [];
      List<List<CartModel>> cartProductList = [];
      List<List<int>> cartProductIndexList = [];
      cartList.forEach((cart) {
        if (!sellerList.contains(cart.cartGroupId)) {
          sellerList.add(cart.cartGroupId);
          sellerGroupList.add(cart);
        }
      });

      sellerList.forEach((seller) {
        List<CartModel> cartLists = [];
        List<int> indexList = [];
        cartList.forEach((cart) {
          if (seller == cart.cartGroupId) {
            cartLists.add(cart);
            indexList.add(cartList.indexOf(cart));
          }
        });
        cartProductList.add(cartLists);
        cartProductIndexList.add(indexList);
      });

      if (cart.getData &&
          Provider.of<AuthProvider>(context, listen: false).isLoggedIn() &&
          Provider.of<SplashProvider>(context, listen: false)
                  .configModel
                  .shippingMethod ==
              'sellerwise_shipping') {
        Provider.of<CartProvider>(context, listen: false)
            .getShippingMethod(context, cartProductList);
      }

      for (int i = 0; i < cart.cartList.length; i++) {
        amount += (cart.cartList[i].price - cart.cartList[i].discount) *
            cart.cartList[i].quantity;
        discount += cart.cartList[i].discount * cart.cartList[i].quantity;
        tax += cart.cartList[i].tax * cart.cartList[i].quantity;
      }
      for (int i = 0; i < cart.chosenShippingList.length; i++) {
        shippingAmount += cart.chosenShippingList[i].shippingCost;
      }
      // amount += shippingAmount;

      return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Provider.of<ThemeProvider>(context).darkTheme
              ? Colors.black
              : ColorResources.getSecondary(context),
          title: Text(getTranslated('CART', context)),
          actions: [
            Visibility(
              visible: false,
              child: IconButton(
                padding: EdgeInsets.zero,
                onPressed: () {},
                icon: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Stack(clipBehavior: Clip.none, children: [
                    Image.asset(
                      Images.cart_image,
                      height: Dimensions.ICON_SIZE_DEFAULT,
                      width: Dimensions.ICON_SIZE_DEFAULT,
                      color: ColorResources.getSecondary(context),
                    ),
                    Positioned(
                      top: -4,
                      right: -4,
                      child: Consumer<CartProvider>(
                          builder: (context, cart, child) {
                        return CircleAvatar(
                          radius: 7,
                          backgroundColor: Colors.black,
                          child: Text(cart.cartList.length.toString(),
                              style: titilliumSemiBold.copyWith(
                                color: ColorResources.WHITE,
                                fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                              )),
                        );
                      }),
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: cartList.length > 0 &&
                (!widget.fromCheckout && !cart.isLoading)
            ? Container(
                color: Colors.white,
                height: App.height(context) * 10,
                padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_LARGE,
                  vertical: Dimensions.PADDING_SIZE_DEFAULT,
                ),
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(
                    PriceConverter.convertPrice(
                        context, amount + shippingAmount),
                    style: titilliumSemiBold.copyWith(
                      fontSize: Responsive.isMobile(context)
                          ? Dimensions.FONT_SIZE_OVER_LARGE
                          : Dimensions.FONT_SIZE_OVER_LARGE_IPAD,
                      color: ColorResources.getGreen(context),
                    ),
                  ),
                  SizedBox(
                    width: Dimensions.PADDING_SIZE_EXTRA_LARGE,
                  ),
                  Builder(
                    builder: (context) => SizedBox(
                      height: App.height(context) * 6,
                      width: App.width(context) * 30,
                      child: TextButton(
                        onPressed: () {
                          if (Provider.of<AuthProvider>(context, listen: false)
                              .isLoggedIn()) {
                            if (cart.cartList.length == 0) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text(
                                  getTranslated(
                                      'select_at_least_one_product', context),
                                  style: titilliumRegular.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  ),
                                ),
                                backgroundColor: Colors.red,
                              ));
                            }
                            // else if (cart.chosenShippingList == null ||
                            //     cart.chosenShippingList.length == 0) {
                            //   Provider.of<CartProvider>(context,
                            //           listen: false)
                            //       .setSelectedShippingMethod(1, 0);
                            //   print(
                            //       "CheckShippingList ${cart.chosenShippingList.length}");
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (_) => CheckoutScreen(
                            //               cartList: cartList,
                            //               totalOrderAmount: amount,
                            //               shippingFee: shippingAmount,
                            //               discount: discount,
                            //               tax: tax,
                            //             )));
                            // ScaffoldMessenger.of(context).showSnackBar(
                            //     SnackBar(
                            //         content: Text(getTranslated(
                            //             'select_shipping_method', context)),
                            //         backgroundColor: Colors.red));
                            // }
                            else if (Provider.of<SplashProvider>(context,
                                        listen: false)
                                    .configModel
                                    .shippingMethodEnable &&
                                cart.chosenShippingList.length <
                                    cartProductList.length &&
                                Provider.of<SplashProvider>(context,
                                            listen: false)
                                        .configModel
                                        .shippingMethod ==
                                    'sellerwise_shipping') {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                      content: Text(
                                        getTranslated(
                                            'select_all_shipping_method',
                                            context),
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                        ),
                                      ),
                                      backgroundColor: Colors.red));
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => CheckoutScreen(
                                    cartList: cartList,
                                  ),
                                ),
                              );
                            }
                          } else {
                            showAnimatedDialog(context, GuestDialog(),
                                isFlip: true);
                          }
                        },
                        style: TextButton.styleFrom(
                          backgroundColor: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: Text(getTranslated('checkout', context),
                            style: titilliumSemiBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_LARGE
                                  : Dimensions.FONT_SIZE_LARGE_IPAD,
                              color: ColorResources.WHITE,
                            )),
                      ),
                    ),
                  ),
                ]),
              )
            : null,
        body: Column(children: [
          cart.isLoading
              ? Padding(
                  padding: const EdgeInsets.only(top: 200.0),
                  child: Center(
                    child: CircularProgressIndicator(
                      color: ColorResources.getSecondary(context),
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                )
              : sellerList.length != 0
                  ? Expanded(
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(
                                  Dimensions.PADDING_SIZE_SMALL,
                                  10,
                                  Dimensions.PADDING_SIZE_SMALL,
                                  Dimensions.PADDING_SIZE_SMALL),
                              child: TitleRow(
                                title: cart.cartList.length.toString() +
                                    ' ' +
                                    getTranslated('PRODUCTS', context),
                                onTap: () {},
                                isDetailsPage: true,
                              ),
                            ),
                            Expanded(
                              child: RefreshIndicator(
                                backgroundColor:
                                    ColorResources.getSecondary(context),
                                color: Colors.white,
                                onRefresh: () async {
                                  if (Provider.of<AuthProvider>(context,
                                          listen: false)
                                      .isLoggedIn()) {
                                    await Provider.of<CartProvider>(context,
                                            listen: false)
                                        .getCartDataAPI(context);
                                  }
                                },
                                child: ListView.builder(
                                  itemCount: sellerList.length,
                                  padding: EdgeInsets.all(0),
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: EdgeInsets.only(
                                          bottom:
                                              Dimensions.PADDING_SIZE_LARGE),
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                                height: Dimensions
                                                    .PADDING_SIZE_LARGE),
                                            sellerGroupList[index]
                                                    .shopInfo
                                                    .isNotEmpty
                                                ? Container(
                                                    padding: const EdgeInsets
                                                            .all(
                                                        Dimensions
                                                            .PADDING_SIZE_SMALL),
                                                    child: Text(
                                                        sellerGroupList[index]
                                                            .shopInfo,
                                                        textAlign:
                                                            TextAlign.end,
                                                        style: titilliumSemiBold
                                                            .copyWith(
                                                          fontSize: Responsive
                                                                  .isMobile(
                                                                      context)
                                                              ? Dimensions
                                                                  .FONT_SIZE_LARGE
                                                              : Dimensions
                                                                  .FONT_SIZE_LARGE_IPAD,
                                                        )),
                                                  )
                                                : SizedBox(),
                                            Provider.of<SplashProvider>(context,
                                                            listen: false)
                                                        .configModel
                                                        .shippingMethodEnable &&
                                                    Provider.of<SplashProvider>(
                                                                context,
                                                                listen: false)
                                                            .configModel
                                                            .shippingMethod ==
                                                        'sellerwise_shipping'
                                                ? Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: Dimensions
                                                            .PADDING_SIZE_SMALL),
                                                    child: InkWell(
                                                      onTap: () {
                                                        if (Provider.of<
                                                                    AuthProvider>(
                                                                context,
                                                                listen: false)
                                                            .isLoggedIn()) {
                                                          showModalBottomSheet(
                                                            context: context,
                                                            isScrollControlled:
                                                                true,
                                                            backgroundColor:
                                                                Colors
                                                                    .transparent,
                                                            builder: (context) => ShippingMethodBottomSheet(
                                                                groupId: sellerGroupList[
                                                                        index]
                                                                    .cartGroupId,
                                                                sellerIndex:
                                                                    index,
                                                                sellerId:
                                                                    sellerGroupList[
                                                                            index]
                                                                        .id),
                                                          );
                                                        } else {
                                                          showCustomSnackBar(
                                                              'not_logged_in',
                                                              context);
                                                        }
                                                      },
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          border: Border.all(
                                                              width: 0.5,
                                                              color:
                                                                  Colors.grey),
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10)),
                                                        ),
                                                        child: Container(
                                                          padding: EdgeInsets
                                                              .all(Dimensions
                                                                  .PADDING_SIZE_SMALL),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .start,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                        getTranslated(
                                                                            'SHIPPING_PARTNER',
                                                                            context),
                                                                        style: titilliumRegular
                                                                            .copyWith(
                                                                          fontSize: Responsive.isMobile(context)
                                                                              ? Dimensions.FONT_SIZE_DEFAULT
                                                                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                                                        )),
                                                                    Visibility(
                                                                      visible: !(cart.shippingList == null ||
                                                                          cart.shippingList[index].shippingMethodList ==
                                                                              null ||
                                                                          cart.chosenShippingList.length ==
                                                                              0 ||
                                                                          cart.shippingList[index].shippingIndex ==
                                                                              -1),
                                                                      child: Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.start,
                                                                          children: [
                                                                            Text(
                                                                              (cart.shippingList == null || cart.shippingList[index].shippingMethodList == null || cart.chosenShippingList.length == 0 || cart.shippingList[index].shippingIndex == -1) ? '' : '${cart.shippingList[index].shippingMethodList[cart.shippingList[index].shippingIndex].title.toString()} ',
                                                                              style: titilliumSemiBold.copyWith(fontSize: Responsive.isMobile(context) ? Dimensions.FONT_SIZE_DEFAULT : Dimensions.FONT_SIZE_DEFAULT_IPAD, color: ColorResources.getPrimary(context)),
                                                                              overflow: TextOverflow.ellipsis,
                                                                            ),
                                                                          ]),
                                                                    ),
                                                                  ]),
                                                              Icon(
                                                                  Icons
                                                                      .keyboard_arrow_down,
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : SizedBox(),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  bottom: Dimensions
                                                      .PADDING_SIZE_LARGE),
                                              decoration: BoxDecoration(
                                                  color: Theme.of(context)
                                                      .highlightColor,
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.3),
                                                        spreadRadius: 1,
                                                        blurRadius: 3,
                                                        offset: Offset(0, 3)),
                                                  ]),
                                              child: Column(
                                                children: [
                                                  ListView.builder(
                                                    physics:
                                                        NeverScrollableScrollPhysics(),
                                                    shrinkWrap: true,
                                                    padding: EdgeInsets.all(0),
                                                    itemCount:
                                                        cartProductList[index]
                                                            .length,
                                                    itemBuilder: (context, i) =>
                                                        CartWidget(
                                                      cartModel:
                                                          cartProductList[index]
                                                              [i],
                                                      index:
                                                          cartProductIndexList[
                                                              index][i],
                                                      fromCheckout:
                                                          widget.fromCheckout,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ]),
                                    );
                                  },
                                ),
                              ),
                            ),
                            Visibility(
                              // visible: true,
                              visible: Provider.of<SplashProvider>(context,
                                      listen: false)
                                  .configModel
                                  .shippingMethodEnable,
                              child: Container(
                                padding: const EdgeInsets.all(
                                  Dimensions.PADDING_SIZE_SMALL,
                                ),
                                child:
                                    Provider.of<SplashProvider>(context,
                                                    listen: false)
                                                .configModel
                                                .shippingMethod !=
                                            'sellerwise_shipping'
                                        ? InkWell(
                                            onTap: () {
                                              if (Provider.of<AuthProvider>(
                                                      context,
                                                      listen: false)
                                                  .isLoggedIn()) {
                                                showModalBottomSheet(
                                                  context: context,
                                                  isScrollControlled: true,
                                                  backgroundColor:
                                                      Colors.transparent,
                                                  builder: (context) =>
                                                      ShippingMethodBottomSheet(
                                                          groupId:
                                                              'all_cart_group',
                                                          sellerIndex: 0,
                                                          sellerId: 1),
                                                );
                                              } else {
                                                showCustomSnackBar(
                                                    'not_logged_in', context);
                                              }
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 0.5,
                                                    color: Color.fromARGB(
                                                        255, 0, 0, 0)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.all(
                                                  Dimensions.PADDING_SIZE_SMALL,
                                                ),
                                                child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                          getTranslated(
                                                              'SHIPPING_PARTNER',
                                                              context),
                                                          style: titilliumRegular.copyWith(
                                                              fontSize: Responsive
                                                                      .isMobile(
                                                                          context)
                                                                  ? Dimensions
                                                                      .FONT_SIZE_DEFAULT
                                                                  : Dimensions
                                                                      .FONT_SIZE_DEFAULT_IPAD)),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Text(
                                                              (cart.shippingList == null ||
                                                                      cart.chosenShippingList
                                                                              .length ==
                                                                          0 ||
                                                                      cart.shippingList
                                                                              .length ==
                                                                          0 ||
                                                                      cart.shippingList[0].shippingMethodList ==
                                                                          null ||
                                                                      cart.shippingList[0]
                                                                              .shippingIndex ==
                                                                          -1)
                                                                  ? ''
                                                                  : '${cart.shippingList[0].shippingMethodList[cart.shippingList[0].shippingIndex].title.toString()}',
                                                              style: titilliumSemiBold.copyWith(
                                                                  color: ColorResources
                                                                      .getPrimary(
                                                                          context)),
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                            SizedBox(
                                                                width: Dimensions
                                                                    .PADDING_SIZE_EXTRA_SMALL),
                                                            Icon(
                                                                Icons
                                                                    .keyboard_arrow_down,
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColor),
                                                          ]),
                                                    ]),
                                              ),
                                            ),
                                          )
                                        : SizedBox(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Expanded(
                      child: NoInternetOrDataScreen(isNoInternet: false)),
        ]),
      );
    });
  }
}
