import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/cart_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_details_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:provider/provider.dart';

class CartBottomSheet extends StatefulWidget {
  final Product product;
  final Function callback;
  CartBottomSheet({@required this.product, this.callback});

  @override
  _CartBottomSheetState createState() => _CartBottomSheetState();
}

class _CartBottomSheetState extends State<CartBottomSheet> {
  route(bool isRoute, String message) async {
    if (isRoute) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            message,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: Colors.green));
      Navigator.pop(context);
    } else {
      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            message,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: Colors.red));
    }
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<ProductDetailsProvider>(context, listen: false)
        .initData(widget.product);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
          decoration: BoxDecoration(
            color: Theme.of(context).highlightColor,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(20), topLeft: Radius.circular(20)),
          ),
          child: Consumer<ProductDetailsProvider>(
            builder: (context, details, child) {
              Variation _variation;
              String _variantName = widget.product.colors.length != 0
                  ? widget.product.colors[details.variantIndex].name
                  : null;
              List<String> _variationList = [];
              for (int index = 0;
                  index < widget.product.choiceOptions.length;
                  index++) {
                _variationList.add(widget.product.choiceOptions[index]
                    .options[details.variationIndex[index]]
                    .trim());
              }
              String variationType = '';
              if (_variantName != null) {
                variationType = _variantName;
                _variationList.forEach(
                    (variation) => variationType = '$variationType-$variation');
              } else {
                bool isFirst = true;
                _variationList.forEach((variation) {
                  if (isFirst) {
                    variationType = '$variationType$variation';
                    isFirst = false;
                  } else {
                    variationType = '$variationType-$variation';
                  }
                });
              }
              double price = widget.product.unitPrice;
              int _stock = widget.product.currentStock;
              for (Variation variation in widget.product.variation) {
                if (variation.type.replaceAll(' ', '') ==
                    variationType.replaceAll(' ', '')) {
                  price = variation.price;
                  _variation = variation;
                  _stock = variation.qty;
                  break;
                }
              }
              double priceWithDiscount = PriceConverter.convertWithDiscount(
                  context,
                  price,
                  widget.product.discount,
                  widget.product.discountType);
              double priceWithQuantity = priceWithDiscount * details.quantity;

              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Close Button
                    Align(
                        alignment: Alignment.centerRight,
                        child: InkWell(
                          onTap: () => Navigator.pop(context),
                          child: Container(
                            width: Responsive.isMobile(context)
                                ? Dimensions.ICON_SIZE_DEFAULT
                                : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                            height: Responsive.isMobile(context)
                                ? Dimensions.ICON_SIZE_DEFAULT
                                : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Theme.of(context).highlightColor,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[
                                        Provider.of<ThemeProvider>(context)
                                                .darkTheme
                                            ? 700
                                            : 200],
                                    spreadRadius: 1,
                                    blurRadius: 5,
                                  )
                                ]),
                            child: Icon(
                              Icons.clear,
                              size: Responsive.isMobile(context)
                                  ? Dimensions.ICON_SIZE_DEFAULT
                                  : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                            ),
                          ),
                        )),

                    // Product details
                    Row(children: [
                      Container(
                        width: 100,
                        height: 100,
                        child: CachedNetworkImage(
                          width: double.infinity,
                          height: double.infinity,
                          imageUrl:
                              '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${widget.product.thumbnail}',
                          placeholder: (c, o) => Image.asset(
                            Images.placeholder,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Expanded(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(widget.product.name ?? '',
                                  style: titilliumSemiBold.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_LARGE
                                        : Dimensions.FONT_SIZE_LARGE_IPAD,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis),
                              Text(
                                PriceConverter.convertPrice(
                                    context, widget.product.unitPrice,
                                    discountType: widget.product.discountType,
                                    discount: widget.product.discount),
                                style: titilliumBold.copyWith(
                                  color: ColorResources.getGreen(context),
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_DEFAULT
                                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                ),
                              ),
                              widget.product.discount > 0
                                  ? Text(
                                      PriceConverter.convertPrice(
                                          context, widget.product.unitPrice),
                                      style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                          color: Theme.of(context).hintColor,
                                          decoration:
                                              TextDecoration.lineThrough),
                                    )
                                  : SizedBox(),
                            ]),
                      ),
                      Expanded(child: SizedBox.shrink()),
                      widget.product.discount > 0
                          ? Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      Dimensions.PADDING_SIZE_EXTRA_SMALL),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                border: Border.all(
                                    width: 2,
                                    color: ColorResources.getPrimary(context)),
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Text(
                                PriceConverter.percentageCalculation(
                                    context,
                                    widget.product.unitPrice,
                                    widget.product.discount,
                                    widget.product.discountType),
                                style: titilliumRegular.copyWith(
                                  color: Theme.of(context).hintColor,
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_EXTRA_SMALL
                                      : Dimensions.FONT_SIZE_EXTRA_SMALL_IPAD,
                                ),
                              ),
                            )
                          : SizedBox(),
                    ]),
                    SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                    // Variant
                    widget.product.colors.length > 0
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text(getTranslated('select_variant', context),
                                    style: robotoBold.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    )),
                                SizedBox(
                                  height: Dimensions.PADDING_SIZE_SMALL,
                                ),
                                SizedBox(
                                  height:
                                      Responsive.isMobile(context) ? 30 : 50,
                                  child: ListView.builder(
                                    itemCount: widget.product.colors.length,
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) {
                                      String colorString = '0xff' +
                                          widget.product.colors[index].code
                                              .substring(1, 7);
                                      return InkWell(
                                        onTap: () {
                                          print(
                                              'autoloadd ${details.variantIndex}');
                                          Provider.of<ProductDetailsProvider>(
                                                  context,
                                                  listen: false)
                                              .setCartVariantIndex(index);
                                        },
                                        child: Container(
                                          height: Responsive.isMobile(context)
                                              ? 30
                                              : 50,
                                          width: Responsive.isMobile(context)
                                              ? 30
                                              : 50,
                                          margin: EdgeInsets.only(
                                              left: Dimensions
                                                  .PADDING_SIZE_EXTRA_SMALL),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            color:
                                                Color(int.parse(colorString)),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.grey[
                                                      Provider.of<ThemeProvider>(
                                                                  context)
                                                              .darkTheme
                                                          ? 700
                                                          : 200],
                                                  spreadRadius: 1,
                                                  blurRadius: 5)
                                            ],
                                          ),
                                          child: details.variantIndex == index
                                              ? Icon(
                                                  Icons.check_circle,
                                                  color: ColorResources
                                                      .getSecondary(context),
                                                  size: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .ICON_SIZE_DEFAULT
                                                      : Dimensions
                                                          .ICON_SIZE_DEFAULT_IPAD,
                                                )
                                              : null,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                                SizedBox(
                                  height: Dimensions.PADDING_SIZE_SMALL,
                                ),
                              ])
                        : SizedBox(),

                    // Variation
                    ListView.builder(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      itemCount: widget.product.choiceOptions.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.product.choiceOptions[index].title,
                                    style: robotoBold.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                  ),
                                  SizedBox(
                                    height: Dimensions.PADDING_SIZE_SMALL,
                                  ),
                                  SizedBox(
                                    height:
                                        Responsive.isMobile(context) ? 30 : 50,
                                    child: ListView.builder(
                                      itemCount: widget.product
                                          .choiceOptions[index].options.length,
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, i) {
                                        return InkWell(
                                          onTap: () {
                                            Provider.of<ProductDetailsProvider>(
                                                    context,
                                                    listen: false)
                                                .setCartVariationIndex(
                                                    index, i);
                                          },
                                          child: Container(
                                            height: Responsive.isMobile(context)
                                                ? 30
                                                : 50,
                                            width: Responsive.isMobile(context)
                                                ? 50
                                                : 60,
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(
                                                left: Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL),
                                            decoration: BoxDecoration(
                                              color: details.variationIndex[
                                                          index] !=
                                                      i
                                                  ? Theme.of(context)
                                                      .highlightColor
                                                  : ColorResources.getSecondary(
                                                      context),
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              border: details.variationIndex[
                                                          index] !=
                                                      i
                                                  ? Border.all(
                                                      color: Colors.grey
                                                          .withOpacity(.5),
                                                      width: 1)
                                                  : null,
                                            ),
                                            child: Text(
                                                widget
                                                    .product
                                                    .choiceOptions[index]
                                                    .options[i]
                                                    .replaceAll(' ', ''),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_SMALL
                                                      : Dimensions
                                                          .FONT_SIZE_SMALL_IPAD,
                                                  color: details.variationIndex[
                                                              index] !=
                                                          i
                                                      ? Theme.of(context)
                                                          .hintColor
                                                      : Theme.of(context)
                                                          .highlightColor,
                                                )),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: Dimensions.PADDING_SIZE_SMALL,
                                  ),
                                ],
                              ),
                            ]);
                      },
                    ),

                    // Quantity

                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(getTranslated('quantity', context),
                              style: robotoBold.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              )),
                          Row(
                            children: [
                              QuantityButton(
                                  isIncrement: false,
                                  quantity: details.quantity,
                                  stock: _stock),
                              Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: Dimensions.MARGIN_SIZE_DEFAULT,
                                ),
                                child: Text(
                                  details.quantity.toString(),
                                  style: titilliumSemiBold.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_EXTRA_LARGE
                                        : Dimensions.FONT_SIZE_EXTRA_LARGE_IPAD,
                                  ),
                                ),
                              ),
                              QuantityButton(
                                  isIncrement: true,
                                  quantity: details.quantity,
                                  stock: _stock),
                            ],
                          )
                        ]),
                    SizedBox(height: Dimensions.PADDING_SIZE_DEFAULT),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(getTranslated('total_price', context),
                            style: robotoBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                            )),
                        SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                        Text(
                          PriceConverter.convertPrice(
                              context, priceWithQuantity),
                          style: titilliumBold.copyWith(
                            color: ColorResources.getGreen(context),
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_EXTRA_LARGE
                                : Dimensions.FONT_SIZE_EXTRA_LARGE_IPAD,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                    // Cart button

                    Provider.of<CartProvider>(context).isLoading
                        ? Center(
                            child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                Theme.of(context).primaryColor,
                              ),
                            ),
                          )
                        : Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: 100,
                            ),
                            child: CustomButton(
                                buttonText: getTranslated(
                                    _stock < 1 ? 'out_of_stock' : 'add_to_cart',
                                    context),
                                onTap: _stock < 1
                                    ? null
                                    : () {
                                        if (_stock > 0) {
                                          CartModel cart = CartModel(
                                              widget.product.id,
                                              widget.product.thumbnail,
                                              widget.product.name,
                                              widget.product.addedBy == 'seller'
                                                  ? '${Provider.of<SellerProvider>(context, listen: false).sellerModel.fName} '
                                                      '${Provider.of<SellerProvider>(context, listen: false).sellerModel.lName}'
                                                  : 'admin',
                                              price,
                                              priceWithDiscount,
                                              details.quantity,
                                              _stock,
                                              widget.product.colors.length > 0
                                                  ? widget
                                                      .product
                                                      .colors[
                                                          details.variantIndex]
                                                      .name
                                                  : '',
                                              widget.product.colors.length > 0
                                                  ? widget
                                                      .product
                                                      .colors[
                                                          details.variantIndex]
                                                      .code
                                                  : '',
                                              _variation,
                                              widget.product.discount,
                                              widget.product.discountType,
                                              widget.product.tax,
                                              widget.product.taxType,
                                              1,
                                              '',
                                              widget.product.userId,
                                              '',
                                              '',
                                              '',
                                              widget.product.choiceOptions,
                                              Provider.of<ProductDetailsProvider>(
                                                      context,
                                                      listen: false)
                                                  .variationIndex,
                                              '');

                                          // cart.variations = _variation;

                                          if (Provider.of<AuthProvider>(context,
                                                  listen: false)
                                              .isLoggedIn()) {
                                            Provider.of<CartProvider>(context,
                                                    listen: false)
                                                .addToCartAPI(
                                              cart,
                                              route,
                                              context,
                                              widget.product.choiceOptions,
                                              Provider.of<ProductDetailsProvider>(
                                                      context,
                                                      listen: false)
                                                  .variationIndex,
                                            );
                                          } else {
                                            Provider.of<CartProvider>(context,
                                                    listen: false)
                                                .addToCart(cart);
                                            showCustomSnackBar(
                                                getTranslated(
                                                    'added_to_cart', context),
                                                context,
                                                isError: false);
                                          }
                                        }
                                      }),
                          ),
                  ]);
            },
          ),
        ),
      ],
    );
  }
}

class QuantityButton extends StatelessWidget {
  final bool isIncrement;
  final int quantity;
  final bool isCartWidget;
  final int stock;

  QuantityButton({
    @required this.isIncrement,
    @required this.quantity,
    @required this.stock,
    this.isCartWidget = false,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (!isIncrement && quantity > 1) {
          Provider.of<ProductDetailsProvider>(context, listen: false)
              .setQuantity(quantity - 1);
        } else if (isIncrement && quantity < stock) {
          Provider.of<ProductDetailsProvider>(context, listen: false)
              .setQuantity(quantity + 1);
        }
      },
      child: Container(
        height: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
        width: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
        decoration: BoxDecoration(
          border: Border.all(color: ColorResources.getSecondary(context)),
          borderRadius: BorderRadius.circular(100),
        ),
        child: Icon(
          isIncrement ? Icons.add : Icons.remove,
          color: isIncrement
              ? quantity >= stock
                  ? ColorResources.getLowGreen(context)
                  : ColorResources.getSecondary(context)
              : quantity > 1
                  ? ColorResources.getSecondary(context)
                  : ColorResources.getLowGreen(context),
          size: Responsive.isMobile(context)
              ? Dimensions.ICON_SIZE_SMALL
              : Dimensions.ICON_SIZE_SMALL_IPAD,
        ),
      ),
    );
  }
}
