import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/guest_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_orderby_app_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_screen.dart';
import 'package:provider/provider.dart';

class SellerView extends StatelessWidget {
  final String sellerId;
  final bool fromShop;
  SellerView({@required this.sellerId, this.fromShop = false});

  @override
  Widget build(BuildContext context) {
    Provider.of<SellerProvider>(context, listen: false)
        .initSeller(sellerId, context);
    ShopModel shopModel =
        Provider.of<ShopsProvider>(context, listen: true).shop;
    return Consumer<SellerProvider>(
      builder: (context, seller, child) {
        if (seller.sellerModel?.shop != null) {
          return Container(
            margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
            padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
            color: Theme.of(context).cardColor,
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        height: Responsive.isMobile(context) ? 60 : 120,
                        width: Responsive.isMobile(context) ? 60 : 120,
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: Colors.grey.withOpacity(.2)),
                          borderRadius: BorderRadius.circular(500),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(500),
                          child: CachedNetworkImage(
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.cover,
                            imageUrl:
                                '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${seller.sellerModel.shop.image}',
                            placeholder: (c, o) => Image.asset(
                                Images.placeholder,
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: Dimensions.PADDING_SIZE_DEFAULT,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width -
                            (Responsive.isMobile(context) ? 250 : 400),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              seller.sellerModel.shop.name,
                              style: titilliumSemiBold.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_LARGE
                                    : Dimensions.FONT_SIZE_LARGE_IPAD,
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Row(
                              children: [
                                Text(
                                  seller.sellerModel?.productOrderType != null
                                      ? getTranslated(
                                          seller.sellerModel?.productOrderType,
                                          context)
                                      : '',
                                  style: titilliumRegular.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    color: ColorResources.getSecondary(context),
                                  ),
                                ),
                              ],
                            ),
                            Visibility(
                              visible: fromShop ? false : true,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.phone,
                                    size: Responsive.isMobile(context)
                                        ? Dimensions.ICON_SIZE_SMALL
                                        : Dimensions.ICON_SIZE_SMALL_IPAD,
                                    color: ColorResources.getSecondary(context),
                                  ),
                                  Text(
                                    seller.sellerModel.shop.contact,
                                    style: titilliumRegular.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                            ),
                            Row(
                              children: [
                                Icon(
                                  Icons.timer_sharp,
                                  size: Responsive.isMobile(context)
                                      ? Dimensions.ICON_SIZE_SMALL
                                      : Dimensions.ICON_SIZE_SMALL_IPAD,
                                  color: ColorResources.getSecondary(context),
                                ),
                                SizedBox(
                                  width: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                ),
                                Text(
                                  seller.sellerModel.shop.open != null &&
                                          int.parse(seller
                                                  .sellerModel.shop.open) >
                                              0
                                      ? getTranslated('Opening', context)
                                      : getTranslated('Closed', context),
                                  style: titilliumRegular.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  fromShop
                      ? InkWell(
                          onTap: () {
                            App.launchUrl('tel:' + shopModel.contact);
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                              vertical: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            decoration: BoxDecoration(
                              color: ColorResources.getSecondary(context),
                              borderRadius: BorderRadius.circular(
                                Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              ),
                            ),
                            child: Row(
                              children: [
                                Icon(
                                  Icons.phone,
                                  size: Responsive.isMobile(context)
                                      ? Dimensions.ICON_SIZE_SMALL
                                      : Dimensions.ICON_SIZE_SMALL_IPAD,
                                  color: ColorResources.WHITE,
                                ),
                                Text(
                                  shopModel.contact,
                                  style: robotoRegular.copyWith(
                                      color: Colors.white,
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD),
                                ),
                              ],
                            ),
                          ),
                        )
                      : InkWell(
                          onTap: () {
                            if (!Provider.of<AuthProvider>(context,
                                    listen: false)
                                .isLoggedIn()) {
                              showAnimatedDialog(context, GuestDialog(),
                                  isFlip: true);
                            } else if (seller.sellerModel != null) {
                              ShopModel shopModel = ShopModel(
                                id: seller.sellerModel.shop.id,
                                name: seller.sellerModel.shop.name,
                                image: seller.sellerModel.shop.image,
                                banner: seller.sellerModel.shop.banner,
                                address: seller.sellerModel.shop.address,
                                contact: seller.sellerModel.shop.contact,
                                seller: seller.sellerModel,
                              );

                              Navigator.push(context,
                                  MaterialPageRoute(builder: (_) {
                                if (seller.sellerModel.productOrderType ==
                                    AppConstants.ORDER_BY_APP) {
                                  return ShopOrderByAppScreen(
                                      shopModel: shopModel);
                                }
                                return ShopScreen(shopModel: shopModel);
                              }));
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                              vertical: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            decoration: BoxDecoration(
                              color: ColorResources.getPrimary(context),
                              borderRadius: BorderRadius.circular(
                                Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              ),
                            ),
                            child: Text(
                              getTranslated('VIEW_SHOP', context),
                              style: robotoRegular.copyWith(
                                  color: Colors.white,
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_DEFAULT
                                      : Dimensions.FONT_SIZE_DEFAULT_IPAD),
                            ),
                          ),
                        ),
                ],
              ),
              Visibility(
                visible: false,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: Dimensions.PADDING_SIZE_LARGE,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SellerDetailReviewItem(
                        title: 'Item',
                        review: '5.0 Excellent',
                      ),
                      SellerDetailReviewItem(
                        title: 'Service',
                        review: '5.0 Excellent',
                      ),
                      SellerDetailReviewItem(
                        title: 'Delivery',
                        review: '5.0 Excellent',
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          );
        }
        return SizedBox.shrink();
      },
    );
  }
}

class SellerDetailReviewItem extends StatelessWidget {
  final String title;
  final String review;
  const SellerDetailReviewItem(
      {Key key, @required this.title, @required this.review})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            title,
            style: titilliumRegular.copyWith(
              color: Colors.black,
              fontSize: Dimensions.FONT_SIZE_DEFAULT,
            ),
          ),
          Text(
            review,
            style: titilliumRegular.copyWith(
              color: ColorResources.getSecondary(context),
              fontSize: Dimensions.FONT_SIZE_DEFAULT,
            ),
          ),
        ],
      ),
    );
  }
}
