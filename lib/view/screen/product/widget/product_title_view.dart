import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/seller_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_details_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/wishlist_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/rating_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/favourite_button.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class ProductTitleView extends StatelessWidget {
  final Product productModel;
  ProductTitleView({@required this.productModel});

  @override
  Widget build(BuildContext context) {
    SellerModel sellerModel =
        Provider.of<SellerProvider>(context, listen: false).sellerModel;

    double _startingPrice;
    double _endingPrice;
    if (productModel.variation.length != 0) {
      List<double> _priceList = [];
      productModel.variation
          .forEach((variation) => _priceList.add(variation.price));
      _priceList.sort((a, b) => a.compareTo(b));
      _startingPrice = _priceList[0];
      if (_priceList[0] < _priceList[_priceList.length - 1]) {
        _endingPrice = _priceList[_priceList.length - 1];
      }
    } else {
      _startingPrice = productModel.unitPrice;
    }

    return Container(
      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      decoration: BoxDecoration(
        color: Colors.lightBlue[100].withOpacity(.4),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Consumer<ProductDetailsProvider>(
        builder: (context, details, child) {
          return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(children: [
                  Text(
                    '${PriceConverter.convertPrice(context, _startingPrice, discount: productModel.discount, discountType: productModel.discountType)}'
                    '${_endingPrice != null ? ' - ${PriceConverter.convertPrice(context, _endingPrice, discount: productModel.discount, discountType: productModel.discountType)}' : ''}',
                    style: titilliumBold.copyWith(
                      color: ColorResources.getGreen(context),
                      fontSize: Responsive.isMobile(context)
                          ? Dimensions.FONT_SIZE_OVER_LARGE
                          : Dimensions.FONT_SIZE_OVER_LARGE_IPAD,
                    ),
                  ),
                  SizedBox(width: 20),
                  productModel.discount > 0
                      ? Container(
                          child: Text(
                            PriceConverter.percentageCalculation(
                                context,
                                productModel.unitPrice,
                                productModel.discount,
                                productModel.discountType),
                            style: titilliumRegular.copyWith(
                              color: Colors.red,
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_EXTRA_LARGE
                                  : Dimensions.FONT_SIZE_EXTRA_LARGE_IPAD,
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                  Expanded(child: SizedBox.shrink()),
                  productModel.addedBy == 'seller'
                      ? sellerModel != null &&
                              sellerModel?.productOrderType ==
                                  AppConstants.ORDER_BY_APP
                          ? FavouriteButton(
                              isSelected:
                                  Provider.of<WishListProvider>(context).isWish,
                              productId: productModel.id,
                            )
                          : SizedBox.shrink()
                      : FavouriteButton(
                          isSelected:
                              Provider.of<WishListProvider>(context).isWish,
                          productId: productModel.id,
                        ),
                  Visibility(
                    visible: false,
                    child: InkWell(
                      onTap: () {
                        if (Provider.of<ProductDetailsProvider>(context,
                                    listen: false)
                                .sharableLink !=
                            null) {
                          Share.share(Provider.of<ProductDetailsProvider>(
                                  context,
                                  listen: false)
                              .sharableLink);
                        }
                      },
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          color: Theme.of(context).highlightColor,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[
                                    Provider.of<ThemeProvider>(context)
                                            .darkTheme
                                        ? 700
                                        : 200],
                                spreadRadius: 1,
                                blurRadius: 5)
                          ],
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.share,
                          color: ColorResources.getPrimary(context),
                          size: Responsive.isMobile(context)
                              ? Dimensions.ICON_SIZE_SMALL
                              : Dimensions.ICON_SIZE_SMALL_IPAD,
                        ),
                      ),
                    ),
                  ),
                ]),
                productModel.discount > 0
                    ? Text(
                        '${PriceConverter.convertPrice(context, _startingPrice)}'
                        '${_endingPrice != null ? ' - ${PriceConverter.convertPrice(context, _endingPrice)}' : ''}',
                        style: titilliumRegular.copyWith(
                            color: Colors.grey,
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_LARGE
                                : Dimensions.FONT_SIZE_LARGE_IPAD,
                            decoration: TextDecoration.lineThrough),
                      )
                    : SizedBox(),
                Text(productModel.name ?? '',
                    style: titilliumSemiBold.copyWith(
                      fontSize: Responsive.isMobile(context)
                          ? Dimensions.FONT_SIZE_LARGE
                          : Dimensions.FONT_SIZE_LARGE_IPAD,
                    ),
                    maxLines: 2),
                Row(children: [
                  RatingBar(
                    number: 1,
                    rating: productModel.rating != null
                        ? productModel.rating.length > 0
                            ? double.parse(productModel.rating[0].average)
                            : 0.0
                        : 0.0,
                  ),
                  SizedBox(width: 5),
                  Text(
                      '(${productModel.rating != null ? productModel.rating.length > 0 ? double.parse(productModel.rating[0].average).toStringAsFixed(1) : '0.0' : '0.0'})',
                      style: titilliumSemiBold.copyWith(
                        color: Theme.of(context).hintColor,
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_LARGE
                            : Dimensions.FONT_SIZE_LARGE_IPAD,
                      )),
                  // Expanded(child: SizedBox.shrink()),
                  // Text(
                  //     '${details.reviewList != null ? details.reviewList.length : 0} reviews | ',
                  //     style: titilliumRegular.copyWith(
                  //       color: Theme.of(context).hintColor,
                  //       fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                  //     )),
                  // Text('${details.orderCount} orders | ',
                  //     style: titilliumRegular.copyWith(
                  //       color: Theme.of(context).hintColor,
                  //       fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                  //     )),
                  // Text('${details.wishCount} wish',
                  //     style: titilliumRegular.copyWith(
                  //       color: Theme.of(context).hintColor,
                  //       fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                  //     )),
                ]),
              ]);
        },
      ),
    );
  }
}
