import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class ProductVideoView extends StatefulWidget {
  final List<String> videoList;
  const ProductVideoView({Key key, @required this.videoList}) : super(key: key);

  @override
  State<ProductVideoView> createState() => _ProductVideoViewState();
}

class _ProductVideoViewState extends State<ProductVideoView> {
  @override
  Widget build(BuildContext context) {
    YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId:
          YoutubePlayerController.convertUrlToId(widget.videoList.first),
      params: YoutubePlayerParams(
        autoPlay: false,
        showControls: true,
        showFullscreenButton: true,
      ),
    );

    return YoutubePlayerIFrame(
      gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{},
      controller: _controller,
      aspectRatio: 16 / 9,
    );
  }
}
