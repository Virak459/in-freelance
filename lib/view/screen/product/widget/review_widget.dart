import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/review_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/date_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_details_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class ReviewWidget extends StatelessWidget {
  final ReviewModel reviewModel;
  ReviewWidget({@required this.reviewModel});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius:
                  BorderRadius.circular(Responsive.isMobile(context) ? 15 : 30),
              child: CachedNetworkImage(
                height: Responsive.isMobile(context) ? 30 : 60,
                width: Responsive.isMobile(context) ? 30 : 60,
                fit: BoxFit.cover,
                imageUrl:
                    '${Provider.of<SplashProvider>(context, listen: false).baseUrls.customerImageUrl}/${reviewModel.customer.image}',
                placeholder: (c, o) => Image.asset(Images.placeholder,
                    height: Responsive.isMobile(context) ? 30 : 60,
                    width: Responsive.isMobile(context) ? 30 : 60,
                    fit: BoxFit.cover),
              ),
            ),
            SizedBox(width: 5),
            Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    Text(
                      '${reviewModel.customer.fName} ${reviewModel.customer.lName}',
                      style: titilliumRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_SMALL
                            : Dimensions.FONT_SIZE_SMALL_IPAD,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(width: 5),
                    RatingBar(
                      rating: reviewModel.rating.toDouble(),
                      size: Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_SMALL
                          : Dimensions.ICON_SIZE_SMALL_IPAD,
                    ),
                  ]),
                  Text(
                      DateConverter.isoStringToLocalDateAndTime(
                          context, reviewModel.updatedAt),
                      style: titilliumRegular.copyWith(
                        color: Theme.of(context).hintColor,
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_EXTRA_SMALL
                            : Dimensions.FONT_SIZE_EXTRA_SMALL_IPAD,
                      )),
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Text(
                        getTranslated('comment', context) + ' : ',
                        style: titilliumRegular.copyWith(
                          color: Theme.of(context).hintColor,
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_EXTRA_SMALL
                              : Dimensions.FONT_SIZE_EXTRA_SMALL_IPAD,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        reviewModel.comment,
                        style: titilliumRegular.copyWith(
                          color: Theme.of(context).hintColor,
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_EXTRA_SMALL
                              : Dimensions.FONT_SIZE_EXTRA_SMALL_IPAD,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  reviewModel.attachment.length > 0
                      ? SizedBox(
                          height: Responsive.isMobile(context) ? 30 : 60,
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: reviewModel.attachment.length,
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(
                                  right: Dimensions.PADDING_SIZE_SMALL,
                                ),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey.withOpacity(.2),
                                  ),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(1),
                                  child: CachedNetworkImage(
                                    height:
                                        Responsive.isMobile(context) ? 30 : 60,
                                    width:
                                        Responsive.isMobile(context) ? 30 : 60,
                                    fit: BoxFit.cover,
                                    imageUrl:
                                        '${Provider.of<SplashProvider>(context, listen: false).baseUrls.reviewImageUrl}/review/${reviewModel.attachment[index]}',
                                    placeholder: (c, o) => Image.asset(
                                        Images.placeholder,
                                        height: Responsive.isMobile(context)
                                            ? 30
                                            : 60,
                                        width: Responsive.isMobile(context)
                                            ? 30
                                            : 60,
                                        fit: BoxFit.cover),
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      : SizedBox(),
                ]),
          ]),
    ]);
  }
}

class ReviewShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      enabled: Provider.of<ProductDetailsProvider>(context).reviewList == null,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(children: [
          CircleAvatar(
            maxRadius: 15,
            backgroundColor: ColorResources.SELLER_TXT,
            child: Icon(Icons.person),
          ),
          SizedBox(width: 5),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(children: [
              Container(height: 10, width: 50, color: ColorResources.WHITE),
              SizedBox(width: 5),
              RatingBar(rating: 0, size: 12),
            ]),
            Container(height: 10, width: 50, color: ColorResources.WHITE),
          ]),
        ]),
        SizedBox(height: 5),
        Container(height: 20, width: 200, color: ColorResources.WHITE),
      ]),
    );
  }
}
