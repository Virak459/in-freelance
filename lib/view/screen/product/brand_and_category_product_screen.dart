// ignore: unused_import
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/data/model/response/response_model.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/basewidget/custom_app_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_shimmer.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class BrandAndCategoryProductScreen extends StatelessWidget {
  final bool isBrand;
  final String id;
  final String name;
  final String image;
  BrandAndCategoryProductScreen(
      {@required this.isBrand,
      @required this.id,
      @required this.name,
      this.image});

  @override
  Widget build(BuildContext context) {
    Provider.of<ProductProvider>(context, listen: false)
        .initBrandOrCategoryProductList(isBrand, id, context);

    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(name),
      ),
      body: Consumer<ProductProvider>(
        builder: (context, productProvider, child) {
          return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                // Products
                productProvider.brandOrCategoryProductList.length > 0
                    ? Expanded(
                        child: RefreshIndicator(
                          color: Colors.white,
                          backgroundColor: ColorResources.getSecondary(context),
                          onRefresh: () async {
                            await Provider.of<ProductProvider>(context,
                                    listen: false)
                                .initBrandOrCategoryProductList(
                                    isBrand, id, context);
                          },
                          child: StaggeredGridView.countBuilder(
                            padding: EdgeInsets.symmetric(
                                horizontal: Dimensions.PADDING_SIZE_SMALL),
                            physics: BouncingScrollPhysics(),
                            crossAxisCount: 2,
                            itemCount: productProvider
                                .brandOrCategoryProductList.length,
                            shrinkWrap: true,
                            staggeredTileBuilder: (int index) =>
                                StaggeredTile.fit(1),
                            itemBuilder: (BuildContext context, int index) {
                              return ProductWidget(
                                  productModel: productProvider
                                      .brandOrCategoryProductList[index]);
                            },
                          ),
                        ),
                      )
                    : Expanded(
                        child: productProvider.hasData
                            ? ProductShimmer(
                                isHomePage: false,
                                isEnabled: Provider.of<ProductProvider>(context)
                                        .brandOrCategoryProductList
                                        .length ==
                                    0)
                            : Center(
                                child: NoInternetOrDataScreen(
                                    isNoInternet: false)),
                      ),
              ]);
        },
      ),
    );
  }
}
