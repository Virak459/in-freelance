import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/seller_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/cart_bottom_sheet2.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/coupon_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/product_videos_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/seller_view.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_details_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/wishlist_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/product_image_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/product_specification_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/product_title_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/review_widget.dart';
import 'package:provider/provider.dart';

import 'faq_and_review_screen.dart';

class ProductDetails extends StatefulWidget {
  final Product product;
  final bool fromShop;
  ProductDetails({@required this.product, this.fromShop = false});

  @override
  State<ProductDetails> createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  ScrollController _scrollViewController;
  Color appBackgroundColor = Colors.transparent;
  SellerModel sellerModel;
  @override
  void initState() {
    super.initState();
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _scrollViewController.addListener(changeColor);
    load();
  }

  load() async {
    await Provider.of<SellerProvider>(context, listen: false)
        .initSeller(widget.product.userId.toString(), context);

    sellerModel =
        Provider.of<SellerProvider>(context, listen: false).sellerModel;
    // Provider.of<ProductDetailsProvider>(context, listen: false).removePrevReview();
    Provider.of<ProductDetailsProvider>(context, listen: false)
        .initProduct(widget.product, context);
    // Provider.of<ProductProvider>(context, listen: false).removePrevRelatedProduct();
    //Provider.of<ProductProvider>(context, listen: false).initRelatedProductList(product.id.toString(), context);
    Provider.of<ProductDetailsProvider>(context, listen: false)
        .getCount(widget.product.id.toString(), context);
    Provider.of<ProductDetailsProvider>(context, listen: false)
        .getSharableLink(widget.product.id.toString(), context);

    if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
      Provider.of<WishListProvider>(context, listen: false)
          .checkWishList(widget.product.id.toString(), context);
    }
    Provider.of<ProductDetailsProvider>(context, listen: false)
        .initData(widget.product);
  }

  void changeColor() {
    if (_scrollViewController.offset == 0) {
      appBackgroundColor = Colors.transparent;
      setState(() {});
    } else if (_scrollViewController.offset <= 40) {
      appBackgroundColor = ColorResources.getSecondary(context);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    sellerModel =
        Provider.of<SellerProvider>(context, listen: false).sellerModel;
    return Consumer<ProductDetailsProvider>(
      builder: (context, detail, child) {
        sellerModel =
            Provider.of<SellerProvider>(context, listen: false).sellerModel;

        return detail.hasConnection
            ? Scaffold(
                extendBodyBehindAppBar: true,
                appBar: AppBar(
                  elevation: 0,
                  backgroundColor: appBackgroundColor,
                  leading: Container(
                    padding:
                        EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
                    child: CircleAvatar(
                      backgroundColor: ColorResources.getSecondary(context),
                      child: BackButton(
                        color: Colors.white,
                      ),
                    ),
                  ),
                  actions: [
                    IconButton(
                      padding: EdgeInsets.zero,
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => CartScreen()));
                      },
                      icon: Container(
                        child: CircleAvatar(
                          backgroundColor: ColorResources.getSecondary(context),
                          child: Stack(clipBehavior: Clip.none, children: [
                            Image.asset(
                              Images.cart_image,
                              height: Dimensions.ICON_SIZE_DEFAULT,
                              width: Dimensions.ICON_SIZE_DEFAULT,
                              color: ColorResources.WHITE,
                            ),
                            Positioned(
                              top: -4,
                              right: -4,
                              child: Consumer<CartProvider>(
                                  builder: (context, cart, child) {
                                return CircleAvatar(
                                  radius: 7,
                                  backgroundColor: Colors.black,
                                  child: Text(cart.cartList.length.toString(),
                                      style: titilliumSemiBold.copyWith(
                                        color: ColorResources.WHITE,
                                        fontSize:
                                            Dimensions.FONT_SIZE_EXTRA_SMALL,
                                      )),
                                );
                              }),
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ],
                ),
                body: SingleChildScrollView(
                  controller: _scrollViewController,
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ProductImageView(productModel: widget.product),
                      ProductTitleView(productModel: widget.product),
                      // Coupon
                      // Container(
                      //   margin:
                      //       EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                      //   padding:
                      //       EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      //   color: ColorResources.WHITE,
                      //   child: CouponView(),
                      // ),

                      // Specification
                      (widget.product.details != null &&
                              widget.product.details.isNotEmpty)
                          ? Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      top: Dimensions.PADDING_SIZE_SMALL),
                                  padding: EdgeInsets.all(
                                      Dimensions.PADDING_SIZE_SMALL),
                                  color: Theme.of(context).highlightColor,
                                  child: ProductSpecification(
                                      productSpecification:
                                          widget.product.details ?? ''),
                                ),
                                Divider(
                                  thickness: 10,
                                  color: Colors.grey[100].withOpacity(.4),
                                ),
                              ],
                            )
                          : SizedBox(),

                      // Product variant
                      /*Container(
                  margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  color: ColorResources.WHITE,
                  child: Column(
                    children: [
                      Row(children: [
                        Expanded(child: Text(Strings.product_variants, style: robotoBold)),
                        Text(Strings.details, style: titilliumRegular.copyWith(
                          color: ColorResources.SELLER_TXT,
                          fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                        )),
                      ]),
                      SizedBox(height: 5),
                      SizedBox(
                        height: 45,
                        child: ListView.builder(
                          itemCount: product.colors.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            String colorString = '0xff' + product.colors[index].substring(1, 7);
                            return Container(
                              width: 45,
                              margin: EdgeInsets.only(right: 10),
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Color(int.parse(colorString))),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),*/

                      // Reviews

                      detail.reviewList != null && detail.reviewList.length != 0
                          ? Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.PADDING_SIZE_SMALL),
                              padding:
                                  EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                              color: Theme.of(context).highlightColor,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    TitleRow(
                                        title: getTranslated(
                                                'reviews', context) +
                                            '(${detail.reviewList != null ? detail.reviewList.length : 0})',
                                        isDetailsPage: true,
                                        isMore: true,
                                        onTap: () {
                                          if (detail.reviewList != null) {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (_) =>
                                                        ReviewScreen(
                                                            reviewList: detail
                                                                .reviewList)));
                                          }
                                        }),
                                    Divider(),
                                    detail.reviewList != null
                                        ? detail.reviewList.length != 0
                                            ? Column(
                                                children: [
                                                  ReviewWidget(
                                                      reviewModel:
                                                          detail.reviewList[0]),
                                                  Divider(
                                                    thickness: 10,
                                                    color: Colors.grey[100]
                                                        .withOpacity(.4),
                                                  ),
                                                ],
                                              )
                                            : Center(
                                                child: Text(
                                                getTranslated(
                                                    'no_review', context),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ))
                                        : ReviewShimmer(),
                                  ]),
                            )
                          : SizedBox.shrink(),
                      widget.product.videoURL != null &&
                              widget.product.videoURL.isNotEmpty
                          ? ProductVideoView(
                              videoList: [widget.product.videoURL],
                            )
                          : SizedBox(),
                      widget.product.addedBy == 'seller'
                          ? sellerModel != null &&
                                  sellerModel?.productOrderType ==
                                      AppConstants.ORDER_BY_APP
                              ? CartBottomSheet2(
                                  product: widget.product,
                                )
                              : CartBottomSheet2(
                                  product: widget.product,
                                  canorder: false,
                                )
                          : CartBottomSheet2(
                              product: widget.product,
                            ),

                      // Related Products
                      // Container(
                      //   margin:
                      //       EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                      //   padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                      //   color: Theme.of(context).highlightColor,
                      //   child: Column(
                      //     children: [
                      //       TitleRow(
                      //           title:
                      //               getTranslated('related_products', context),
                      //           isDetailsPage: true),
                      //       SizedBox(height: 5),
                      //       RelatedProductView(),
                      //     ],
                      //   ),
                      // ),
                      // Seller

                      widget.product.addedBy == 'seller' &&
                              (sellerModel?.productOrderType ?? '') ==
                                  AppConstants.DIRECT_TO_SHOP
                          ? SellerView(
                              sellerId: widget.product.userId.toString(),
                              fromShop: widget.fromShop)
                          : SizedBox.shrink(),
                    ],
                  ),
                ),
              )
            : Scaffold(
                body: NoInternetOrDataScreen(
                    isNoInternet: true,
                    child: ProductDetails(product: widget.product)));
      },
    );
  }
}
