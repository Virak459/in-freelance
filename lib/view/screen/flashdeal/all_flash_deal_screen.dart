import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/flash_deal_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';

import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/flashdeal/flash_deal_screen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class AllFlashDealScreen extends StatelessWidget {
  const AllFlashDealScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<FlashDealProvider>(context, listen: false)
        .getMegaDealList(true, context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(
          getTranslated('promotions', context),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Consumer<FlashDealProvider>(
                builder: (context, flashDealProvider, child) {
              return Column(children: [
                flashDealProvider.flashDeals.length != 0
                    ? StaggeredGridView.countBuilder(
                        itemCount: flashDealProvider.flashDeals.length,
                        crossAxisCount: 2,
                        mainAxisSpacing: Dimensions.PADDING_SIZE_DEFAULT,
                        crossAxisSpacing: Dimensions.PADDING_SIZE_DEFAULT,
                        addAutomaticKeepAlives: true,
                        padding:
                            EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        staggeredTileBuilder: (int index) =>
                            StaggeredTile.fit(1),
                        itemBuilder: (BuildContext context, int index) {
                          FlashDealModel flashDealModel =
                              flashDealProvider.flashDeals[index];
                          return InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => FlashDealScreen(
                                      flashDealModel: flashDealModel),
                                ),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Theme.of(context).highlightColor,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 1,
                                      blurRadius: 5)
                                ],
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(
                                  Dimensions.PADDING_SIZE_SMALL,
                                ),
                                child: CachedNetworkImage(
                                  height: Responsive.isMobile(context)
                                      ? App.height(context) * 20
                                      : App.height(context) * 40,
                                  imageUrl:
                                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.reviewImageUrl}/deal/${flashDealModel.banner}',
                                  placeholder: (c, o) => Image.asset(
                                    Images.placeholder,
                                    fit: BoxFit.cover,
                                  ),
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : NoInternetOrDataScreen(isNoInternet: false),
              ]);
            }),
          ],
        ),
      ),
    );
  }
}
