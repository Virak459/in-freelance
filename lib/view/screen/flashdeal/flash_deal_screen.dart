import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/flash_deal_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class FlashDealScreen extends StatelessWidget {
  final FlashDealModel flashDealModel;
  const FlashDealScreen({Key key, @required this.flashDealModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<FlashDealProvider>(context, listen: false)
        .setFlashDeal(flashDealModel);
    Provider.of<FlashDealProvider>(context, listen: false)
        .getFlashDealProductList(true, context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(
          flashDealModel.title.toUpperCase(),
          style: titilliumBold.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              color: Colors.white,
              backgroundColor: ColorResources.getSecondary(context),
              onRefresh: () async {
                await Provider.of<FlashDealProvider>(context, listen: false)
                    .getFlashDealProductList(true, context);
              },
              child: Consumer<FlashDealProvider>(
                  builder: (context, flashDealProvider, child) {
                return Column(children: [
                  flashDealProvider.flashDealProductList.length != 0
                      ? Expanded(
                          child: ListView.builder(
                            itemCount: 1,
                            itemBuilder: (context, index) =>
                                StaggeredGridView.countBuilder(
                              itemCount:
                                  flashDealProvider.flashDealProductList.length,
                              crossAxisCount:
                                  Responsive.isMobile(context) ? 2 : 3,
                              mainAxisSpacing: Dimensions.PADDING_SIZE_DEFAULT,
                              crossAxisSpacing: Dimensions.PADDING_SIZE_DEFAULT,
                              addAutomaticKeepAlives: true,
                              padding: EdgeInsets.all(
                                  Dimensions.PADDING_SIZE_DEFAULT),
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              staggeredTileBuilder: (int index) =>
                                  StaggeredTile.fit(1),
                              itemBuilder: (BuildContext context, int index) {
                                Product productModel = flashDealProvider
                                    .flashDealProductList[index];
                                return ProductWidget(
                                  productModel: productModel,
                                );
                              },
                            ),
                          ),
                        )
                      : NoInternetOrDataScreen(isNoInternet: false),
                ]);
              }),
            ),
          ),
        ],
      ),
    );
  }
}
