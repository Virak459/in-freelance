import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/notification_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/shimmer_loading.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/splash/splash_screen.dart';
import 'package:provider/provider.dart';

class NotificationDetailScreen extends StatefulWidget {
  final NotificationModel notificationModel;
  final bool fromNotify;
  NotificationDetailScreen({
    @required this.notificationModel,
    this.fromNotify = false,
  });

  @override
  State<NotificationDetailScreen> createState() =>
      _NotificationDetailScreenState();
}

class _NotificationDetailScreenState extends State<NotificationDetailScreen> {
  ConfigModel configModel;
  _load(BuildContext context) async {
    await Provider.of<SplashProvider>(context, listen: false)
        .initConfig(context)
        .then((isSuccess) {
      configModel =
          Provider.of<SplashProvider>(context, listen: false).configModel;
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    _load(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: widget.fromNotify
            ? BackButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SplashScreen(),
                    ),
                  );
                },
              )
            : BackButton(),
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(widget.notificationModel.title),
      ),
      body: SingleChildScrollView(
        child: configModel != null
            ? Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color:
                            Theme.of(context).primaryColor.withOpacity(0.20)),
                    child: ClipRRect(
                      child: CachedNetworkImage(
                        // ignore: null_aware_in_condition
                        imageUrl: Uri.tryParse(widget.notificationModel.image)
                                ?.hasAbsolutePath
                            ? widget.notificationModel.image
                            : '${configModel.baseUrls.notificationImageUrl}/${widget.notificationModel.image}',
                        fit: BoxFit.fitHeight,
                        placeholder: (c, o) => Image.asset(
                          Images.placeholder,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Dimensions.PADDING_SIZE_LARGE),
                    child: Text(
                      widget.notificationModel.title,
                      textAlign: TextAlign.center,
                      style: titilliumSemiBold.copyWith(
                        color: Theme.of(context).primaryColor,
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_LARGE
                            : Dimensions.FONT_SIZE_LARGE_IPAD,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                    child: Text(
                      widget.notificationModel.description,
                      textAlign: TextAlign.center,
                      style: titilliumRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                      ),
                    ),
                  ),
                ],
              )
            : LoadingPage(),
      ),
    );
  }
}
