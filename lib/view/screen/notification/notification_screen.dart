import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/notification_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/date_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/notification_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/notification/notification_detail_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/notification/widget/notification_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class NotificationScreen extends StatelessWidget {
  final bool isBacButtonExist;
  NotificationScreen({this.isBacButtonExist = true});

  @override
  Widget build(BuildContext context) {
    Provider.of<NotificationProvider>(context, listen: false)
        .initNotificationList(context);
    ConfigModel configModel =
        Provider.of<SplashProvider>(context, listen: false).configModel;

    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('notification', context)),
      ),
      body: Column(children: [
        Expanded(
          child: Consumer<NotificationProvider>(
            builder: (context, notification, child) {
              return notification.notificationList != null
                  ? notification.notificationList.length != 0
                      ? RefreshIndicator(
                          color: Colors.white,
                          backgroundColor: ColorResources.getSecondary(context),
                          onRefresh: () async {
                            await Provider.of<NotificationProvider>(context,
                                    listen: false)
                                .initNotificationList(context);
                          },
                          child: ListView.builder(
                            itemCount:
                                Provider.of<NotificationProvider>(context)
                                    .notificationList
                                    .length,
                            padding: EdgeInsets.symmetric(
                                vertical: Dimensions.PADDING_SIZE_SMALL),
                            itemBuilder: (context, index) {
                              NotificationModel notificationModel =
                                  notification.notificationList[index];

                              return InkWell(
                                onTap: () {
                                  switch (configModel.showNotificationStyle) {
                                    case 1:
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) =>
                                              NotificationDetailScreen(
                                            notificationModel: notification
                                                .notificationList[index],
                                          ),
                                        ),
                                      );
                                      break;
                                    default:
                                      showDialog(
                                        context: context,
                                        builder: (context) =>
                                            NotificationDialog(
                                          notificationModel: notification
                                              .notificationList[index],
                                        ),
                                      );
                                  }
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      bottom: Dimensions.PADDING_SIZE_SMALL),
                                  decoration: BoxDecoration(
                                    color: ColorResources.getGrey(context),
                                  ),
                                  child: Container(
                                    margin: EdgeInsets.symmetric(
                                      horizontal: Dimensions.PADDING_SIZE_SMALL,
                                    ),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: ColorResources.getSecondary(
                                            context,
                                          ),
                                        ),
                                        borderRadius: BorderRadius.circular(
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                        )),
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(
                                                Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL,
                                              ),
                                              bottomLeft: Radius.circular(
                                                Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL,
                                              )),
                                          child: CachedNetworkImage(
                                            height: Responsive.isMobile(context)
                                                ? 100
                                                : 150,
                                            width: Responsive.isMobile(context)
                                                ? 100
                                                : 150,
                                            fit: BoxFit.cover,
                                            imageUrl: notificationModel.image !=
                                                    null
                                                ? '${Provider.of<SplashProvider>(context, listen: false).baseUrls.notificationImageUrl}/${notificationModel.image}'
                                                : Images.placeholder,
                                            placeholder: (c, o) => Image.asset(
                                              Images.placeholder,
                                              height:
                                                  Responsive.isMobile(context)
                                                      ? 100
                                                      : 150,
                                              width:
                                                  Responsive.isMobile(context)
                                                      ? 100
                                                      : 150,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: Dimensions.PADDING_SIZE_SMALL,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  (Responsive.isMobile(context)
                                                      ? 140
                                                      : 240),
                                              child: Text(
                                                notification
                                                    .notificationList[index]
                                                    .title,
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_LARGE
                                                      : Dimensions
                                                          .FONT_SIZE_LARGE_IPAD,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  (Responsive.isMobile(context)
                                                      ? 140
                                                      : 240),
                                              child: Text(
                                                DateConverter
                                                    .isoStringToLocalDateOnly(
                                                  context,
                                                  notificationModel.createdAt,
                                                ),
                                                style: titilliumRegular.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_DEFAULT
                                                        : Dimensions
                                                            .FONT_SIZE_DEFAULT_IPAD,
                                                    color:
                                                        ColorResources.getHint(
                                                            context)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  // child: ListTile(
                                  //   contentPadding: EdgeInsets.zero,
                                  //   leading: CachedNetworkImage(
                                  //     placeholder: Images.placeholder,
                                  //     height: 100,
                                  //     width: 100,
                                  //     fit: BoxFit.cover,
                                  //     image: notificationModel.image != null
                                  //         ? '${Provider.of<SplashProvider>(context, listen: false).baseUrls.notificationImageUrl}/${notificationModel.image}'
                                  //         : Images.placeholder,
                                  //     imageErrorBuilder: (c, o, s) =>
                                  //         Image.asset(
                                  //       Images.placeholder,
                                  //       height: 100,
                                  //       width: 100,
                                  //       fit: BoxFit.cover,
                                  //     ),
                                  //   ),
                                  //   title: Text(
                                  //       notification
                                  //           .notificationList[index].title,
                                  //       style: titilliumRegular.copyWith(
                                  //         fontSize: Dimensions.FONT_SIZE_SMALL,
                                  //       )),
                                  //   subtitle: Text(
                                  //     DateConverter.isoStringToLocalDateOnly(
                                  //       context,
                                  //       notificationModel.createdAt,
                                  //     ),
                                  //     style: titilliumRegular.copyWith(
                                  //         fontSize: Dimensions.FONT_SIZE_SMALL,
                                  //         color:
                                  //             ColorResources.getHint(context)),
                                  //   ),
                                  // ),
                                ),
                              );
                            },
                          ),
                        )
                      : NoInternetOrDataScreen(isNoInternet: false)
                  : NotificationShimmer();
            },
          ),
        ),
      ]),
    );
  }
}

class NotificationShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 10,
      padding: EdgeInsets.all(0),
      itemBuilder: (context, index) {
        return Container(
          height: 80,
          margin: EdgeInsets.only(bottom: Dimensions.PADDING_SIZE_SMALL),
          color: ColorResources.getGrey(context),
          alignment: Alignment.center,
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            enabled:
                Provider.of<NotificationProvider>(context).notificationList ==
                    null,
            child: ListTile(
              leading: CircleAvatar(child: Icon(Icons.notifications)),
              title: Container(height: 20, color: ColorResources.WHITE),
              subtitle:
                  Container(height: 10, width: 50, color: ColorResources.WHITE),
            ),
          ),
        );
      },
    );
  }
}
