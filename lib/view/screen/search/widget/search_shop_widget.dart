import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/search_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/shop/shop_list_widget.dart';
import 'package:provider/provider.dart';

class SearchShopWidget extends StatelessWidget {
  final List<ShopModel> shops;
  SearchShopWidget({this.shops});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  getTranslated('Search result for', context) +
                      ': \"${Provider.of<SearchProvider>(context).searchText}\" (${shops.length} ${getTranslated('shops', context)})',
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
        Expanded(
          child: RefreshIndicator(
            backgroundColor: ColorResources.getSecondary(context),
            color: Colors.white,
            onRefresh: () async {
              Provider.of<SearchProvider>(context, listen: false).searchShop(
                  Provider.of<SearchProvider>(context, listen: false)
                      .searchText,
                  context);
            },
            child: Consumer<SearchProvider>(
                builder: (context, searchProvider, child) {
              if (searchProvider.searchShopList.length > 0) {
                return ListView(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  children: List.generate(searchProvider.searchShopList.length,
                      (int index) {
                    ShopModel shopModel = searchProvider.searchShopList[index];
                    return ShopListWidget(shopModel: shopModel);
                  }),
                );
              }

              return NoInternetOrDataScreen(isNoInternet: false);
            }),
          ),
        ),
      ],
    );
  }
}
