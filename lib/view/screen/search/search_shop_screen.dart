import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/search_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_shimmer.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/search_widget.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_product_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_shop_widget.dart';

import 'package:provider/provider.dart';

class SearchShopScreen extends StatefulWidget {
  @override
  State<SearchShopScreen> createState() => _SearchShopScreenState();
}

_load(BuildContext context) {
  Provider.of<SearchProvider>(context, listen: false).cleanSearchShop();
  Provider.of<SearchProvider>(context, listen: false).initHistoryList();
}

class _SearchShopScreenState extends State<SearchShopScreen> {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero, () async {
      _load(context);
    });

    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      resizeToAvoidBottomInset: true,
      body: Container(
        child: Column(
          children: [
            SearchWidget(
              hintText: getTranslated('SEARCH_NOW', context),
              onSubmit: (String text) {
                Provider.of<SearchProvider>(context, listen: false)
                    .searchShop(text, context);
                Provider.of<SearchProvider>(context, listen: false)
                    .saveSearchAddress(text);
              },
              // onTextChanged: (String val) {},
              onClearPressed: () {
                Provider.of<SearchProvider>(context, listen: false)
                    .cleanSearchShop();
              },
            ),
            Consumer<SearchProvider>(
              builder: (context, searchProvider, child) {
                return !searchProvider.isClear
                    ? searchProvider.searchShopList != null
                        ? searchProvider.searchShopList.length > 0
                            ? Expanded(
                                child: SearchShopWidget(
                                  shops: searchProvider.searchShopList,
                                ),
                              )
                            : Expanded(
                                child:
                                    NoInternetOrDataScreen(isNoInternet: false),
                              )
                        : Expanded(
                            child: ProductShimmer(
                                isHomePage: false,
                                isEnabled: Provider.of<SearchProvider>(context)
                                        .searchShopList ==
                                    null),
                          )
                    : searchProvider.historyList.length > 0
                      ? Expanded(
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          getTranslated(
                                              'SEARCH_HISTORY', context),
                                          style: robotoBold.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          )),
                                      InkWell(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          onTap: () {
                                            Provider.of<SearchProvider>(context,
                                                    listen: false)
                                                .clearSearchAddress();
                                          },
                                          child: Container(
                                              padding: EdgeInsets.all(5),
                                              child: Text(
                                                getTranslated(
                                                    'REMOVE', context),
                                                style: titilliumRegular.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_DEFAULT
                                                        : Dimensions
                                                            .FONT_SIZE_DEFAULT_IPAD,
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              )))
                                    ],
                                  ),
                                  Expanded(
                                    child: SingleChildScrollView(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          // Row(
                                          //   mainAxisAlignment:
                                          //       MainAxisAlignment.spaceBetween,
                                          //   children: [
                                          //     SizedBox(
                                          //       width: 10,
                                          //     ),
                                          //     InkWell(
                                          //       onTap: () {
                                          //         //_key.currentState.openEndDrawer();
                                          //         showModalBottomSheet(
                                          //           context: context,
                                          //           isScrollControlled: true,
                                          //           backgroundColor: Colors.transparent,
                                          //           builder: (c) =>
                                          //               SearchFilterBottomSheet(),
                                          //         );
                                          //       },
                                          //       child: Transform.rotate(
                                          //         angle: 90 * math.pi / 180,
                                          //         child: Icon(Icons.tune),
                                          //       ),
                                          //     ),
                                          //   ],
                                          // ),

                                          Consumer<SearchProvider>(builder:
                                              (context, searchProvider, child) {
                                            return Column(
                                              children: List.generate(
                                                searchProvider
                                                    .historyList.length,
                                                ((index) {
                                                  return InkWell(
                                                    onTap: () {
                                                      Provider.of<SearchProvider>(
                                                              context,
                                                              listen: false)
                                                          .searchShop(
                                                              searchProvider
                                                                      .historyList[
                                                                  index],
                                                              context);
                                                    },
                                                    child: Container(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                vertical: 3.0,
                                                                horizontal:
                                                                    3.0),
                                                        padding: EdgeInsets.symmetric(
                                                            vertical: Dimensions
                                                                .PADDING_SIZE_EXTRA_SMALL,
                                                            horizontal: Dimensions
                                                                .PADDING_SIZE_SMALL),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Row(
                                                              children: [
                                                                Image.asset(
                                                                  Images.search,
                                                                  color: Colors
                                                                      .grey,
                                                                  width: Responsive
                                                                          .isMobile(
                                                                              context)
                                                                      ? Dimensions
                                                                          .ICON_SIZE_DEFAULT
                                                                      : Dimensions
                                                                          .ICON_SIZE_DEFAULT_IPAD,
                                                                ),
                                                                SizedBox(
                                                                  width: Dimensions
                                                                      .PADDING_SIZE_EXTRA_SMALL,
                                                                ),
                                                                SizedBox(
                                                                  width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width -
                                                                      (Responsive.isMobile(
                                                                              context)
                                                                          ? 100
                                                                          : 120),
                                                                  child: Text(
                                                                    Provider.of<SearchProvider>(context,
                                                                                listen: false)
                                                                            .historyList[index] ??
                                                                        "",
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    style: titilliumRegular
                                                                        .copyWith(
                                                                      color: ColorResources
                                                                          .getHint(
                                                                              context),
                                                                      fontSize: Responsive.isMobile(
                                                                              context)
                                                                          ? Dimensions
                                                                              .FONT_SIZE_DEFAULT
                                                                          : Dimensions
                                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            Transform.rotate(
                                                              angle: 1,
                                                              child: Icon(
                                                                Icons
                                                                    .arrow_back,
                                                                color:
                                                                    Colors.grey,
                                                                size: Responsive
                                                                        .isMobile(
                                                                            context)
                                                                    ? Dimensions
                                                                        .ICON_SIZE_DEFAULT
                                                                    : Dimensions
                                                                        .ICON_SIZE_DEFAULT_IPAD,
                                                              ),
                                                            )
                                                          ],
                                                        )),
                                                  );
                                                }),
                                              ),
                                            );
                                          }),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : SizedBox(
                            height: 0,
                          );
              },
            ),
          ],
        ),
      ),
    );
  }
}
