import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/bank_info_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:provider/provider.dart';

class CustomCheckBox2 extends StatelessWidget {
  final DataBank bank;
  final int index;
  CustomCheckBox2({@required this.bank, @required this.index});

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(
      builder: (context, order, child) {
        if (order.bankpaymentMethodIndex == 0) {
          order.setPaymentMethod(index, bank.bankName);
        }
        return InkWell(
          onTap: () => order.setPaymentMethod(index, bank.bankName),
          child: Row(children: [
            Transform.scale(
              scale: Responsive.isMobile(context) ? 1 : 2,
              child: Checkbox(
                value: order.bankpaymentMethodIndex == index,
                activeColor: ColorResources.getSecondary(context),
                onChanged: (bool isChecked) =>
                    order.setPaymentMethod(index, bank.bankName),
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Visibility(
                    visible: bank.bankLogo != null ? true : false,
                    child: CachedNetworkImage(
                      imageUrl: bank.bankLogo,
                      height: Responsive.isMobile(context) ? 60 : 120,
                      width: Responsive.isMobile(context) ? 60 : 120,
                      fit: BoxFit.cover,
                      placeholder: (c, o) => Image.asset(
                        Images.placeholder,
                        fit: BoxFit.cover,
                        height: Responsive.isMobile(context) ? 60 : 120,
                        width: Responsive.isMobile(context) ? 60 : 120,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            getTranslated('bank', context) + ' : ',
                            style: titilliumRegular.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: order.bankpaymentMethodIndex == index
                                  ? Theme.of(context).textTheme.bodyText1.color
                                  : ColorResources.getGainsBoro(context),
                            ),
                          ),
                          Text(
                            bank.bankName,
                            style: titilliumBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: order.bankpaymentMethodIndex == index
                                  ? Theme.of(context).textTheme.bodyText1.color
                                  : ColorResources.getGainsBoro(context),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            getTranslated('account_name', context) + ' : ',
                            style: titilliumRegular.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: order.bankpaymentMethodIndex == index
                                  ? Theme.of(context).textTheme.bodyText1.color
                                  : ColorResources.getGainsBoro(context),
                            ),
                          ),
                          Text(
                            bank.holderName,
                            style: titilliumBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: order.bankpaymentMethodIndex == index
                                  ? Theme.of(context).textTheme.bodyText1.color
                                  : ColorResources.getGainsBoro(context),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            getTranslated('account_number', context) + ' : ',
                            style: titilliumRegular.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: order.bankpaymentMethodIndex == index
                                  ? Theme.of(context).textTheme.bodyText1.color
                                  : ColorResources.getGainsBoro(context),
                            ),
                          ),
                          Text(
                            bank.accountNo,
                            style: titilliumBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: order.bankpaymentMethodIndex == index
                                  ? Theme.of(context).textTheme.bodyText1.color
                                  : ColorResources.getGainsBoro(context),
                            ),
                          ),
                        ],
                      ),
                      Visibility(
                        visible: bank.bankUrl != null,
                        child: InkWell(
                          onTap: () {
                            if (bank.bankUrl != null) {
                              App.launchUrl(bank.bankUrl);
                            }
                          },
                          child: Row(
                            children: [
                              Text(
                                getTranslated('open_link', context),
                                style: titilliumBold.copyWith(
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_DEFAULT
                                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  color: order.bankpaymentMethodIndex == index
                                      ? Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          .color
                                      : ColorResources.getGainsBoro(context),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ]),
        );
      },
    );
  }
}
