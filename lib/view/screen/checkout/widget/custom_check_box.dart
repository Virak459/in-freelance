import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:provider/provider.dart';

class CustomCheckBox extends StatelessWidget {
  final String title;
  final int index;
  final Function onChanged;
  CustomCheckBox({@required this.title, @required this.index, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(
      builder: (context, order, child) {
        return InkWell(
          onTap: () => order.setPaymentMethod(index, title),
          child: Row(children: [
            Transform.scale(
              scale: Responsive.isMobile(context) ? 1.0 : 1.5,
              child: Checkbox(
                  value: order.bankpaymentMethodIndex == index,
                  activeColor: ColorResources.getSecondary(context),
                  onChanged: (bool isChecked) {
                    order.setPaymentMethod(index, title);
                    if (onChanged != null) {
                      onChanged(index);
                    }
                  }),
            ),
            Expanded(
              child: Text(getTranslated(title, context),
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                    color: order.bankpaymentMethodIndex == index
                        ? Theme.of(context).textTheme.bodyText1.color
                        : Theme.of(context).textTheme.bodyText1.color,
                  )),
            ),
          ]),
        );
      },
    );
  }
}
