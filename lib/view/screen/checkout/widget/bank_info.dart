import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/bank_info_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:provider/provider.dart';

class BankInfo extends StatelessWidget {
  final DataBank bank;
  BankInfo({@required this.bank});

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(
      builder: (context, order, child) {
        return InkWell(
          onTap: () => {},
          child: Row(children: [
            CachedNetworkImage(
          
              imageUrl: bank.bankLogo,
              fit: BoxFit.cover,
              width: Responsive.isMobile(context) ? 60 : 160,
              height: Responsive.isMobile(context) ? 60 : 160,
              placeholder: (c, o) => Image.asset(
                Images.placeholder,
                fit: BoxFit.cover,
                width: Responsive.isMobile(context) ? 60 : 160,
                height: Responsive.isMobile(context) ? 60 : 160,
              ),
            ),
            SizedBox(
              width: Dimensions.PADDING_SIZE_DEFAULT,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  getTranslated('bank_name', context) + ": ${bank.bankName}",
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  ),
                ),
                Text(
                  getTranslated('account_name', context) +
                      ": ${bank.holderName}",
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  ),
                ),
                Text(
                  getTranslated('account_number', context) +
                      ": ${bank.accountNo}",
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_DEFAULT
                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                  ),
                ),
              ],
            )
          ]),
        );
      },
    );
  }
}
