import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/body/order_place_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/address_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/bank_info_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/cart_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/chosen_shipping_method.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';

import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/coupon_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/order_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/amount_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/my_dialog.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/widget/list_cart.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/checkout/widget/address_bottom_sheet.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/checkout/widget/bank_info.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/checkout/widget/custom_check_box.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/review_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class CheckoutScreen extends StatefulWidget {
  final List<CartModel> cartList;
  final bool fromProductDetails;
  final int sellerId;
  CheckoutScreen(
      {@required this.cartList,
      this.fromProductDetails = false,
      this.sellerId});

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldKey =
      GlobalKey<ScaffoldMessengerState>();
  final TextEditingController _controller = TextEditingController();
  double _order = 0;
  bool _digitalPayment = false;
  File _files = File('');
  String image = "";
  ImagePicker imagePicker = ImagePicker();

  @override
  void initState() {
    super.initState();
    Provider.of<ProfileProvider>(context, listen: false)
        .initAddressList(context);
    Provider.of<ProfileProvider>(context, listen: false)
        .initAddressTypeList(context);
    Provider.of<CouponProvider>(context, listen: false).removePrevCouponData();
    Provider.of<CartProvider>(context, listen: false).getCartDataAPI(context);
    Provider.of<CartProvider>(context, listen: false)
        .getChosenShippingMethod(context);

    _digitalPayment = Provider.of<SplashProvider>(context, listen: false)
        .configModel
        .digitalPayment;
    if (_digitalPayment) {
      Provider.of<CartProvider>(context, listen: false)
          .getPaymentInfor(context);
    }
    List<AddressModel> addressList =
        Provider.of<ProfileProvider>(context, listen: false).addressList;

    if (addressList != null && addressList.length > 0) {
      Provider.of<OrderProvider>(context, listen: false).setAddressIndex(0);
    }
  }

  void onCallbackUploadImage(String imageResponse) {
    print("CheckImageName $imageResponse");
    image = imageResponse;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double totalOrderAmount = 0.0;
    double shippingFee = 0.0;
    double discount = 0.0;
    double tax = 0.0;
    ConfigModel configModel =
        Provider.of<SplashProvider>(context, listen: true).configModel;

    if (!configModel.cashOnDelivery) {
      Provider.of<CartProvider>(context, listen: false)
          .setPaymentMethod(1, 'digital_payment');

      setState(() {});
    }
    List<CartModel> cartList =
        Provider.of<CartProvider>(context, listen: true).cartList;
    List<ChosenShippingMethodModel> chosenShippingList =
        Provider.of<CartProvider>(context, listen: true).chosenShippingList;

    for (int i = 0; i < cartList.length; i++) {
      totalOrderAmount +=
          (cartList[i].price - cartList[i].discount) * cartList[i].quantity;
      discount += cartList[i].discount * cartList[i].quantity;
      tax += cartList[i].tax * cartList[i].quantity;
    }
    for (int i = 0; i < chosenShippingList.length; i++) {
      shippingFee += chosenShippingList[i].shippingCost;
    }

    _order = totalOrderAmount + discount;
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('checkout', context)),
      ),
      bottomNavigationBar: Container(
        height: App.height(context) * 9,
        padding: EdgeInsets.symmetric(
          horizontal: Dimensions.PADDING_SIZE_LARGE,
          vertical: Dimensions.PADDING_SIZE_DEFAULT,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(color: Colors.grey.withOpacity(.5)),
          ),
        ),
        child: Consumer<OrderProvider>(
          builder: (context, order, child) {
            // double _shippingCost = Provider.of<CartProvider>(context, listen: false).shippingMethodIndex != null ? Provider.of<CartProvider>(context, listen: false).shippingMethodList[Provider.of<CartProvider>(context, listen: false).shippingMethodIndex[0]].cost : 0;
            // double _couponDiscount = Provider.of<CouponProvider>(context).discount != null ? Provider.of<CouponProvider>(context).discount : 0;
            return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              !Provider.of<OrderProvider>(context).isLoading
                  ? Builder(
                      builder: (context) => SizedBox(
                        height: App.height(context) * 6,
                        width: App.width(context) * 30,
                        child: TextButton(
                          onPressed: () async {
                            if (Provider.of<OrderProvider>(context,
                                            listen: false)
                                        .addressIndex ==
                                    null ||
                                Provider.of<ProfileProvider>(context,
                                            listen: false)
                                        .addressList
                                        .length ==
                                    0) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                    content: Text(
                                      getTranslated(
                                          'select_a_shipping_address', context),
                                      style: titilliumRegular.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_DEFAULT
                                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                      ),
                                    ),
                                    backgroundColor: Colors.red),
                              );
                            } else if (Provider.of<CartProvider>(context,
                                            listen: false)
                                        .bankpaymentMethodIndex ==
                                    1 &&
                                _files.path.isEmpty) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                      content: Text(
                                        getTranslated(
                                            'select_a_image_payment', context),
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                        ),
                                      ),
                                      backgroundColor: Colors.red));
                            } else {
                              List<CartModel> _cartList = [];
                              _cartList.addAll(widget.cartList);
                              for (int index = 0;
                                  index < widget.cartList.length;
                                  index++) {
                                for (int i = 0;
                                    i <
                                        Provider.of<CartProvider>(context,
                                                listen: false)
                                            .chosenShippingList
                                            .length;
                                    i++) {
                                  if (Provider.of<CartProvider>(context,
                                              listen: false)
                                          .chosenShippingList[i]
                                          .cartGroupId ==
                                      widget.cartList[index].cartGroupId) {
                                    _cartList[index].shippingMethodId =
                                        Provider.of<CartProvider>(context,
                                                listen: false)
                                            .chosenShippingList[i]
                                            .id;
                                    print(
                                        "Checkhere2 ${_cartList[index].shippingMethodId}");

                                    break;
                                  }
                                }
                              }

                              double couponDiscount =
                                  Provider.of<CouponProvider>(context,
                                                  listen: false)
                                              .discount !=
                                          null
                                      ? Provider.of<CouponProvider>(context,
                                              listen: false)
                                          .discount
                                      : 0;
                              String couponCode = Provider.of<CouponProvider>(
                                              context,
                                              listen: false)
                                          .discount !=
                                      null
                                  ? Provider.of<CouponProvider>(context,
                                          listen: false)
                                      .coupon
                                      .code
                                  : '';

                              Provider.of<OrderProvider>(context, listen: false)
                                  .placeOrder(
                                      OrderPlaceModel(
                                        CustomerInfo(
                                          Provider.of<ProfileProvider>(context,
                                                  listen: false)
                                              .addressList[
                                                  Provider.of<OrderProvider>(
                                                          context,
                                                          listen: false)
                                                      .addressIndex]
                                              .id
                                              .toString(),
                                          Provider.of<ProfileProvider>(context,
                                                  listen: false)
                                              .addressList[
                                                  Provider.of<OrderProvider>(
                                                          context,
                                                          listen: false)
                                                      .addressIndex]
                                              .address,
                                        ),
                                        _cartList,
                                        Provider.of<CartProvider>(context,
                                                        listen: false)
                                                    .bankpaymentMethodIndex ==
                                                1
                                            ? 'digital_payment'
                                            : '',
                                        couponDiscount,
                                      ),
                                      _callback,
                                      _cartList,
                                      Provider.of<ProfileProvider>(context,
                                              listen: false)
                                          .addressList[
                                              Provider.of<OrderProvider>(
                                                      context,
                                                      listen: false)
                                                  .addressIndex]
                                          .id
                                          .toString(),
                                      couponCode,
                                      bankName: Provider.of<CartProvider>(
                                              context,
                                              listen: false)
                                          .bankpaymentName
                                          .toString(),
                                      image: image);

                              // else {
                              //   print("Checkhere4");

                              //   String userID =
                              //       await Provider.of<ProfileProvider>(
                              //               context,
                              //               listen: false)
                              //           .getUserInfo(context);
                              //   Navigator.push(
                              //       context,
                              //       MaterialPageRoute(
                              //           builder: (_) => PaymentScreen(
                              //                 customerID: userID,
                              //                 addressID: Provider.of<
                              //                             ProfileProvider>(
                              //                         context,
                              //                         listen: false)
                              //                     .addressList[Provider.of<
                              //                                 OrderProvider>(
                              //                             context,
                              //                             listen: false)
                              //                         .addressIndex]
                              //                     .id
                              //                     .toString(),
                              //                 couponCode:
                              //                     Provider.of<CouponProvider>(
                              //                                     context,
                              //                                     listen:
                              //                                         false)
                              //                                 .discount !=
                              //                             null
                              //                         ? Provider.of<
                              //                                     CouponProvider>(
                              //                                 context,
                              //                                 listen: false)
                              //                             .coupon
                              //                             .code
                              //                         : '',
                              //                 tax: widget.tax,
                              //                 totalOrderAmount:
                              //                     widget.totalOrderAmount,
                              //                 shippingFee:
                              //                     widget.shippingFee,
                              //                 discount: widget.discount,
                              //               )));
                              // }
                            }
                          },
                          style: TextButton.styleFrom(
                            backgroundColor: ColorResources.getPrimary(context),
                            alignment: Alignment.center,
                            // padding: EdgeInsets.only(bottom: 4),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                          child: Text(getTranslated('process_order', context),
                              style: titilliumSemiBold.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                color: ColorResources.WHITE,
                              )),
                        ),
                      ),
                    )
                  : Container(
                      height: 100,
                      width: 100,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                        ColorResources.getSecondary(context),
                      )),
                    ),
            ]);
          },
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.all(0),
                children: [
                  // Shipping Details
                  Container(
                    margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                    padding: EdgeInsets.symmetric(
                      vertical: Dimensions.PADDING_SIZE_SMALL,
                      horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                    ),
                    decoration: BoxDecoration(
                        color: Theme.of(context).highlightColor,
                        border: Border.all(
                            color: ColorResources.getSecondary(context)),
                        borderRadius: BorderRadius.circular(
                            Dimensions.PADDING_SIZE_DEFAULT)),
                    child: Column(children: [
                      InkWell(
                        onTap: () => showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            backgroundColor: Colors.transparent,
                            builder: (context) => AddressBottomSheet()),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width -
                                          80,
                                      child: Center(
                                        child: Text(
                                          Provider.of<OrderProvider>(context)
                                                          .addressIndex ==
                                                      null ||
                                                  Provider.of<ProfileProvider>(
                                                              context,
                                                              listen: false)
                                                          .addressList
                                                          .length ==
                                                      0
                                              ? '${getTranslated('add_your_address', context)}'
                                              : (Provider.of<ProfileProvider>(
                                                              context,
                                                              listen: false)
                                                          .addressList
                                                          .length >
                                                      0
                                                  ? "${Provider.of<ProfileProvider>(context, listen: false).addressList[Provider.of<OrderProvider>(context, listen: false).addressIndex].address}, ${Provider.of<ProfileProvider>(context, listen: false).addressList[Provider.of<OrderProvider>(context, listen: false).addressIndex].city}"
                                                  : ""),
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions.FONT_SIZE_LARGE
                                                    : Dimensions
                                                        .FONT_SIZE_LARGE_IPAD,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                              Icon(
                                Provider.of<OrderProvider>(context)
                                                .addressIndex ==
                                            null ||
                                        Provider.of<ProfileProvider>(context,
                                                    listen: false)
                                                .addressList
                                                .length ==
                                            0
                                    ? Icons.add
                                    : Icons.edit,
                                size: Responsive.isMobile(context)
                                    ? Dimensions.ICON_SIZE_DEFAULT
                                    : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                color: ColorResources.getSecondary(context),
                              ),
                            ]),
                      ),

                      // InkWell(
                      //   onTap: () => showModalBottomSheet(
                      //     context: context, isScrollControlled: true, backgroundColor: Colors.transparent,
                      //     builder: (context) => ShippingMethodBottomSheet(),
                      //   ),
                      //   child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      //     Text(getTranslated('SHIPPING_PARTNER', context), style: titilliumRegular),
                      //     Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                      //       Text(
                      //         Provider.of<OrderProvider>(context).shippingIndex == null ? getTranslated('select_shipping_method', context)
                      //             : Provider.of<OrderProvider>(context, listen: false).shippingList[Provider.of<OrderProvider>(context, listen: false).shippingIndex].title,
                      //         style: titilliumSemiBold.copyWith(color: ColorResources.getPrimary(context)),
                      //         maxLines: 1,
                      //         overflow: TextOverflow.ellipsis,
                      //       ),
                      //       SizedBox(width: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                      //       Image.asset(Images.EDIT_TWO, width: 15, height: 15, color: ColorResources.getPrimary(context)),
                      //     ]),
                      //   ]),
                      // ),
                    ]),
                  ),
                  // Order Details
                  Container(
                    margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                    padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                    color: Theme.of(context).highlightColor,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListCart(),
                          // Coupon
                          Row(children: [
                            Expanded(
                              child: SizedBox(
                                height: Responsive.isMobile(context) ? 40 : 50,
                                child: TextField(
                                    style: titilliumRegular.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                    controller: _controller,
                                    decoration: InputDecoration(
                                      hintText:
                                          getTranslated('have_coupon', context),
                                      hintStyle: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_SMALL
                                              : Dimensions.FONT_SIZE_SMALL_IPAD,
                                          color:
                                              ColorResources.HINT_TEXT_COLOR),
                                      filled: true,
                                      fillColor:
                                          ColorResources.getIconBg(context),
                                      border: InputBorder.none,
                                    )),
                              ),
                            ),
                            SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                            !Provider.of<CouponProvider>(context).isLoading
                                ? ElevatedButton(
                                    onPressed: () {
                                      if (_controller.text.isNotEmpty) {
                                        Provider.of<CouponProvider>(context,
                                                listen: false)
                                            .initCoupon(
                                                _controller.text, _order)
                                            .then((value) {
                                          if (value > 0) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                                    content: Text(
                                                      'You got ${PriceConverter.convertPrice(context, value)} discount',
                                                      style: titilliumRegular
                                                          .copyWith(
                                                        fontSize: Responsive
                                                                .isMobile(
                                                                    context)
                                                            ? Dimensions
                                                                .FONT_SIZE_DEFAULT
                                                            : Dimensions
                                                                .FONT_SIZE_DEFAULT_IPAD,
                                                      ),
                                                    ),
                                                    backgroundColor:
                                                        Colors.green));
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                              content: Text(
                                                getTranslated(
                                                    'invalid_coupon_or',
                                                    context),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ),
                                              backgroundColor: Colors.red,
                                            ));
                                          }
                                        });
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: ColorResources.getGreen(context),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                    ),
                                    child: Text(
                                      getTranslated('APPLY', context),
                                      style: titilliumRegular.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_DEFAULT
                                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                      ),
                                    ),
                                  )
                                : CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Theme.of(context).primaryColor)),
                          ]),
                        ]),
                  ),

                  // Total bill
                  Container(
                    margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                    padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                    color: Theme.of(context).highlightColor,
                    child: Consumer<OrderProvider>(
                      builder: (context, order, child) {
                        //_shippingCost = order.shippingIndex != null ? order.shippingList[order.shippingIndex].cost : 0;
                        double _couponDiscount =
                            Provider.of<CouponProvider>(context).discount !=
                                    null
                                ? Provider.of<CouponProvider>(context).discount
                                : 0;

                        return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TitleRow(title: getTranslated('TOTAL', context)),
                              AmountWidget(
                                  title: getTranslated('ORDER', context),
                                  amount: PriceConverter.convertPrice(
                                      context, _order)),
                              Visibility(
                                visible: Provider.of<SplashProvider>(context,
                                        listen: false)
                                    .configModel
                                    .shippingMethodEnable,
                                child: AmountWidget(
                                    title:
                                        getTranslated('SHIPPING_FEE', context),
                                    amount: PriceConverter.convertPrice(
                                        context, shippingFee)),
                              ),
                              AmountWidget(
                                  title: getTranslated('DISCOUNT', context),
                                  amount: PriceConverter.convertPrice(
                                      context, discount)),
                              AmountWidget(
                                  title:
                                      getTranslated('coupon_voucher', context),
                                  amount: PriceConverter.convertPrice(
                                      context, _couponDiscount)),
                              AmountWidget(
                                  title: getTranslated('TAX', context),
                                  amount: PriceConverter.convertPrice(
                                      context, tax)),
                              Divider(
                                  height: 5,
                                  color: Theme.of(context).hintColor),
                              AmountWidget(
                                  title:
                                      getTranslated('TOTAL_PAYABLE', context),
                                  amount: PriceConverter.convertPrice(
                                      context,
                                      (_order +
                                          shippingFee -
                                          discount -
                                          _couponDiscount +
                                          tax))),
                            ]);
                      },
                    ),
                  ),

                  // Payment Method
                  Container(
                    margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                    padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                    color: Theme.of(context).highlightColor,
                    child: Column(children: [
                      TitleRow(title: getTranslated('payment_method', context)),
                      SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                      configModel.cashOnDelivery
                          ? CustomCheckBox(
                              title: 'cash_on_delivery',
                              index: 0,
                              onChanged: (index) {
                                Provider.of<CartProvider>(context,
                                        listen: false)
                                    .setPaymentMethod(0, 'cash_on_delivery');
                                setState(() {});
                              },
                            )
                          : SizedBox(),
                      _digitalPayment
                          ? CustomCheckBox(
                              title: 'digital_payment',
                              index: 1,
                              onChanged: (index) {
                                Provider.of<CartProvider>(context,
                                        listen: false)
                                    .setPaymentMethod(1, 'digital_payment');

                                setState(() {});
                              },
                            )
                          : SizedBox.shrink(),
                      _digitalPayment
                          ? Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: Dimensions.PADDING_SIZE_DEFAULT),
                              padding: EdgeInsets.all(
                                  Dimensions.PADDING_SIZE_DEFAULT),
                              decoration: BoxDecoration(
                                  color: Theme.of(context).highlightColor,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Consumer<CartProvider>(
                                  builder: (context, bank, child) {
                                List<DataBank> _bankInfor = [];
                                if (bank.bankInfoModel != null &&
                                    !bank.isLoading) {
                                  _bankInfor.addAll(bank.bankInfoModel.data);

                                  if (bank.bankpaymentMethodIndex == 1) {
                                    return Column(
                                      children: [
                                        ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: _bankInfor.length,
                                            padding: EdgeInsets.all(0),
                                            physics:
                                                NeverScrollableScrollPhysics(),
                                            itemBuilder:
                                                (BuildContext context, index) {
                                              return BankInfo(
                                                  bank: _bankInfor[index]);
                                            }),
                                        //Upload Invoice bank
                                        Container(
                                          child: Row(
                                            children: [
                                              Expanded(
                                                  child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                      getTranslated(
                                                          'upload_bank_invoice',
                                                          context),
                                                      style:
                                                          robotoBold.copyWith(
                                                        fontSize: Responsive
                                                                .isMobile(
                                                                    context)
                                                            ? Dimensions
                                                                .FONT_SIZE_DEFAULT
                                                            : Dimensions
                                                                .FONT_SIZE_DEFAULT_IPAD,
                                                      )),
                                                  Text(
                                                      getTranslated(
                                                          'warning_payment_before',
                                                          context),
                                                      style: robotoRegular
                                                          .copyWith(
                                                        fontSize: Responsive
                                                                .isMobile(
                                                                    context)
                                                            ? Dimensions
                                                                .FONT_SIZE_SMALL
                                                            : Dimensions
                                                                .FONT_SIZE_SMALL_IPAD,
                                                      )),
                                                ],
                                              )),
                                              SizedBox(
                                                height: Responsive.isMobile(
                                                        context)
                                                    ? App.height(context) * 10
                                                    : App.height(context) * 20,
                                                child: Padding(
                                                  padding: EdgeInsets.only(
                                                      right: Dimensions
                                                          .PADDING_SIZE_SMALL),
                                                  child: InkWell(
                                                    onTap: () async {
                                                      PickedFile pickedFile =
                                                          await imagePicker
                                                              .getImage(
                                                                  source:
                                                                      ImageSource
                                                                          .gallery,
                                                                  maxWidth:
                                                                      1024,
                                                                  maxHeight:
                                                                      1024,
                                                                  imageQuality:
                                                                      100);
                                                      if (pickedFile != null) {
                                                        _files = File(
                                                            pickedFile.path);
                                                        await Provider.of<
                                                                    OrderProvider>(
                                                                context,
                                                                listen: false)
                                                            .uploadImage(_files,
                                                                onCallbackUploadImage);
                                                        setState(() {});
                                                      }
                                                    },
                                                    child: _files.path.isEmpty
                                                        ? Container(
                                                            height: 50,
                                                            width: 50,
                                                            alignment: Alignment
                                                                .center,
                                                            child: Stack(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Icon(
                                                                  Icons
                                                                      .cloud_upload_outlined,
                                                                  color: ColorResources
                                                                      .getSecondary(
                                                                          context),
                                                                  size: Responsive
                                                                          .isMobile(
                                                                              context)
                                                                      ? Dimensions
                                                                          .ICON_SIZE_DEFAULT
                                                                      : Dimensions
                                                                          .ICON_SIZE_DEFAULT_IPAD,
                                                                ),
                                                                CustomPaint(
                                                                  size: Size(
                                                                      Responsive.isMobile(
                                                                              context)
                                                                          ? 100
                                                                          : 200,
                                                                      Responsive.isMobile(
                                                                              context)
                                                                          ? 40
                                                                          : 80),
                                                                  foregroundPainter: new MyPainter(
                                                                      completeColor:
                                                                          ColorResources.getSecondary(
                                                                              context),
                                                                      width: 2),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        : ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5),
                                                            child: Image.file(
                                                                _files,
                                                                height: App.height(
                                                                        context) *
                                                                    10,
                                                                width: App.width(
                                                                        context) *
                                                                    20,
                                                                fit: BoxFit
                                                                    .cover),
                                                          ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  }
                                }
                                return SizedBox.shrink();
                              }),
                            )
                          : SizedBox(),
                    ]),
                  ),
                ]),
          ),
        ],
      ),
    );
  }

  void _callback(bool isSuccess, String message, String orderID,
      List<CartModel> carts) async {
    if (isSuccess) {
      //jaman baba
      // Provider.of<CartProvider>(context, listen: false).removeCheckoutProduct(carts);
      Provider.of<ProductProvider>(context, listen: false).getLatestProductList(
        1,
        context,
        reload: true,
      );
      // if (Provider.of<OrderProvider>(context, listen: false)
      //         .paymentMethodIndex ==
      //     0) {

      // } else {}
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => DashBoardScreen()),
          (route) => false);
      showAnimatedDialog(
          context,
          MyDialog(
            icon: Icons.check,
            title: getTranslated('order_placed', context),
            description: getTranslated('your_order_placed', context),
            isFailed: false,
          ),
          dismissible: false,
          isFlip: true);
      Provider.of<OrderProvider>(context, listen: false).stopLoader();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            message,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    }
  }
}

class PaymentButton extends StatelessWidget {
  final String image;
  final Function onTap;
  PaymentButton({@required this.image, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 45,
        margin: EdgeInsets.symmetric(
            horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(width: 2, color: ColorResources.getGrey(context)),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Image.asset(image),
      ),
    );
  }
}
