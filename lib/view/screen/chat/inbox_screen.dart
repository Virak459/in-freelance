import 'dart:convert';
import 'dart:developer';
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';
// ignore: unused_import
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/chat_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/date_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/chat_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/not_loggedin_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/shop_chat_screen.dart';
import 'package:provider/provider.dart';
import 'package:pusher_channels_flutter/pusher_channels_flutter.dart';
import 'package:shimmer/shimmer.dart';

// ignore: must_be_immutable
class InboxScreen extends StatefulWidget {
  final bool isBackButtonExist;
  InboxScreen({this.isBackButtonExist = true});

  @override
  State<InboxScreen> createState() => _InboxScreenState();
}

class _InboxScreenState extends State<InboxScreen> {
  bool isFirstTime = true;

  PusherChannelsFlutter pusher = PusherChannelsFlutter.getInstance();

  setPusher() async {
    ConfigModel configModel =
        Provider.of<SplashProvider>(context, listen: false).configModel;
    await Provider.of<ProfileProvider>(context, listen: false)
        .getUserInfo(context);

    try {
      await pusher.init(
        apiKey: configModel.pusher.key ?? AppConstants.PUSHER_KEY,
        cluster: configModel.pusher.cluster ?? AppConstants.PUSHER_CLUSTER,
        onError: onError,
        onEvent: onEvent,
      );

      await pusher.subscribe(channelName: 'chat-channel');
      await pusher.connect();
    } catch (e) {
      log("ERROR: $e");
    }
  }

  void onError(String message, int code, dynamic e) {
    log("onError: $message code: $code exception: $e");
  }

  void onEvent(PusherEvent event) {
    log("onEvent: Inbox ${event.eventName}");
    if (event.eventName == 'chat-event') {
      Provider.of<ChatProvider>(context, listen: false).initChatInfo(context);
      var json = jsonDecode(event.data);
      ChatModel chatModel = ChatModel.fromJson(json['message']);

      if (chatModel.userId ==
          Provider.of<ProfileProvider>(context, listen: false)
              .userInfoModel
              .id) {
        int shopId = Provider.of<ChatProvider>(context, listen: false).shopId;
        if (shopId != null && shopId == chatModel.shopId) {
          if (chatModel.sentBySeller == 1) {
            Provider.of<ChatProvider>(context, listen: false)
                .addChat(chatModel);
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isGuestMode =
        !Provider.of<AuthProvider>(context, listen: false).isLoggedIn();
    if (isFirstTime) {
      if (!isGuestMode) {
        Provider.of<ChatProvider>(context, listen: false).initChatInfo(context);
      }
      setPusher();
      isFirstTime = false;
    }
    Provider.of<ChatProvider>(context, listen: false).setShopID(0);
    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        elevation: 0,
        title: Text(
          getTranslated('inbox', context),
        ),
      ),
      body: Column(children: [
        // AppBar
        Visibility(
          visible: false,
          child: Stack(children: [
            // ClipRRect(
            //   borderRadius: BorderRadius.only(
            //       bottomLeft: Radius.circular(5),
            //       bottomRight: Radius.circular(5)),
            //   child: Image.asset(
            //     Images.toolbar_background,
            //     fit: BoxFit.fill,
            //     height: 90,
            //     width: double.infinity,
            //     color: Provider.of<ThemeProvider>(context).darkTheme
            //         ? Colors.black
            //         : null,
            //   ),
            // ),
            Container(
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top,
              ),
              height: 90 - MediaQuery.of(context).padding.top,
              alignment: Alignment.center,
              child: Row(children: [
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Image.asset(Images.logo_with_name_image,
                      height: App.height(context) * 5),
                ),
                widget.isBackButtonExist
                    ? IconButton(
                        icon: Icon(Icons.arrow_back_ios,
                            size: 20, color: ColorResources.WHITE),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    : SizedBox.shrink(),
                SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                Expanded(
                  child: Provider.of<ChatProvider>(context).isSearching
                      ? TextField(
                          autofocus: true,
                          decoration: InputDecoration(
                            hintText: 'Search...',
                            border: InputBorder.none,
                            hintStyle: titilliumRegular.copyWith(
                                color: ColorResources.getTextTitle(context)),
                          ),
                          style: titilliumSemiBold.copyWith(
                              color: ColorResources.WHITE,
                              fontSize: Dimensions.FONT_SIZE_LARGE),
                          onChanged: (String query) {
                            Provider.of<ChatProvider>(context, listen: false)
                                .filterList(query);
                          },
                        )
                      : Text(
                          getTranslated('inbox', context),
                          style: titilliumRegular.copyWith(
                              fontSize: 20,
                              color: ColorResources.getTextTitle(context)),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                ),
                IconButton(
                  icon: Icon(
                    Provider.of<ChatProvider>(context).isSearching
                        ? Icons.close
                        : Icons.search,
                    size: Dimensions.ICON_SIZE_LARGE,
                    color: ColorResources.getTextTitle(context),
                  ),
                  onPressed: () =>
                      Provider.of<ChatProvider>(context, listen: false)
                          .toggleSearch(),
                ),
              ]),
            ),
          ]),
        ),

        Expanded(
          child: isGuestMode
              ? NotLoggedInWidget()
              : RefreshIndicator(
                  backgroundColor: ColorResources.getSecondary(context),
                  color: Colors.white,
                  onRefresh: () async {
                    await Provider.of<ChatProvider>(context, listen: false)
                        .initChatInfo(context);
                  },
                  child: Consumer<ChatProvider>(
                    builder: (context, chat, child) {
                      return chat.chatInfoModel != null
                          ? chat.uniqueShopList.length != 0
                              ? ListView.builder(
                                  //physics: BouncingScrollPhysics(),
                                  itemCount: chat.uniqueShopList.length,
                                  padding: EdgeInsets.zero,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: [
                                        ListTile(
                                            leading: ClipOval(
                                              child: Container(
                                                color: Theme.of(context)
                                                    .highlightColor,
                                                child: CachedNetworkImage(
                                                  fit: BoxFit.cover,
                                                  height: 50,
                                                  width: 50,
                                                  imageUrl:
                                                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}'
                                                      '/${chat.uniqueShopList[index].sellerInfo != null ? chat.uniqueShopList[index].shop.image : ''}',
                                                  placeholder: (c, o) =>
                                                      Image.asset(
                                                          Images.placeholder,
                                                          fit: BoxFit.cover,
                                                          height: 50,
                                                          width: 50),
                                                ),
                                              ),
                                            ),
                                            title: Text(
                                              chat.uniqueShopList[index]
                                                          .sellerInfo !=
                                                      null
                                                  ? chat.uniqueShopList[index]
                                                          .shop.name ??
                                                      ''
                                                  : '',
                                              style: titilliumSemiBold.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_EXTRA_SMALL_IPAD,
                                              ),
                                            ),
                                            subtitle: Text(
                                                chat.uniqueShopList[index]
                                                    .message,
                                                style: titilliumRegular.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_EXTRA_SMALL
                                                        : Dimensions
                                                            .FONT_SIZE_EXTRA_SMALL_IPAD)),
                                            trailing: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                      DateConverter
                                                          .isoStringToLocalDateAndTime(
                                                              context,
                                                              chat
                                                                  .uniqueShopList[
                                                                      index]
                                                                  .createdAt),
                                                      style: titilliumRegular
                                                          .copyWith(
                                                        fontSize: Responsive
                                                                .isMobile(
                                                                    context)
                                                            ? Dimensions
                                                                .FONT_SIZE_DEFAULT
                                                            : Dimensions
                                                                .FONT_SIZE_EXTRA_SMALL_IPAD,
                                                      )),
                                                  chat.chatInfoModel.lastChat
                                                              .sellerId ==
                                                          chat
                                                              .uniqueShopList[
                                                                  index]
                                                              .sellerId
                                                      ? Visibility(
                                                          visible: false,
                                                          child: Container(
                                                            height: 20,
                                                            width: 20,
                                                            margin: EdgeInsets.only(
                                                                top: Dimensions
                                                                    .PADDING_SIZE_SMALL),
                                                            alignment: Alignment
                                                                .center,
                                                            decoration:
                                                                BoxDecoration(
                                                              shape: BoxShape
                                                                  .circle,
                                                              color: Theme.of(
                                                                      context)
                                                                  .primaryColor,
                                                            ),
                                                            child: Text('1',
                                                                style: titilliumSemiBold.copyWith(
                                                                    fontSize:
                                                                        Dimensions
                                                                            .FONT_SIZE_EXTRA_SMALL,
                                                                    color: ColorResources
                                                                        .WHITE)),
                                                          ),
                                                        )
                                                      : SizedBox.shrink(),
                                                ]),
                                            onTap: () {
                                              ShopModel shopModel = ShopModel(
                                                id: chat.uniqueShopList[index]
                                                    .shop.id,
                                                name: chat.uniqueShopList[index]
                                                    .shop.name,
                                                image: chat
                                                    .uniqueShopList[index]
                                                    .shop
                                                    .image,
                                              );

                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                      builder: (_) {
                                                return ShopChatScreen(
                                                    shopModel: shopModel);
                                              }));
                                            }),
                                        Divider(
                                            height: 2,
                                            color:
                                                ColorResources.CHAT_ICON_COLOR),
                                      ],
                                    );
                                  },
                                )
                              : NoInternetOrDataScreen(isNoInternet: false)
                          : InboxShimmer();
                    },
                  ),
                ),
        ),
      ]),
    );
  }
}

class InboxShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 15,
      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      itemBuilder: (context, index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled: Provider.of<ChatProvider>(context).uniqueShopList == null,
          child: Padding(
            padding: EdgeInsets.only(bottom: Dimensions.PADDING_SIZE_SMALL),
            child: Row(children: [
              CircleAvatar(child: Icon(Icons.person), radius: 30),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Dimensions.PADDING_SIZE_SMALL),
                  child: Column(children: [
                    Container(height: 15, color: ColorResources.WHITE),
                    SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                    Container(height: 15, color: ColorResources.WHITE),
                  ]),
                ),
              ),
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Container(height: 10, width: 30, color: ColorResources.WHITE),
                SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                Container(
                  height: 15,
                  width: 15,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Theme.of(context).primaryColor),
                ),
              ])
            ]),
          ),
        );
      },
    );
  }
}
