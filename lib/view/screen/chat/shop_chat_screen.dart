import 'dart:convert';
import 'dart:developer';
import 'package:flutter_sixvalley_ecommerce/data/model/response/config_model.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/shimmer_loading.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/splash/splash_screen.dart';
// ignore: unused_import
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/body/MessageBody.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/chat_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';

import 'package:flutter_sixvalley_ecommerce/provider/chat_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/basewidget/custom_app_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/widget/message_bubble.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/all_top_seller_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:pusher_channels_flutter/pusher_channels_flutter.dart';
import 'package:shimmer/shimmer.dart';

// ignore: must_be_immutable
class ShopChatScreen extends StatefulWidget {
  final ShopModel shopModel;
  final bool fromNotify;
  ShopChatScreen({@required this.shopModel, this.fromNotify = false});

  @override
  State<ShopChatScreen> createState() => _ShopChatScreenState();
}

class _ShopChatScreenState extends State<ShopChatScreen> {
  final ImagePicker picker = ImagePicker();
  final TextEditingController _controller = TextEditingController();
  String message = '';
  ConfigModel configModel;

  bool isFirstTime = true;
  PusherChannelsFlutter pusher = PusherChannelsFlutter.getInstance();
  setPusher() async {
    configModel =
        Provider.of<SplashProvider>(context, listen: false).configModel;
    await Provider.of<ProfileProvider>(context, listen: false)
        .getUserInfo(context);

    try {
      await pusher.init(
        apiKey: (configModel?.pusher?.key) ?? AppConstants.PUSHER_KEY,
        cluster: (configModel?.pusher?.cluster) ?? AppConstants.PUSHER_CLUSTER,
        onError: onError,
        onEvent: onEvent,
      );

      await pusher.subscribe(channelName: 'chat-channel');
      await pusher.connect();
    } catch (e) {
      log("ERROR: $e");
    }
  }

  void onError(String message, int code, dynamic e) {
    log("onError: $message code: $code exception: $e");
  }

  void onEvent(PusherEvent event) {
    log("onEvent: Inbox ${event.eventName}");
    if (event.eventName == 'chat-event') {
      Provider.of<ChatProvider>(context, listen: false).initChatInfo(context);
      var json = jsonDecode(event.data);
      ChatModel chatModel = ChatModel.fromJson(json['message']);

      print('chatModel.userId ${chatModel.userId}');
      print(
          'userInfoModel.userId ${Provider.of<ProfileProvider>(context, listen: false).userInfoModel.id}');
      if (chatModel.userId ==
          Provider.of<ProfileProvider>(context, listen: false)
              .userInfoModel
              .id) {
        int shopId = widget.shopModel.id;

        // ignore: unnecessary_brace_in_string_interps
        print('widget.shopId ${shopId}');
        print('chatModel.shopId ${chatModel.shopId}');
        if (shopId != null && shopId == chatModel.shopId) {
          Provider.of<ChatProvider>(context, listen: false).addChat(chatModel);
        }
      }
    }
  }

  _load(BuildContext context) async {
    await Provider.of<SplashProvider>(context, listen: false)
        .initConfig(context);
  }

  @override
  Widget build(BuildContext context) {
    _load(context);
    if (isFirstTime) {
      Provider.of<ChatProvider>(context, listen: false)
          .initChatList(widget.shopModel.id, context);
      setPusher();
      isFirstTime = false;
    }
    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      appBar: AppBar(
        leading: widget.fromNotify
            ? BackButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SplashScreen(),
                    ),
                  );
                },
              )
            : BackButton(),
        backgroundColor: ColorResources.getSecondary(context),
        title: Row(children: [
          SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
          Expanded(
            child: Text(
              widget.shopModel.name,
              style: titilliumRegular.copyWith(
                fontSize: 20,
                color: ColorResources.WHITE,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
              Dimensions.PADDING_SIZE_EXTRA_LARGE,
            )),
            clipBehavior: Clip.antiAlias,
            child: CachedNetworkImage(
              imageUrl:
                  '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${widget.shopModel.image}',
              placeholder: (c, o) => Image.asset(
                Images.placeholder,
                fit: BoxFit.cover,
              ),
              height: 50,
              width: 50,
              fit: BoxFit.cover,
            ),
          ),
        ]),
      ),
      body: configModel != null
          ? Column(children: [
              // Chats
              Expanded(
                  child: Provider.of<ChatProvider>(context).chatList != null
                      ? Provider.of<ChatProvider>(context).chatList.length != 0
                          ? ListView.builder(
                              physics: BouncingScrollPhysics(),
                              padding:
                                  EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                              itemCount: Provider.of<ChatProvider>(context)
                                  .chatList
                                  .length,
                              reverse: true,
                              itemBuilder: (context, index) {
                                List<ChatModel> chats =
                                    Provider.of<ChatProvider>(context)
                                        .chatList
                                        .reversed
                                        .toList();
                                return MessageBubble(
                                    chat: chats[index],
                                    sellerImage: widget.shopModel.image,
                                    onProfileTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) =>
                                                  AllTopSellerScreen(
                                                      topSeller:
                                                          widget.shopModel)));
                                    });
                              },
                            )
                          : SizedBox.shrink()
                      : ChatShimmer()),

              // Bottom TextField
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /*Provider.of<ChatProvider>(context).imageFile != null ? Padding(
              padding: EdgeInsets.only(left: Dimensions.PADDING_SIZE_DEFAULT),
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Image.file(Provider.of<ChatProvider>(context).imageFile, height: 70, width: 70, fit: BoxFit.cover),
                  Positioned(
                    top: -2, right: -2,
                    child: InkWell(
                      onTap: () => Provider.of<ChatProvider>(context, listen: false).removeImage(_controller.text),
                      child: Icon(Icons.cancel, color: ColorResources.WHITE),
                    ),
                  ),
                ],
              ),
            ) : SizedBox.shrink(),*/

                  SizedBox(
                    height: Responsive.isMobile(context)
                        ? 70
                        : (message.length > 80)
                            ? 180
                            : 90,
                    child: Card(
                      color: Theme.of(context).highlightColor,
                      shadowColor: Colors.grey[200],
                      elevation: 2,
                      margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          Dimensions.PADDING_SIZE_SMALL,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: Dimensions.PADDING_SIZE_SMALL),
                        child: Row(children: [
                          Expanded(
                            child: TextField(
                              controller: _controller,
                              style: titilliumRegular.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              ),
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              expands: true,
                              decoration: InputDecoration(
                                hintText: getTranslated('type_here', context),
                                hintStyle: titilliumRegular.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_SMALL
                                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                                    color: ColorResources.HINT_TEXT_COLOR),
                                border: InputBorder.none,
                              ),
                              onChanged: (String newText) {
                                if (newText.isNotEmpty &&
                                    !Provider.of<ChatProvider>(context,
                                            listen: false)
                                        .isSendButtonActive) {
                                  Provider.of<ChatProvider>(context,
                                          listen: false)
                                      .toggleSendButtonActivity();
                                } else if (newText.isEmpty &&
                                    Provider.of<ChatProvider>(context,
                                            listen: false)
                                        .isSendButtonActive) {
                                  Provider.of<ChatProvider>(context,
                                          listen: false)
                                      .toggleSendButtonActivity();
                                }
                                message = newText;
                                setState(() {});
                              },
                            ),
                          ),
                          /*InkWell(
                      onTap: () async {
                        final PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
                        if (pickedFile != null) {
                          Provider.of<ChatProvider>(context, listen: false).setImage(File(pickedFile.path));
                        } else {
                          print('No image selected.');
                        }
                      },
                      child: Icon(Icons.image, color: ColorResources.HINT_TEXT_COLOR, size: Dimensions.ICON_SIZE_DEFAULT),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL, horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                      child: VerticalDivider(width: 2, color: ColorResources.CHAT_ICON_COLOR),
                    ),*/
                          InkWell(
                            onTap: () {
                              if (Provider.of<ChatProvider>(context,
                                      listen: false)
                                  .isSendButtonActive) {
                                MessageBody messageBody = MessageBody(
                                    shopId: widget.shopModel.id.toString(),
                                    message: _controller.text);
                                Provider.of<ChatProvider>(context,
                                        listen: false)
                                    .sendMessage(messageBody, context);
                                _controller.text = '';
                              }
                            },
                            child: Icon(
                              Icons.send,
                              color: Provider.of<ChatProvider>(context)
                                      .isSendButtonActive
                                  ? Theme.of(context).primaryColor
                                  : ColorResources.HINT_TEXT_COLOR,
                              size: Dimensions.ICON_SIZE_DEFAULT,
                            ),
                          ),
                        ]),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: Dimensions.PADDING_SIZE_DEFAULT,
                  ),
                ],
              ),
            ])
          : LoadingPage(),
    );
  }
}

class ChatShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 20,
      shrinkWrap: true,
      reverse: true,
      itemBuilder: (context, index) {
        bool isMe = index % 2 == 0;
        return Shimmer.fromColors(
          baseColor: isMe ? Colors.grey[300] : ColorResources.IMAGE_BG,
          highlightColor: isMe
              ? Colors.grey[100]
              : ColorResources.IMAGE_BG.withOpacity(0.9),
          enabled: Provider.of<ChatProvider>(context).chatList == null,
          child: Row(
            mainAxisAlignment:
                isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
            children: [
              isMe
                  ? SizedBox.shrink()
                  : InkWell(child: CircleAvatar(child: Icon(Icons.person))),
              Expanded(
                child: Container(
                  margin: isMe
                      ? EdgeInsets.fromLTRB(50, 5, 10, 5)
                      : EdgeInsets.fromLTRB(10, 5, 50, 5),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft:
                            isMe ? Radius.circular(10) : Radius.circular(0),
                        bottomRight:
                            isMe ? Radius.circular(0) : Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      color: isMe
                          ? ColorResources.IMAGE_BG
                          : ColorResources.WHITE),
                  child: Container(height: 20),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
