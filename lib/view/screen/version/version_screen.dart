import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:provider/provider.dart';

class UpdateVersion extends StatelessWidget {
  const UpdateVersion({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
          child: Column(
            children: [
              Container(
                child: Image.asset(
                  Images.splash_logo,
                  fit: BoxFit.fill,
                ),
              ),
              Text(
                getTranslated('New version available', context),
                style: titilliumBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_OVER_LARGE
                      : Dimensions.FONT_SIZE_OVER_LARGE_IPAD,
                ),
              ),
              SizedBox(
                height: Dimensions.PADDING_SIZE_SMALL,
              ),
              Text(
                getTranslated('Please_update', context),
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_LARGE
                      : Dimensions.FONT_SIZE_LARGE_IPAD,
                ),
              ),
              SizedBox(
                height: Dimensions.PADDING_SIZE_DEFAULT,
              ),
              CustomButton(
                buttonText: getTranslated('UPDATE_APP', context),
                onTap: () {
                  if (Platform.isAndroid) {
                    App.launchUrl(
                        Provider.of<SplashProvider>(context, listen: false)
                            .configModel
                            .androidUrl);
                  } else if (Platform.isIOS) {
                    App.launchUrl(
                        Provider.of<SplashProvider>(context, listen: false)
                            .configModel
                            .iosUrl);
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
