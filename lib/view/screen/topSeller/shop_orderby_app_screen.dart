import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/category.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';

import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/rating_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_category_product_screen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class ShopOrderByAppScreen extends StatefulWidget {
  ShopModel shopModel;
  ShopOrderByAppScreen({@required this.shopModel});

  @override
  State<ShopOrderByAppScreen> createState() => _ShopOrderByAppScreenState();
}

class _ShopOrderByAppScreenState extends State<ShopOrderByAppScreen> {
  ShopModel shopModel;
  @override
  void initState() {
    Provider.of<ShopsProvider>(context, listen: false)
        .setShop(widget.shopModel);
    Provider.of<ShopsProvider>(context, listen: false).getShop(context);
    Provider.of<ProductProvider>(context, listen: false).initSellerProductList(
        widget.shopModel.seller.id.toString(), 1, context,
        reload: true);
    Provider.of<SellerProvider>(context, listen: false)
        .initSeller(widget.shopModel.seller.id.toString(), context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    shopModel = Provider.of<ShopsProvider>(context, listen: true).shop;
    //ScrollController _scrollController = ScrollController();
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(500),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 5)
            ],
          ),
          child: CircleAvatar(
            backgroundColor: Color.fromARGB(255, 233, 234, 255),
            child: BackButton(
              color: ColorResources.getSecondary(context),
            ),
          ),
        ),
      ),
      body: RefreshIndicator(
        color: Colors.white,
        backgroundColor: ColorResources.getSecondary(context),
        onRefresh: () async {
          await Provider.of<ShopsProvider>(context, listen: false)
              .getShop(context);
          Provider.of<ProductProvider>(context, listen: false)
              .initSellerProductList(shopModel.seller.id.toString(), 1, context,
                  reload: true);
          shopModel = Provider.of<ShopsProvider>(context, listen: false).shop;
          setState(() {});
        },
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: SingleChildScrollView(
                child: Column(children: [
                  Container(
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 5,
                        )
                      ],
                    ),
                    child: Stack(
                      children: [
                        Container(
                          height: Responsive.isMobile(context) ? 240 : 340,
                          width: double.infinity,
                          child: CachedNetworkImage(
                            height: Responsive.isMobile(context) ? 240 : 340,
                            fit: BoxFit.cover,
                            imageUrl:
                                '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/banner/${shopModel.banner != null ? shopModel.banner : ''}',
                            placeholder: (c, o) => Image.asset(
                              Images.placeholder,
                              height: Responsive.isMobile(context) ? 240 : 340,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: Responsive.isMobile(context) ? 200 : 300),
                          decoration: BoxDecoration(
                            color: Color(0xFFEEE6FE),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(
                                  Dimensions.PADDING_SIZE_EXTRA_LARGE),
                              topRight: Radius.circular(
                                  Dimensions.PADDING_SIZE_EXTRA_LARGE),
                            ),
                          ),
                          child: Container(
                            padding:
                                EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Seller Info
                                Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: Responsive.isMobile(context)
                                            ? 80
                                            : 180,
                                        width: Responsive.isMobile(context)
                                            ? 80
                                            : 180,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(500),
                                          color:
                                              Theme.of(context).highlightColor,
                                          border: Border.all(
                                            color: Colors.grey.withOpacity(.2),
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.2),
                                                spreadRadius: 1,
                                                blurRadius: 5)
                                          ],
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(500),
                                          child: CachedNetworkImage(
                                            fit: BoxFit.cover,
                                            imageUrl:
                                                '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${shopModel.image}',
                                            placeholder: (c, o) => Image.asset(
                                                Images.placeholder,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                          width: Dimensions.PADDING_SIZE_SMALL),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                (Responsive.isMobile(context)
                                                    ? 150
                                                    : 250),
                                            child: Text(
                                              shopModel.name,
                                              style: titilliumSemiBold.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions.FONT_SIZE_LARGE
                                                    : Dimensions
                                                        .FONT_SIZE_EXTRA_LARGE_IPAD,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: Dimensions
                                                .PADDING_SIZE_EXTRA_SMALL,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                (shopModel.avgReview ?? 0)
                                                    .toString(),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                  color: ColorResources
                                                      .getSecondary(context),
                                                ),
                                              ),
                                              SizedBox(
                                                width: Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL,
                                              ),
                                              RatingBar(
                                                  rating: shopModel.avgReview),
                                            ],
                                          ),
                                          SizedBox(
                                            height: Dimensions
                                                .PADDING_SIZE_EXTRA_SMALL,
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    (shopModel.totalOrder ?? 0)
                                                        .toString(),
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_DEFAULT
                                                          : Dimensions
                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                      color: ColorResources
                                                          .getSecondary(
                                                              context),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: Dimensions
                                                        .PADDING_SIZE_EXTRA_SMALL,
                                                  ),
                                                  Text(
                                                    getTranslated(
                                                        'orders', context),
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_DEFAULT
                                                          : Dimensions
                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                width: Dimensions
                                                    .PADDING_SIZE_SMALL,
                                              ),
                                              Text(
                                                '/',
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ),
                                              SizedBox(
                                                width: Dimensions
                                                    .PADDING_SIZE_SMALL,
                                              ),
                                              Text(
                                                (shopModel.totalReview ?? 0)
                                                    .toString(),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                  color: ColorResources
                                                      .getSecondary(context),
                                                ),
                                              ),
                                              SizedBox(
                                                width: Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL,
                                              ),
                                              Text(
                                                getTranslated(
                                                    'reviews', context),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: Dimensions
                                                .PADDING_SIZE_EXTRA_SMALL,
                                          ),
                                          Row(
                                            children: [
                                              Icon(
                                                Icons.shopping_cart,
                                                size: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions.ICON_SIZE_SMALL
                                                    : Dimensions
                                                        .ICON_SIZE_SMALL_IPAD,
                                                color:
                                                    ColorResources.getSecondary(
                                                        context),
                                              ),
                                              SizedBox(
                                                width: Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL,
                                              ),
                                              Text(
                                                getTranslated(
                                                    shopModel.seller
                                                        .productOrderType,
                                                    context),
                                                style:
                                                    titilliumRegular.copyWith(
                                                  color: Color.fromRGBO(
                                                      147, 105, 251, 1),
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ]),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 125,
                    child: Consumer<ShopsProvider>(
                        builder: (context, shopsProvider, child) {
                      return Column(mainAxisSize: MainAxisSize.max, children: [
                        shopsProvider.shopCategoryList.length != 0
                            ? Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        Dimensions.PADDING_SIZE_SMALL,
                                        10,
                                        Dimensions.PADDING_SIZE_SMALL,
                                        Dimensions.PADDING_SIZE_SMALL),
                                    child: TitleRow(
                                      title:
                                          getTranslated('CATEGORIES', context),
                                      onTap: () {},
                                      isDetailsPage: true,
                                    ),
                                  ),
                                  StaggeredGridView.countBuilder(
                                    itemCount:
                                        shopsProvider.shopCategoryList.length,
                                    crossAxisCount: 5,
                                    mainAxisSpacing:
                                        Dimensions.PADDING_SIZE_SMALL,
                                    crossAxisSpacing:
                                        Dimensions.PADDING_SIZE_SMALL,
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            Dimensions.PADDING_SIZE_SMALL),
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    staggeredTileBuilder: (int index) =>
                                        StaggeredTile.fit(1),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      Category categoryModel =
                                          shopsProvider.shopCategoryList[index];
                                      return InkWell(
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) =>
                                                  ShopCategoryProductScreen(
                                                shopModel: shopModel,
                                                category: categoryModel,
                                              ),
                                            ),
                                          );
                                        },
                                        child: Container(
                                          color: Color(0xFFEEE6FE),
                                          padding: EdgeInsets.all(Dimensions
                                              .PADDING_SIZE_EXTRA_SMALL),
                                          height: Responsive.isMobile(context)
                                              ? 70.0
                                              : 190.0,
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.stretch,
                                              children: [
                                                Expanded(
                                                  flex: 3,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Color(0xFFEEE6FE),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        Dimensions
                                                            .PADDING_SIZE_SMALL,
                                                      ),
                                                    ),
                                                    // padding: EdgeInsets.all(
                                                    //     Dimensions
                                                    //         .PADDING_SIZE_SMALL),
                                                    child: ClipRRect(
                                                      borderRadius: BorderRadius
                                                          .circular(Dimensions
                                                              .PADDING_SIZE_SMALL),
                                                      child: FadeInImage
                                                          .assetNetwork(
                                                        fit: BoxFit.fill,
                                                        placeholder:
                                                            Images.placeholder,
                                                        image:
                                                            '${Provider.of<SplashProvider>(context, listen: false).baseUrls.categoryImageUrl}'
                                                            '/${categoryModel.icon}',
                                                        imageErrorBuilder: (c,
                                                                o, s) =>
                                                            Image.asset(Images
                                                                .placeholder),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        10),
                                                                bottomRight:
                                                                    Radius
                                                                        .circular(
                                                                            10)),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                      child: Container(
                                                        child: Text(
                                                          categoryModel.name ??
                                                              getTranslated(
                                                                  'CATEGORY',
                                                                  context),
                                                          textAlign:
                                                              TextAlign.center,
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style:
                                                              titilliumRegular
                                                                  .copyWith(
                                                            fontSize: Responsive
                                                                    .isMobile(
                                                                        context)
                                                                ? Dimensions
                                                                    .FONT_SIZE_EXTRA_SMALL
                                                                : Dimensions
                                                                    .FONT_SIZE_EXTRA_SMALL_IPAD,
                                                          ),
                                                        ),
                                                      ),

                                                      // Text(
                                                      //   categoryProvider.categoryList.length != 0
                                                      //       ? categoryProvider.categoryList[index].name
                                                      //       : getTranslated('CATEGORY', context),
                                                      //   textAlign: TextAlign.center,
                                                      //   maxLines: 1,
                                                      //   overflow: TextOverflow.ellipsis,
                                                      //   style: titilliumSemiBold.copyWith(
                                                      //       fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL),
                                                      // ),
                                                    )),
                                              ]),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              )
                            : SizedBox.shrink(),
                      ]);
                    }),
                  ),
                  Consumer<ProductProvider>(
                      builder: (context, prodProvider, child) {
                    return Column(children: [
                      prodProvider.sellerProductList.length != 0
                          ? StaggeredGridView.countBuilder(
                              itemCount: prodProvider.sellerProductList.length,
                              crossAxisCount:
                                  Responsive.isMobile(context) ? 2 : 3,
                              mainAxisSpacing: Dimensions.PADDING_SIZE_DEFAULT,
                              crossAxisSpacing: Dimensions.PADDING_SIZE_DEFAULT,
                              addAutomaticKeepAlives: true,
                              padding: EdgeInsets.all(
                                  Dimensions.PADDING_SIZE_DEFAULT),
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              staggeredTileBuilder: (int index) =>
                                  StaggeredTile.fit(1),
                              itemBuilder: (BuildContext context, int index) {
                                Product productModel =
                                    prodProvider.sellerProductList[index];
                                return ProductWidget(
                                  productModel: productModel,
                                  fromShop: true,
                                );
                              },
                            )
                          : NoInternetOrDataScreen(isNoInternet: false),
                    ]);
                  }),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
