import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_orderby_app_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/wishlist/wishlist_screen.dart';
import 'package:provider/provider.dart';

class AllShopScreen extends StatelessWidget {
  AllShopScreen();
  @override
  Widget build(BuildContext context) {
    // final TextEditingController _controller = TextEditingController();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Provider.of<ThemeProvider>(context).darkTheme
            ? Colors.black
            : ColorResources.getSecondary(context),
        leading: IconButton(
          icon:
              Icon(Icons.arrow_back_ios, size: 20, color: ColorResources.WHITE),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Center(
          child: Text(getTranslated('all_shopping', context),
              style: titilliumRegular.copyWith(
                  fontSize: 20, color: ColorResources.WHITE)),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => WishListScreen(),
                ),
              );
            },
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Stack(clipBehavior: Clip.none, children: [
                Image.asset(
                  Images.heart,
                  height: Dimensions.ICON_SIZE_DEFAULT,
                  width: Dimensions.ICON_SIZE_DEFAULT,
                  color: ColorResources.getSecondary(context),
                ),
              ]),
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => CartScreen()));
            },
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Stack(clipBehavior: Clip.none, children: [
                Image.asset(
                  Images.cart_image,
                  height: Dimensions.ICON_SIZE_DEFAULT,
                  width: Dimensions.ICON_SIZE_DEFAULT,
                  color: ColorResources.getSecondary(context),
                ),
                Positioned(
                  top: -4,
                  right: -4,
                  child:
                      Consumer<CartProvider>(builder: (context, cart, child) {
                    return CircleAvatar(
                      radius: 7,
                      backgroundColor: Colors.black,
                      child: Text(cart.cartList.length.toString(),
                          style: titilliumSemiBold.copyWith(
                            color: ColorResources.WHITE,
                            fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                          )),
                    );
                  }),
                ),
              ]),
            ),
          ),
        ],
      ),
      body: RefreshIndicator(
        color: Colors.white,
        backgroundColor: ColorResources.getSecondary(context),
        onRefresh: () async {
          await Provider.of<ShopsProvider>(context, listen: false)
              .getShopsList(true, context);
        },
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                child: Column(children: [
                  Consumer<ShopsProvider>(
                      builder: (context, shopProvider, child) {
                    return Column(
                      children: List.generate(shopProvider.tempShopList.length,
                          (int index) {
                        ShopModel shopModel = shopProvider.tempShopList[index];
                        return InkWell(
                          onTap: () {
                            ShopModel shopModel = shopProvider.shopList[index];
                            if (shopModel.seller.productOrderType ==
                                AppConstants.DIRECT_TO_SHOP) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => ShopScreen(
                                      shopModel: shopProvider.shopList[index]),
                                ),
                              );
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => ShopOrderByAppScreen(
                                      shopModel: shopProvider.shopList[index]),
                                ),
                              );
                            }
                          },
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(.1)),
                                  borderRadius: BorderRadius.circular(
                                    Dimensions.PADDING_SIZE_SMALL,
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 1,
                                      blurRadius: 5,
                                    )
                                  ],
                                ),
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(
                                          Dimensions.PADDING_SIZE_SMALL,
                                        ),
                                        bottomLeft: Radius.circular(
                                          Dimensions.PADDING_SIZE_SMALL,
                                        ),
                                      ),
                                      child: CachedNetworkImage(
                                        height: Responsive.isMobile(context)
                                            ? 100
                                            : 150,
                                        width: Responsive.isMobile(context)
                                            ? 100
                                            : 150,
                                        fit: BoxFit.cover,
                                        imageUrl:
                                            '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${shopModel.image}',
                                        placeholder: (c, o) => Image.asset(
                                          Images.placeholder,
                                          height: Responsive.isMobile(context)
                                              ? 100
                                              : 150,
                                          width: Responsive.isMobile(context)
                                              ? 100
                                              : 150,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: Dimensions.PADDING_SIZE_SMALL,
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width -
                                          (Responsive.isMobile(context)
                                              ? 140
                                              : 240),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            shopModel.name,
                                            overflow: TextOverflow.ellipsis,
                                            style: titilliumBold.copyWith(
                                              fontSize:
                                                  Responsive.isMobile(context)
                                                      ? Dimensions
                                                          .FONT_SIZE_LARGE
                                                      : Dimensions
                                                          .FONT_SIZE_LARGE_IPAD,
                                            ),
                                          ),
                                          Visibility(
                                            visible: shopModel
                                                    .seller.productOrderType ==
                                                AppConstants.DIRECT_TO_SHOP,
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Icon(
                                                  Icons.location_pin,
                                                  color: ColorResources
                                                      .getSecondary(context),
                                                ),
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width -
                                                      (Responsive.isMobile(
                                                              context)
                                                          ? 170
                                                          : 270),
                                                  child: Text(
                                                    shopModel.address,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: titilliumRegular
                                                        .copyWith(
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_DEFAULT
                                                          : Dimensions
                                                              .FONT_SIZE_DEFAULT_IPAD,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Text(
                                            getTranslated(
                                                shopModel
                                                    .seller.productOrderType,
                                                context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                              color:
                                                  ColorResources.getSecondary(
                                                      context),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: Dimensions.PADDING_SIZE_SMALL,
                              ),
                            ],
                          ),
                        );
                      }),
                    );
                  }),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
