import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/wishlist/wishlist_screen.dart';
import 'package:provider/provider.dart';

class AllTopSellerScreen extends StatelessWidget {
  final ShopModel topSeller;

  AllTopSellerScreen({@required this.topSeller});
  @override
  Widget build(BuildContext context) {
    final TextEditingController _controller = TextEditingController();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Provider.of<ThemeProvider>(context).darkTheme
            ? Colors.black
            : ColorResources.getSecondary(context),
        leading: IconButton(
          icon:
              Icon(Icons.arrow_back_ios, size: 20, color: ColorResources.WHITE),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Center(
          child: Text(getTranslated('all_shopping', context),
              style: titilliumRegular.copyWith(
                  fontSize: 20, color: ColorResources.WHITE)),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => WishListScreen(),
                ),
              );
            },
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Stack(clipBehavior: Clip.none, children: [
                Image.asset(
                  Images.wish_image,
                  height: Dimensions.ICON_SIZE_DEFAULT,
                  width: Dimensions.ICON_SIZE_DEFAULT,
                  color: ColorResources.getPrimary(context),
                ),
              ]),
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => CartScreen()));
            },
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Stack(clipBehavior: Clip.none, children: [
                Image.asset(
                  Images.cart_arrow_down_image,
                  height: Dimensions.ICON_SIZE_DEFAULT,
                  width: Dimensions.ICON_SIZE_DEFAULT,
                  color: ColorResources.getPrimary(context),
                ),
                Positioned(
                  top: -4,
                  right: -4,
                  child:
                      Consumer<CartProvider>(builder: (context, cart, child) {
                    return CircleAvatar(
                      radius: 7,
                      backgroundColor: Colors.black,
                      child: Text(cart.cartList.length.toString(),
                          style: titilliumSemiBold.copyWith(
                            color: ColorResources.WHITE,
                            fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                          )),
                    );
                  }),
                ),
              ]),
            ),
          ),
        ],
      ),
      body: RefreshIndicator(
        color: Colors.white,
        backgroundColor: ColorResources.getSecondary(context),
        onRefresh: () async {
          await Provider.of<ShopsProvider>(context, listen: false)
              .getShopsList(true, context);
        },
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                child: Column(children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                        vertical: Dimensions.PADDING_SIZE_DEFAULT),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey.withOpacity(.5),
                                ),
                                color: ColorResources.getTextBg(context),
                                borderRadius: BorderRadius.circular(8.0)),
                            child: TextFormField(
                              controller: _controller,
                              onFieldSubmitted: (String query) async {
                                 Provider.of<ShopsProvider>(context,
                                        listen: false)
                                    .filterData(query);
                              },
                              onChanged: (String query) {},
                              textInputAction: TextInputAction.search,
                              maxLines: 1,
                              textAlignVertical: TextAlignVertical.center,
                              decoration: InputDecoration(
                                hintText: '',
                                isDense: true,
                                hintStyle:
                                    robotoRegular.copyWith(color: Colors.black),
                                border: InputBorder.none,
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: Colors.black,
                                  size: Dimensions.ICON_SIZE_DEFAULT,
                                ),
                                contentPadding: EdgeInsets.all(
                                  Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                ),
                                suffixIcon: _controller.text.isNotEmpty
                                    ? IconButton(
                                        icon: Icon(Icons.clear,
                                            color: Colors.black),
                                        onPressed: () {},
                                      )
                                    : null,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Consumer<ShopsProvider>(
                      builder: (context, shopProvider, child) {
                    return Column(
                      children: List.generate(shopProvider.tempShopList.length,
                          (int index) {
                        ShopModel shopModel = shopProvider.tempShopList[index];
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => ShopScreen(
                                  shopModel: shopModel,
                                ),
                              ),
                            );
                          },
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(.1)),
                                  borderRadius: BorderRadius.circular(
                                    Dimensions.PADDING_SIZE_SMALL,
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 1,
                                      blurRadius: 5,
                                    )
                                  ],
                                ),
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(
                                          Dimensions.PADDING_SIZE_SMALL,
                                        ),
                                        bottomLeft: Radius.circular(
                                          Dimensions.PADDING_SIZE_SMALL,
                                        ),
                                      ),
                                      child: CachedNetworkImage(
                                        height: 100,
                                        width: 100,
                                        fit: BoxFit.cover,
                                        imageUrl:
                                            '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${shopModel.image}',
                                        placeholder: (c, o) =>
                                            Image.asset(
                                          Images.placeholder,
                                          height: 100,
                                          width: 100,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: Dimensions.PADDING_SIZE_SMALL,
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width -
                                          140,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            shopModel.name,
                                            overflow: TextOverflow.ellipsis,
                                            style: titilliumBold.copyWith(
                                              fontSize:
                                                  Dimensions.FONT_SIZE_LARGE,
                                            ),
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                Icons.location_pin,
                                                color:
                                                    ColorResources.getSecondary(
                                                        context),
                                              ),
                                              SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    170,
                                                child: Text(
                                                  shopModel.address,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style:
                                                      titilliumRegular.copyWith(
                                                    fontSize: Dimensions
                                                        .FONT_SIZE_DEFAULT,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            getTranslated(
                                                shopModel
                                                    .seller.productOrderType,
                                                context),
                                            style: titilliumRegular.copyWith(
                                              fontSize:
                                                  Dimensions.FONT_SIZE_DEFAULT,
                                              color:
                                                  ColorResources.getSecondary(
                                                      context),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: Dimensions.PADDING_SIZE_SMALL,
                              ),
                            ],
                          ),
                        );
                      }),
                    );
                  }),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
