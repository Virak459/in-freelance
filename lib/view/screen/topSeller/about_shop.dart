import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class AboutShopScreen extends StatelessWidget {
  final String about;
  const AboutShopScreen({Key key, this.about}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('About Shop', context)),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
          child: Text(about,
              style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_LARGE
                    : Dimensions.FONT_SIZE_SMALL_IPAD,
              )),
        ),
      ),
    );
  }
}
