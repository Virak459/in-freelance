import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';

import 'package:flutter_sixvalley_ecommerce/helper/product_type.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/products_view.dart';
import 'package:provider/provider.dart';

class TopSellerProductScreen1 extends StatelessWidget {
  final ShopModel shopModel;
  TopSellerProductScreen1({@required this.shopModel});

  @override
  Widget build(BuildContext context) {
    print(
        '====Top seller Name=====>${shopModel.name} ========id =====>${shopModel.id}');
    // Provider.of<ProductProvider>(context, listen: false).removeFirstLoading();
    //Provider.of<ProductProvider>(context, listen: false).clearSellerData();
    Provider.of<ProductProvider>(context, listen: false).initSellerProductList(
        shopModel.seller.id.toString(), 1, context,
        reload: true);
    ScrollController _scrollController = ScrollController();

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: 240,
                width: double.infinity,
                child: CachedNetworkImage(
                  height: 240,
                  fit: BoxFit.cover,
                  imageUrl:
                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/banner/${shopModel.banner != null ? shopModel.banner : ''}',
                  placeholder: (c, o) => Image.asset(
                    Images.placeholder,
                    height: 240,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                top: 50,
                left: 20,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.white,
                  ),
                  child: BackButton(
                    color: ColorResources.getSecondary(context),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 200),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Dimensions.PADDING_SIZE_DEFAULT),
                    topRight: Radius.circular(Dimensions.PADDING_SIZE_DEFAULT),
                  ),
                ),
                child: Container(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Seller Info
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 80,
                              width: 80,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border.all(
                                    color: Colors.grey.withOpacity(.5),
                                  )),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  imageUrl:
                                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.shopImageUrl}/${shopModel.image}',
                                  placeholder: (c, o) => Image.asset(
                                      Images.placeholder,
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                            SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  shopModel.name,
                                  style: titilliumSemiBold.copyWith(
                                    fontSize: Dimensions.FONT_SIZE_LARGE,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.star,
                                      size: 18,
                                      color: ColorResources.getYellow(context),
                                    ),
                                    Text(
                                      '4.4',
                                      style: titilliumRegular.copyWith(
                                        color: ColorResources.getSecondary(
                                            context),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.timer_sharp,
                                      size: 18,
                                      color:
                                          ColorResources.getSecondary(context),
                                    ),
                                    SizedBox(
                                      width:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                    ),
                                    Text(
                                      'Open',
                                      style: titilliumRegular.copyWith(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.delivery_dining,
                                      size: 18,
                                      color:
                                          ColorResources.getSecondary(context),
                                    ),
                                    SizedBox(
                                      width:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                    ),
                                    Text(
                                      '12,2 Km (delivery)',
                                      style: titilliumRegular.copyWith(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.shopping_cart,
                                      size: 18,
                                      color:
                                          ColorResources.getSecondary(context),
                                    ),
                                    SizedBox(
                                      width:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                    ),
                                    Text(
                                      getTranslated(
                                          shopModel.seller.productOrderType,
                                          context),
                                      style: titilliumRegular.copyWith(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ]),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
          Divider(),
          Expanded(
            child: ListView(
              controller: _scrollController,
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.all(0),
              children: [
                SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                Padding(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  child: ProductView(
                      isHomePage: false,
                      productType: ProductType.SELLER_PRODUCT,
                      scrollController: _scrollController,
                      sellerId: shopModel.seller.id.toString()),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
