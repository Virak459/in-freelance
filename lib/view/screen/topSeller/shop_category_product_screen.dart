import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/category.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/shops_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class ShopCategoryProductScreen extends StatelessWidget {
  final ShopModel shopModel;
  final Category category;
  const ShopCategoryProductScreen({
    Key key,
    @required this.category,
    @required this.shopModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<ShopsProvider>(context, listen: false)
        .getShopCategoryProductList(
            category.id.toString(), shopModel.seller.id.toString(), context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(
          category.name,
          style: titilliumBold.copyWith(
            fontSize: Dimensions.FONT_SIZE_LARGE,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              color: Colors.white,
              backgroundColor: ColorResources.getSecondary(context),
              onRefresh: () async {
                await Provider.of<ShopsProvider>(context, listen: false)
                    .getShopCategoryProductList(category.id.toString(),
                        shopModel.seller.id.toString(), context);
              },
              child: Container(
                child: Consumer<ShopsProvider>(
                    builder: (context, shopsProvider, child) {
                  return ListView.builder(
                    itemCount: 1,
                    itemBuilder: (context, index) => Column(children: [
                      shopsProvider.shopCategoryProductList.length != 0
                          ? Container(
                              child: StaggeredGridView.countBuilder(
                                itemCount: shopsProvider
                                    .shopCategoryProductList.length,
                                crossAxisCount:
                                    Responsive.isMobile(context) ? 2 : 3,
                                mainAxisSpacing:
                                    Dimensions.PADDING_SIZE_DEFAULT,
                                crossAxisSpacing:
                                    Dimensions.PADDING_SIZE_DEFAULT,
                                addAutomaticKeepAlives: true,
                                padding: EdgeInsets.all(
                                    Dimensions.PADDING_SIZE_DEFAULT),
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                staggeredTileBuilder: (int index) =>
                                    StaggeredTile.fit(1),
                                itemBuilder: (BuildContext context, int index) {
                                  Product productModel = shopsProvider
                                      .shopCategoryProductList[index];
                                  return ProductWidget(
                                    productModel: productModel,
                                    fromShop: true,
                                  );
                                },
                              ),
                            )
                          : NoInternetOrDataScreen(isNoInternet: false),
                    ]),
                  );
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
