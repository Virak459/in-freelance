import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/bank_info_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/coupon_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/order_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/amount_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/custom_loader.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/my_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/checkout/widget/custom_check_box2.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/order_details_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/review_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class PaymentScreen extends StatefulWidget {
  final String orderId;
  final String addressID;
  final String customerID;
  final String couponCode;
  final double totalOrderAmount;
  final double totalPrice;
  final double shippingFee;
  final double discount;
  final double tax;
  PaymentScreen({
    @required this.orderId,
    @required this.addressID,
    @required this.customerID,
    @required this.couponCode,
    this.shippingFee,
    this.tax,
    this.discount,
    this.totalOrderAmount,
    this.totalPrice,
  });

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  String selectedUrl;
  double value = 0.0;
  // ignore: unused_field
  bool _isLoading = true;
  // ignore: unused_field
  List<bool> _isChecked;
  List<File> _files = [File(''), File(''), File('')];
  ImagePicker imagePicker = ImagePicker();
  String paymentImage;
  @override
  void initState() {
    super.initState();
    Provider.of<CartProvider>(context, listen: false).getPaymentInfor(context);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CartProvider>(builder: (context, bank, child) {
      List<DataBank> _bankInfor = [];
      if (!bank.isLoading) {
        _bankInfor.addAll(bank.bankInfoModel.data);
        _isChecked = List<bool>.filled(_bankInfor.length, false);
      }
      return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorResources.getSecondary(context),
          title: Text(getTranslated('digital_payment', context)),
        ),
        body: bank.isLoading
            ? Center(
                child: CustomLoader(),
              )
            : SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      // height: App.height(context) * 20,
                      margin: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TitleRow(
                                      title: getTranslated('TOTAL', context)),
                                  AmountWidget(
                                      title: getTranslated('ORDER', context),
                                      amount: PriceConverter.convertPrice(
                                          context, widget.totalOrderAmount)),
                                  AmountWidget(
                                      title: getTranslated(
                                          'SHIPPING_FEE', context),
                                      amount: '(+) ' +
                                          PriceConverter.convertPrice(
                                              context, widget.shippingFee)),
                                  AmountWidget(
                                      title: getTranslated('DISCOUNT', context),
                                      amount: '(-) ' +
                                          PriceConverter.convertPrice(
                                              context, widget.discount)),
                                  AmountWidget(
                                      title: getTranslated('TAX', context),
                                      amount: '(+) ' +
                                          PriceConverter.convertPrice(
                                              context, widget.tax)),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: Dimensions
                                            .PADDING_SIZE_EXTRA_SMALL),
                                    child: Divider(
                                        height: 2,
                                        color: ColorResources.HINT_TEXT_COLOR),
                                  ),
                                  AmountWidget(
                                    title:
                                        getTranslated('TOTAL_PAYABLE', context),
                                    amount: PriceConverter.convertPrice(
                                        context, widget.totalPrice),
                                  ),
                                ]),
                          )
                        ],
                      ),
                    ),
                    Container(
                      // height: App.height(context) * 50,
                      margin: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            getTranslated('SELECT_PAYMENT_METHOD', context),
                            style: TextStyle(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                fontWeight: FontWeight.w600),
                          ),
                          ListView.builder(
                            itemCount: _bankInfor.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            primary: true,
                            itemBuilder: (context, index) {
                              return CustomCheckBox2(
                                bank: _bankInfor[index],
                                index: index,
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    Container(
                        // height: App.height(context) * 20,
                        margin: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                        padding:
                            EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(15)),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                                    getTranslated(
                                        'select_a_image_payment', context),
                                    style: robotoBold.copyWith(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ))),
                            SizedBox(
                              height: App.height(context) * 7,
                              child: ListView.builder(
                                itemCount: 1,
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: EdgeInsets.only(
                                        right: Dimensions.PADDING_SIZE_SMALL),
                                    child: InkWell(
                                      onTap: () async {
                                        if (index == 0 ||
                                            _files[index - 1].path.isNotEmpty) {
                                          PickedFile pickedFile =
                                              await imagePicker.getImage(
                                            source: ImageSource.gallery,
                                            maxWidth: 1024,
                                            maxHeight: 1024,
                                            imageQuality: 100,
                                          );
                                          if (pickedFile != null) {
                                            _files[index] =
                                                File(pickedFile.path);
                                            await Provider.of<OrderProvider>(
                                                    context,
                                                    listen: false)
                                                .uploadImage(_files[index],
                                                    (imageResponse) {
                                              paymentImage = imageResponse;
                                            });
                                            setState(() {});
                                          }
                                        } else {
                                          print(
                                              "CheckUploadClick ${index == 0 || _files[index - 1].path.isNotEmpty}");
                                        }
                                      },
                                      child: _files[index].path.isEmpty
                                          ? Container(
                                              height: 50,
                                              width: 50,
                                              alignment: Alignment.center,
                                              child: Stack(
                                                alignment: Alignment.center,
                                                children: <Widget>[
                                                  Icon(
                                                      Icons
                                                          .cloud_upload_outlined,
                                                      color: ColorResources
                                                          .getSecondary(
                                                              context)),
                                                  CustomPaint(
                                                    size: Size(100, 40),
                                                    foregroundPainter:
                                                        new MyPainter(
                                                            completeColor:
                                                                ColorResources
                                                                    .getSecondary(
                                                                        context),
                                                            width: 2),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: Image.file(
                                                _files[index],
                                                height: 50,
                                                width: 50,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                    ),
                                  );
                                },
                              ),
                            )
                          ],
                        ))
                  ],
                ),
              ),
        bottomSheet: Container(
            height: App.height(context) * 9,
            padding: EdgeInsets.symmetric(
                horizontal: Dimensions.PADDING_SIZE_LARGE,
                vertical: Dimensions.PADDING_SIZE_DEFAULT),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Consumer<CouponProvider>(builder: (context, coupon, child) {
                  double _couponDiscount =
                      coupon.discount != null ? coupon.discount : 0;
                  return Row(
                    children: [
                      Text(
                        PriceConverter.convertPrice(
                            context,
                            (widget.totalOrderAmount +
                                widget.shippingFee +
                                widget.tax -
                                _couponDiscount)),
                        style: titilliumSemiBold.copyWith(
                          color: ColorResources.getGreen(context),
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_OVER_LARGE
                              : Dimensions.FONT_SIZE_OVER_LARGE_IPAD,
                        ),
                      ),
                      SizedBox(
                        width: Dimensions.PADDING_SIZE_DEFAULT,
                      ),
                    ],
                  );
                }),
                !Provider.of<OrderProvider>(context).isLoading
                    ? Builder(
                        builder: (context) => SizedBox(
                          height: App.height(context) * 9,
                          width: App.width(context) * 30,
                          child: TextButton(
                            onPressed: () async {
                              DataBank bank = _bankInfor[
                                  Provider.of<OrderProvider>(context,
                                          listen: false)
                                      .paymentMethodIndex];

                              await Provider.of<OrderProvider>(context,
                                      listen: false)
                                  .paynow(widget.orderId, _callback,
                                      bankName: bank.bankName,
                                      image: paymentImage);
                            },
                            style: TextButton.styleFrom(
                              alignment: Alignment.center,
                              // padding: EdgeInsets.only(bottom: 4),
                              backgroundColor: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            child: Text(getTranslated('pay_now', context),
                                style: titilliumSemiBold.copyWith(
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_DEFAULT
                                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  color: Colors.white,
                                )),
                          ),
                        ),
                      )
                    : Container(
                        height: 50,
                        width: 50,
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              ColorResources.getSecondary(context)),
                        ),
                      ),
              ],
            )),
      );
    });
  }

  void _callback(
    bool isSuccess,
    String message,
  ) async {
    if (isSuccess) {
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => OrderDetailsScreen(
            orderId: int.parse(widget.orderId),
            orderModel: null,
          ),
        ),
      );
      showAnimatedDialog(
          context,
          MyDialog(
            icon: Icons.check,
            title: getTranslated('PAYMENT', context),
            description:
                getTranslated('your_payment_successfully_done', context),
            isFailed: false,
          ),
          dismissible: false,
          isFlip: true);
      Provider.of<OrderProvider>(context, listen: false).stopLoader();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            message,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    }
  }
}
