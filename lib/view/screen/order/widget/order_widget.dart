import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/order_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/date_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/order_details_screen.dart';

class OrderWidget extends StatelessWidget {
  final OrderModel orderModel;
  OrderWidget({this.orderModel});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => OrderDetailsScreen(
              orderModel: orderModel,
              orderId: orderModel.id,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(
            bottom: Dimensions.PADDING_SIZE_SMALL,
            left: Dimensions.PADDING_SIZE_SMALL,
            right: Dimensions.PADDING_SIZE_SMALL),
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
            border: Border.all(color: ColorResources.getSecondary(context)),
            color: Theme.of(context).highlightColor,
            borderRadius: BorderRadius.circular(5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  getTranslated('order_date', context),
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                  ),
                ),
                Text(
                  DateConverter.isoStringToLocalDateOnly(
                      context, orderModel.createdAt),
                  style: titilliumBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                    color: Theme.of(context).hintColor,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  getTranslated('ORDER_ID', context),
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                  ),
                ),
                Text(
                  orderModel.id.toString(),
                  style: titilliumBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                    color: Theme.of(context).hintColor,
                  ),
                ),
              ],
            ),
            Visibility(
              visible: false,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    getTranslated('STATUS', context),
                    style: titilliumRegular.copyWith(
                      fontSize: Responsive.isMobile(context)
                          ? Dimensions.FONT_SIZE_LARGE
                          : Dimensions.FONT_SIZE_SMALL_IPAD,
                    ),
                  ),
                  Text(
                    getTranslated(
                        orderModel.orderStatus.toUpperCase(), context),
                    style: titilliumBold.copyWith(
                      fontSize: Responsive.isMobile(context)
                          ? Dimensions.FONT_SIZE_LARGE
                          : Dimensions.FONT_SIZE_SMALL_IPAD,
                      color: Theme.of(context).hintColor,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  getTranslated('payment_method', context),
                  style: titilliumRegular.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                  ),
                ),
                Text(
                  getTranslated(orderModel.paymentMethod, context),
                  style: titilliumBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_SMALL_IPAD,
                    color: Theme.of(context).hintColor,
                  ),
                ),
              ],
            ),
            Text(
              PriceConverter.convertPrice(context, orderModel.orderAmount),
              style: titilliumSemiBold.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_LARGE
                      : Dimensions.FONT_SIZE_LARGE_IPAD,
                  color: Color.fromRGBO(117, 64, 251, 1)),
            ),
          ],
        ),
      ),
    );
  }
}
