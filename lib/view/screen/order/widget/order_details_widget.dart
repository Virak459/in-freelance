import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/order_details.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/order_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/provider/order_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_details_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/review_dialog.dart';
import 'package:provider/provider.dart';

class OrderDetailsWidget extends StatelessWidget {
  final OrderModel order;
  final OrderDetailsModel orderDetailsModel;

  final Function callback;
  OrderDetailsWidget(
      {@required this.order, @required this.orderDetailsModel, this.callback});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (order.orderStatus.toLowerCase() == 'delivered') {
          Provider.of<ProductDetailsProvider>(context, listen: false)
              .removeData();
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              backgroundColor: Colors.transparent,
              builder: (context) => ReviewBottomSheet(
                  productID: orderDetailsModel.productDetails.id.toString(),
                  callback: callback));
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CachedNetworkImage(
                  fit: BoxFit.cover,
                  width: Responsive.isMobile(context) ? 50 : 100,
                  height: Responsive.isMobile(context) ? 50 : 100,
                  imageUrl:
                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${orderDetailsModel.productDetails.thumbnail}',
                  placeholder: (c, o) => Image.asset(
                    Images.placeholder,
                    fit: BoxFit.cover,
                    width: Responsive.isMobile(context) ? 50 : 100,
                    height: Responsive.isMobile(context) ? 50 : 100,
                  ),
                ),
                SizedBox(width: Dimensions.MARGIN_SIZE_DEFAULT),
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              orderDetailsModel.productDetails.name,
                              style: titilliumSemiBold.copyWith(
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_SMALL
                                      : Dimensions.FONT_SIZE_SMALL_IPAD,
                                  color: Theme.of(context).hintColor),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          order.orderStatus.toLowerCase() == 'delivered'
                              ? Container(
                                  margin: EdgeInsets.only(
                                      left: Dimensions.PADDING_SIZE_SMALL),
                                  padding: EdgeInsets.symmetric(
                                      vertical:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                      horizontal:
                                          Dimensions.PADDING_SIZE_SMALL),
                                  decoration: BoxDecoration(
                                    color: ColorResources.getPrimary(context),
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                        width: 1,
                                        color: Theme.of(context).primaryColor),
                                  ),
                                  child: Text(getTranslated('review', context),
                                      style: titilliumRegular.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_SMALL
                                            : Dimensions.FONT_SIZE_SMALL_IPAD,
                                        color: Theme.of(context).highlightColor,
                                      )),
                                )
                              : SizedBox.shrink(),
                        ],
                      ),
                      SizedBox(height: Dimensions.MARGIN_SIZE_EXTRA_SMALL),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            PriceConverter.convertPrice(
                              context,
                              orderDetailsModel.price -
                                  orderDetailsModel.discount,
                            ),
                            style: titilliumSemiBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_LARGE
                                  : Dimensions.FONT_SIZE_LARGE_IPAD,
                              color: ColorResources.getGreen(context),
                            ),
                          ),
                          Visibility(
                            visible: orderDetailsModel.discount > 0,
                            child: Text(
                              PriceConverter.convertPrice(
                                context,
                                orderDetailsModel.price,
                              ),
                              style: titilliumRegular.copyWith(
                                decoration: TextDecoration.lineThrough,
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              ),
                            ),
                          ),
                          Text(
                            getTranslated('quantity', context) +
                                ' : x${orderDetailsModel.qty}',
                            style: titilliumSemiBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_SMALL
                                  : Dimensions.FONT_SIZE_SMALL_IPAD,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            (orderDetailsModel.variant != null &&
                    orderDetailsModel.variant.isNotEmpty)
                ? Padding(
                    padding: EdgeInsets.only(
                        top: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                    child: Row(children: [
                      SizedBox(width: 65),
                      Text('${getTranslated('variations', context)}: ',
                          style: titilliumSemiBold.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_SMALL
                                : Dimensions.FONT_SIZE_SMALL_IPAD,
                          )),
                      Flexible(
                          child: Text(orderDetailsModel.variant,
                              style: robotoRegular.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_SMALL
                                    : Dimensions.FONT_SIZE_SMALL_IPAD,
                                color: Theme.of(context).disabledColor,
                              ))),
                    ]),
                  )
                : SizedBox(),
            Divider(),
          ],
        ),
      ),
    );
  }
}
