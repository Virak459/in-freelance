// ignore_for_file: unused_import

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/address_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/seller_model.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/shop_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/date_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/localization_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/shimmer_loading.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/widget/payment_image_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/widget/address_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/order_details.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/order_model.dart';

import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/order_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/amount_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/widget/order_details_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/payment/payment_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/splash/splash_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/support/support_ticket_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/shop_orderby_app_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/tracking/tracking_screen.dart';
import 'package:provider/provider.dart';

class OrderDetailsScreen extends StatelessWidget {
  final OrderModel orderModel;
  final bool fromNotify;
  final int orderId;
  OrderDetailsScreen({
    @required this.orderModel,
    @required this.orderId,
    this.fromNotify = false,
  });

  void _loadData(BuildContext context) async {
    await Provider.of<OrderProvider>(context, listen: false)
        .initTrackingInfo(orderId.toString(), orderModel, true, context);
    if (orderModel == null) {
      await Provider.of<SplashProvider>(context, listen: false)
          .initConfig(context);
    }
    Provider.of<SellerProvider>(context, listen: false).removePrevOrderSeller();
    await Provider.of<ProfileProvider>(context, listen: false)
        .initAddressList(context);
    if (Provider.of<SplashProvider>(context, listen: false)
            .configModel
            .shippingMethod ==
        'sellerwise_shipping') {
      await Provider.of<OrderProvider>(context, listen: false).initShippingList(
        context,
        Provider.of<OrderProvider>(context, listen: false)
            .trackingModel
            .sellerId,
      );
    } else {
      await Provider.of<OrderProvider>(context, listen: false)
          .initShippingList(context, 1);
    }
    await Provider.of<OrderProvider>(context, listen: false).getOrderDetails(
      orderId.toString(),
      context,
      Provider.of<LocalizationProvider>(context, listen: false)
          .locale
          .countryCode,
    );
  }

  @override
  Widget build(BuildContext context) {
    _loadData(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: fromNotify
            ? BackButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SplashScreen(),
                    ),
                  );
                },
              )
            : BackButton(),
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('ORDER_DETAILS', context)),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: Dimensions.PADDING_SIZE_DEFAULT,
          ),
          Expanded(
            child: Consumer<OrderProvider>(
              builder: (context, order, child) {
                List<int> sellerList = [];
                List<List<OrderDetailsModel>> sellerProductList = [];
                double _order = 0;
                double _discount = 0;
                double _tax = 0;
                String shippingPartner = '';
                double _shippingFee = 0;
                AddressModel addressModel;
                String shippingAddress = '';
                double _totalPrice = 0;

                if (order.orderDetails != null) {
                  order.orderDetails.forEach((orderDetails) {
                    if (!sellerList
                        .contains(orderDetails.productDetails.userId)) {
                      sellerList.add(orderDetails.productDetails.userId);
                    }
                  });
                  sellerList.forEach((seller) {
                    Provider.of<SellerProvider>(context, listen: false)
                        .initSeller(seller.toString(), context);
                    List<OrderDetailsModel> orderList = [];
                    order.orderDetails.forEach((orderDetails) {
                      if (seller == orderDetails.productDetails.userId) {
                        orderList.add(orderDetails);
                      }
                    });
                    sellerProductList.add(orderList);
                  });

                  order.orderDetails.forEach((orderDetails) {
                    _order = _order + (orderDetails.price * orderDetails.qty);
                    _discount = _discount + orderDetails.discount;
                    _tax = _tax + (orderDetails.tax * orderDetails.qty);
                  });

                  if (order.shippingList != null) {
                    order.shippingList.forEach((shipping) {
                      print('shipping ${shipping.toJson()}');
                      if (shipping.id == order.trackingModel.shippingMethodId) {
                        shippingPartner = shipping.title;
                        _shippingFee = shipping.cost;
                      }
                    });
                  }
                }
                _totalPrice = (_order +
                    _shippingFee +
                    _tax -
                    (_discount + (order.trackingModel?.discountAmount ?? 0)));

                return order.orderDetails != null
                    ? ListView(
                        physics: BouncingScrollPhysics(),
                        padding: EdgeInsets.all(0),
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: getTranslated(
                                            'order_date', context),
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Text(
                                  DateConverter.isoStringToLocalDateOnly(
                                    context,
                                    order.trackingModel.createdAt,
                                  ),
                                  style: titilliumBold.copyWith(
                                    color: ColorResources.getTextTitle(context),
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text:
                                            getTranslated('ORDER_ID', context),
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Text(
                                  order.trackingModel.id.toString(),
                                  style: titilliumBold.copyWith(
                                    color: ColorResources.getTextTitle(context),
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  ),
                                ),
                              ],
                            ),
                          ),

                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: getTranslated('STATUS', context),
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Text(
                                  getTranslated(
                                      order.trackingModel.orderStatus
                                          .toUpperCase(),
                                      context),
                                  style: titilliumBold.copyWith(
                                    color: ColorResources.getTextTitle(context),
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            decoration: BoxDecoration(
                                color: Theme.of(context).highlightColor),
                            child: Column(
                              children: [
                                Row(children: [
                                  Expanded(
                                      child: Text(
                                          getTranslated('SHIPPING_TO', context),
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          ))),
                                  Consumer<ProfileProvider>(
                                    builder: (context, profile, child) {
                                      print(
                                          '=====Addreesss====>$shippingAddress');

                                      if (profile.addressList != null) {
                                        profile.addressList.forEach((address) {
                                          if (address.id ==
                                              order.trackingModel
                                                  .shippingAddress) {
                                            addressModel = address;
                                            shippingAddress = address.address;
                                          }
                                        });
                                      }
                                      return InkWell(
                                        onTap: () {
                                          showAnimatedDialog(
                                            context,
                                            AddressDialog(
                                              addressModel: addressModel,
                                            ),
                                          );
                                        },
                                        child: Text(
                                          shippingAddress,
                                          style: titilliumBold.copyWith(
                                            color: ColorResources.getTextTitle(
                                                context),
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ]),
                                Divider(),
                                Visibility(
                                  visible: shippingPartner.isNotEmpty,
                                  child: Row(children: [
                                    Expanded(
                                        child: Text(
                                            getTranslated(
                                                'SHIPPING_PARTNER', context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                            ))),
                                    Text(shippingPartner,
                                        style: titilliumSemiBold.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                            color: ColorResources.getPrimary(
                                                context))),
                                  ]),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: Dimensions.MARGIN_SIZE_DEFAULT),

                          Container(
                            padding:
                                EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                            color: Theme.of(context).highlightColor,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      getTranslated('ORDERED_PRODUCT', context),
                                      style: robotoBold.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                          color: Theme.of(context).hintColor)),
                                  Divider(),
                                ]),
                          ),

                          ListView.builder(
                            itemCount: sellerList.length,
                            physics: NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.all(0),
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        Dimensions.MARGIN_SIZE_EXTRA_LARGE,
                                    vertical: Dimensions.MARGIN_SIZE_SMALL),
                                color: Theme.of(context).highlightColor,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          return;
                                          // List<SellerModel> orderSellerList =
                                          //     Provider.of<SellerProvider>(
                                          //             context,
                                          //             listen: false)
                                          //         .orderSellerList;
                                          // if (orderSellerList.length != 0 &&
                                          //     sellerList[index] != 1) {
                                          //   Navigator.push(context,
                                          //       MaterialPageRoute(builder: (_) {
                                          //     ShopModel shopModel = ShopModel(
                                          //       id: orderSellerList[index]
                                          //           .shop
                                          //           .id,
                                          //       name: orderSellerList[index]
                                          //           .shop
                                          //           .name,
                                          //       image: orderSellerList[index]
                                          //           .shop
                                          //           .image,
                                          //       banner: orderSellerList[index]
                                          //           .shop
                                          //           .banner,
                                          //       contact: orderSellerList[index]
                                          //           .shop
                                          //           .contact,
                                          //       address: orderSellerList[index]
                                          //           .shop
                                          //           .address,
                                          //       seller: orderSellerList[index],
                                          //     );

                                          //     return ShopOrderByAppScreen(
                                          //         shopModel: shopModel);
                                          //   }));
                                          // }
                                        },
                                        child: Row(children: [
                                          Expanded(
                                              child: Text(
                                                  getTranslated(
                                                      'seller', context),
                                                  style: robotoBold.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_DEFAULT
                                                        : Dimensions
                                                            .FONT_SIZE_DEFAULT_IPAD,
                                                  ))),
                                          Text(
                                            sellerList[index] == 1
                                                ? getTranslated(
                                                    'Admin', context)
                                                : Provider.of<SellerProvider>(
                                                                context)
                                                            .orderSellerList
                                                            .length <
                                                        index + 1
                                                    ? sellerList[index]
                                                        .toString()
                                                    : '${Provider.of<SellerProvider>(context).orderSellerList[index].fName} '
                                                        '${Provider.of<SellerProvider>(context).orderSellerList[index].lName}',
                                            style: titilliumRegular.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                                color: ColorResources
                                                    .HINT_TEXT_COLOR),
                                          ),
                                          SizedBox(
                                              width: Dimensions
                                                  .PADDING_SIZE_EXTRA_SMALL),
                                          // Image.asset(
                                          //   Images.chat_image,
                                          //   color:
                                          //       Theme.of(context).primaryColor,
                                          //   width: 20,
                                          // ),
                                        ]),
                                      ),
                                      Text(
                                          getTranslated(
                                              'ORDERED_PRODUCT', context),
                                          style: robotoBold.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                              color: ColorResources
                                                  .HINT_TEXT_COLOR)),
                                      Divider(),
                                      ListView.builder(
                                        shrinkWrap: true,
                                        padding: EdgeInsets.all(0),
                                        itemCount:
                                            sellerProductList[index].length,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemBuilder: (context, i) =>
                                            OrderDetailsWidget(
                                                order: order.trackingModel,
                                                orderDetailsModel:
                                                    sellerProductList[index][i],
                                                callback: () {
                                                  showCustomSnackBar(
                                                      getTranslated(
                                                          'Review submitted successfully',
                                                          context),
                                                      context,
                                                      isError: false);
                                                }),
                                      ),
                                    ]),
                              );
                            },
                          ),
                          SizedBox(height: Dimensions.MARGIN_SIZE_DEFAULT),

                          // Amounts
                          Container(
                            padding:
                                EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                            color: Theme.of(context).highlightColor,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TitleRow(
                                      title: getTranslated('TOTAL', context)),
                                  AmountWidget(
                                      title: getTranslated('ORDER', context),
                                      amount: PriceConverter.convertPrice(
                                          context, _order)),
                                  Visibility(
                                    visible: _shippingFee > 0,
                                    child: AmountWidget(
                                        title: getTranslated(
                                            'SHIPPING_FEE', context),
                                        amount: PriceConverter.convertPrice(
                                            context, _shippingFee)),
                                  ),
                                  AmountWidget(
                                      title: getTranslated('DISCOUNT', context),
                                      amount: PriceConverter.convertPrice(
                                          context, _discount)),
                                  AmountWidget(
                                    title: getTranslated(
                                        'coupon_voucher', context),
                                    amount: PriceConverter.convertPrice(context,
                                        order.trackingModel.discountAmount),
                                  ),
                                  AmountWidget(
                                      title: getTranslated('TAX', context),
                                      amount: PriceConverter.convertPrice(
                                          context, _tax)),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: Dimensions
                                            .PADDING_SIZE_EXTRA_SMALL),
                                    child: Divider(
                                        height: 2,
                                        color: ColorResources.HINT_TEXT_COLOR),
                                  ),
                                  AmountWidget(
                                    title:
                                        getTranslated('TOTAL_PAYABLE', context),
                                    amount: PriceConverter.convertPrice(
                                        context, _totalPrice),
                                  ),
                                ]),
                          ),
                          SizedBox(height: Dimensions.MARGIN_SIZE_DEFAULT),

                          // Payment
                          Container(
                            padding:
                                EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                            decoration: BoxDecoration(
                                color: Theme.of(context).highlightColor),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(getTranslated('PAYMENT', context),
                                      style: robotoBold.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_DEFAULT
                                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                      )),
                                  SizedBox(
                                      height:
                                          Dimensions.MARGIN_SIZE_EXTRA_SMALL),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                            getTranslated(
                                                'PAYMENT_STATUS', context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                            )),
                                        Text(
                                          (order.trackingModel.paymentStatus !=
                                                      null &&
                                                  order.trackingModel
                                                      .paymentStatus.isNotEmpty)
                                              ? getTranslated(
                                                  order.trackingModel
                                                      .paymentStatus,
                                                  context)
                                              : 'Digital Payment',
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          ),
                                        ),
                                      ]),
                                  SizedBox(
                                      height:
                                          Dimensions.MARGIN_SIZE_EXTRA_SMALL),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                            getTranslated(
                                                'PAYMENT_PLATFORM', context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                            )),
                                        (order.trackingModel.paymentMethod !=
                                                    'cash_on_delivery' &&
                                                order.trackingModel
                                                        .paymentStatus ==
                                                    'unpaid')
                                            ? InkWell(
                                                onTap: () async {
                                                  String userID = await Provider
                                                          .of<ProfileProvider>(
                                                              context,
                                                              listen: false)
                                                      .getUserInfo(context);
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (_) =>
                                                              PaymentScreen(
                                                                orderId: order
                                                                    .trackingModel
                                                                    .id
                                                                    .toString(),
                                                                customerID:
                                                                    userID,
                                                                couponCode: '',
                                                                addressID: order
                                                                    .trackingModel
                                                                    .shippingAddress
                                                                    .toString(),
                                                                tax: _tax,
                                                                totalOrderAmount:
                                                                    _order,
                                                                shippingFee:
                                                                    _shippingFee,
                                                                discount:
                                                                    _discount,
                                                                totalPrice:
                                                                    _totalPrice,
                                                              )));
                                                },
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: Dimensions
                                                          .PADDING_SIZE_EXTRA_SMALL),
                                                  decoration: BoxDecoration(
                                                    color: ColorResources
                                                        .getPrimary(context),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                  ),
                                                  child: Text(
                                                      getTranslated(
                                                          'pay_now', context),
                                                      style: titilliumSemiBold
                                                          .copyWith(
                                                        fontSize: Responsive
                                                                .isMobile(
                                                                    context)
                                                            ? Dimensions
                                                                .FONT_SIZE_DEFAULT
                                                            : Dimensions
                                                                .FONT_SIZE_DEFAULT_IPAD,
                                                        color: Theme.of(context)
                                                            .highlightColor,
                                                      )),
                                                ),
                                              )
                                            : Text(
                                                order.trackingModel
                                                            .paymentMethod !=
                                                        null
                                                    ? getTranslated(
                                                        order.trackingModel
                                                            .paymentMethod,
                                                        context)
                                                    : 'N/A',
                                                style: titilliumBold.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                )),
                                      ]),
                                ]),
                          ),
                          SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                          order.trackingModel.paymentMethod !=
                                      'cash_on_delivery' &&
                                  order.orderDetails.length > 0 &&
                                  order.orderDetails[0].paymentImage != null
                              ? InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) => PaymentImageScreen(
                                          title: getTranslated(
                                              'digital_payment', context),
                                          imageList: [
                                            order.orderDetails[0].paymentImage
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                  child: CachedNetworkImage(
                                    height: Responsive.isMobile(context)
                                        ? 200
                                        : 400,
                                    fit: BoxFit.cover,
                                    imageUrl:
                                        order.orderDetails[0].paymentImage,
                                    placeholder: (c, o) => Image.asset(
                                      Images.placeholder,
                                      height: Responsive.isMobile(context)
                                          ? 200
                                          : 400,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                )
                              : SizedBox.shrink(),

                          // Buttons
                          Visibility(
                            visible: false,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: Dimensions.PADDING_SIZE_LARGE,
                                  vertical: Dimensions.PADDING_SIZE_SMALL),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: CustomButton(
                                      buttonText:
                                          getTranslated('TRACK_ORDER', context),
                                      onTap: () => Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  TrackingScreen(
                                                      orderID:
                                                          orderId.toString()))),
                                    ),
                                  ),
                                  SizedBox(
                                      width: Dimensions.PADDING_SIZE_SMALL),
                                  Expanded(
                                    child: SizedBox(
                                      height: 45,
                                      child: TextButton(
                                        onPressed: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) =>
                                                    SupportTicketScreen())),
                                        child: Text(
                                          getTranslated(
                                              'SUPPORT_CENTER', context),
                                          style: titilliumSemiBold.copyWith(
                                              fontSize: 16,
                                              color: ColorResources.getPrimary(
                                                  context)),
                                        ),
                                        style: TextButton.styleFrom(
                                            shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(6),
                                          side: BorderSide(
                                              color: ColorResources.getPrimary(
                                                  context)),
                                        )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    : LoadingPage(); //Center(child: CustomLoader(color: Theme.of(context).primaryColor));
              },
            ),
          )
        ],
      ),
    );
  }
}
