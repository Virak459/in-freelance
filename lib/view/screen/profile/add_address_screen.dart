import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/address_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_textfield.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location_geocoder/location_geocoder.dart';
import 'package:place_picker/place_picker.dart';
import 'package:provider/provider.dart';
// import 'package:google_maps_flutter_web/google_maps_flutter_web.dart' as webGM;

// ignore: must_be_immutable
class AddAddressScreen extends StatefulWidget {
  AddAddressScreen({
    Key key,
  }) : super(key: key);

  @override
  State<AddAddressScreen> createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  final Completer<GoogleMapController> _controller = Completer();
  final FocusNode _buttonAddressFocus = FocusNode();
  final FocusNode _cityFocus = FocusNode();
  final FocusNode _zipCodeFocus = FocusNode();
  final FocusNode _phoneCodeFocus = FocusNode();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _cityNameController = TextEditingController();
  final TextEditingController _zipCodeController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  GoogleMapController mapController;
  final _formKey = GlobalKey<FormState>();
  final Set<Marker> marker = new Set();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  String _currentAddress;
  Position _currentPosition;
  LatLng locationResult;
// use for check user access to the location
  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) async {
      setState(() {
        _currentPosition = position;
        locationResult = LatLng(position.latitude, position.longitude);
        _addMarker(locationResult);
      });
    }).catchError((e) {
      debugPrint(e);
    });
  }

  Future<void> _addMarker(LatLng latLng) async {
    Marker newMarker = Marker(
      draggable: true,
      markerId: MarkerId(latLng.toString()),
      position: latLng,
      onDragEnd: (value) {
        latLng = value;
      },
    );
    setState(() {
      marker.clear();
      marker.add(newMarker);
      locationResult = latLng;
      Find_by_piont(latLng.latitude, latLng.longitude);
    });
  }

  Future<void> Find_by_piont(double la, double lo) async {
    final response = await http.get(Uri.parse(
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${la},${lo}&key=AIzaSyBTl7_ljked9gmS7JCWlt3i_4zeSQhv9vY'));

    if (response.statusCode == 200) {
      // Successful response
      var jsonResponse = json.decode(response.body);
      var location = jsonResponse['results'][0]['geometry']['location'];
      var lati = location['lat'];
      var longi = location['lng'];
      List ls = jsonResponse['results'];
      List ac;
      bool check_sk = false, check_kn = false;
      for (int j = 0; j < ls.length; j++) {
        ac = jsonResponse['results'][j]['address_components'];
        for (int i = 0; i < ac.length; i++) {
          if (check_kn == false || check_sk == false) {
            if (jsonResponse['results'][j]['address_components'][i]['types']
                    [0] ==
                "administrative_area_level_1") {
              setState(() {
                check_kn = true;
                _addressController = TextEditingController(
                    text: jsonResponse['results'][j]['address_components'][i]
                            ['short_name']
                        .toString());
              });
            }
            if (jsonResponse['results'][j]['address_components'][i]['types']
                    [0] ==
                "administrative_area_level_2") {
              setState(() {
                check_sk = true;
                _cityNameController = TextEditingController(
                    text: jsonResponse['results'][j]['address_components'][i]
                            ['short_name']
                        .toString());
              });
            }
          }
        }
      }
    } else {
      // Error or invalid response
      print(response.statusCode);
    }
  }

  @override
  void initState() {
    _handleLocationPermission();
    _getCurrentPosition();
    // if (widget.locationResult != null) {
    //   _addressController.text =
    //       (widget?.name ?? '') + ' ' + (widget?.locality ?? '');
    //   _cityNameController.text = widget?.name ?? '';
    _phoneController.text = Provider.of<ProfileProvider>(context, listen: false)
            .userInfoModel
            ?.phone ??
        '';
    //   _location = widget.locationResult ?? '';
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(getTranslated('add_new_address', context)),
        backgroundColor: ColorResources.getSecondary(context),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // Container(
            //   child: SizedBox(
            //     height: Responsive.isMobile(context) ? 300 : 400,
            //     child: LayoutBuilder(
            //       builder: (context, constraints) {
            //         var maxWidth = constraints.biggest.width;
            //         var maxHeight = constraints.biggest.height;

            //         return Stack(
            //           children: <Widget>[
            // SizedBox(
            //   height: maxHeight,
            //   width: maxWidth,
            //   child: GoogleMap(
            //     initialCameraPosition: CameraPosition(
            //       target: widget.locationResult,
            //       zoom: 14.0,
            //     ),
            //     onTap: (latLng) {},
            //     // onMapCreated: (GoogleMapController controller) {
            //     //   _controller.complete(controller);
            //     // },
            //     // zoomControlsEnabled: false,
            //     // zoomGesturesEnabled: false,
            //     // scrollGesturesEnabled: false,
            //     // rotateGesturesEnabled: false,
            //     // onCameraMove: (CameraPosition newPosition) {
            //     //   print(newPosition.target);
            //     // },
            //     mapType: MapType.normal,
            //     // myLocationButtonEnabled: false,
            //     // myLocationEnabled: false,
            //   ),
            // ),

            // Positioned(
            //   bottom: maxHeight / 2,
            //   right: (maxWidth - 30) / 2,
            //   child: Icon(
            //     Icons.person_pin_circle,
            //     size: Responsive.isMobile(context)
            //         ? Dimensions.ICON_SIZE_LARGE
            //         : Dimensions.ICON_SIZE_LARGE_IPAD,
            //     color: ColorResources.getSecondary(context),
            //   ),
            // ),
            // Positioned(
            //   bottom: 10,
            //   right: 0,
            //   child: InkWell(
            //     onTap: () {
            //       checkGps();
            //     },
            //     child: Container(
            //       width: Responsive.isMobile(context)
            //           ? Dimensions.ICON_SIZE_LARGE
            //           : Dimensions.ICON_SIZE_LARGE_IPAD,
            //       height: Responsive.isMobile(context)
            //           ? Dimensions.ICON_SIZE_LARGE
            //           : Dimensions.ICON_SIZE_LARGE_IPAD,
            //       margin: EdgeInsets.only(
            //           right: Dimensions.PADDING_SIZE_LARGE),
            //       decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(
            //               Dimensions.PADDING_SIZE_SMALL),
            //           color: Colors.white),
            //       child: Icon(Icons.my_location,
            //           color: ColorResources.getSecondary(context),
            //           size: Responsive.isMobile(context)
            //               ? Dimensions.ICON_SIZE_DEFAULT
            //               : Dimensions.ICON_SIZE_DEFAULT_IPAD),
            //     ),
            //   ),
            // ),
            // Positioned(
            //   top: 10,
            //   right: 0,
            //   child: InkWell(
            //     onTap: () {
            //       Navigator.push(
            //         context,
            //         MaterialPageRoute(
            //           builder: (context) => PickMapScreen(),
            //         ),
            //       );
            //     },
            //     child: Container(
            //       width: Responsive.isMobile(context)
            //           ? Dimensions.ICON_SIZE_LARGE
            //           : Dimensions.ICON_SIZE_LARGE_IPAD,
            //       height: Responsive.isMobile(context)
            //           ? Dimensions.ICON_SIZE_LARGE
            //           : Dimensions.ICON_SIZE_LARGE_IPAD,
            //       margin: EdgeInsets.only(
            //           right: Dimensions.PADDING_SIZE_LARGE),
            //       decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(
            //               Dimensions.PADDING_SIZE_SMALL),
            //           color: Colors.white),
            //       child: Icon(Icons.fullscreen,
            //           color: ColorResources.getSecondary(context),
            //           size: Responsive.isMobile(context)
            //               ? Dimensions.ICON_SIZE_DEFAULT
            //               : Dimensions.ICON_SIZE_DEFAULT_IPAD),
            //     ),
            //   ),
            // ),
            //           ],
            //         );
            //       },
            //     ),
            //   ),
            // ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.3,
              child: (locationResult != null)
                  ? GoogleMap(
                      markers: marker.map((e) => e).toSet(),
                      zoomGesturesEnabled: true,
                      initialCameraPosition: CameraPosition(
                        target: locationResult,
                        zoom: 16,
                      ),
                      mapType: MapType.hybrid,
                      onMapCreated: (GoogleMapController controller) {
                        mapController = controller;
                      },
                      myLocationButtonEnabled: true,
                      myLocationEnabled: true,
                      onTap: (argument) {
                        setState(() {
                          _addMarker(argument);
                        });
                      },
                      onCameraMove: (CameraPosition cameraPositiona) {
                        // cameraPosition = cameraPositiona; //when map is dragging
                      },
                    )
                  : SizedBox(),
            ),
            Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
              decoration: BoxDecoration(),
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(height: 10),

                      CustomTextField(
                        hintText: getTranslated('ENTER_YOUR_ADDRESS', context),
                        controller: _addressController,
                        textInputType: TextInputType.streetAddress,
                        focusNode: _buttonAddressFocus,
                        nextNode: _cityFocus,
                        textInputAction: TextInputAction.next,
                      ),
                      Divider(thickness: 0.7, color: ColorResources.GREY),
                      Consumer<ProfileProvider>(
                          builder: (context, profileProvider, child) {
                        return Container(
                          padding: EdgeInsets.only(
                            left: Dimensions.PADDING_SIZE_DEFAULT,
                            right: Dimensions.PADDING_SIZE_DEFAULT,
                          ),
                          decoration: BoxDecoration(
                            color: Theme.of(context).highlightColor,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 1,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              )
                            ],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(6),
                                bottomLeft: Radius.circular(6)),
                          ),
                          alignment: Alignment.center,
                          child: DropdownButtonFormField<String>(
                            value: profileProvider.addressType,
                            isExpanded: true,
                            icon: Icon(Icons.keyboard_arrow_down,
                                color: Theme.of(context).primaryColor),
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            iconSize: Responsive.isMobile(context)
                                ? Dimensions.ICON_SIZE_DEFAULT
                                : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                            elevation: 16,
                            style: titilliumRegular.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                            ),
                            //underline: SizedBox(),

                            onChanged: profileProvider.updateCountryCode,
                            items: profileProvider.addressTypeList
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(getTranslated(value, context),
                                    style: titilliumRegular.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_DEFAULT
                                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyText1
                                            .color)),
                              );
                            }).toList(),
                          ),
                        );
                      }),
                      Divider(thickness: 0.7, color: ColorResources.GREY),
                      CustomTextField(
                        hintText: getTranslated('ENTER_YOUR_CITY', context),
                        controller: _cityNameController,
                        textInputType: TextInputType.streetAddress,
                        focusNode: _cityFocus,
                        nextNode: _zipCodeFocus,
                        textInputAction: TextInputAction.next,
                      ),
                      Divider(thickness: 0.7, color: ColorResources.GREY),
                      CustomTextField(
                        hintText: getTranslated('enter_phone_number', context),
                        isPhoneNumber: true,
                        controller: _phoneController,
                        textInputType: TextInputType.number,
                        focusNode: _phoneCodeFocus,
                        textInputAction: TextInputAction.done,
                      ),
                      Divider(thickness: 0.7, color: ColorResources.GREY),
                      // CustomTextField(
                      //   hintText: getTranslated('ENTER_YOUR_ZIP_CODE', context),
                      //   isPhoneNumber: true,
                      //   controller: _zipCodeController,
                      //   textInputType: TextInputType.number,
                      //   focusNode: _zipCodeFocus,
                      //   textInputAction: TextInputAction.done,
                      // ),

                      // Visibility(
                      //   visible: false,
                      //   child: CustomTextField(
                      //     hintText:
                      //         getTranslated('ENTER_YOUR_LATITUDE', context),
                      //     isPhoneNumber: true,
                      //     controller: _latController,
                      //     textInputType: TextInputType.number,
                      //     readOnly: true,
                      //     textInputAction: TextInputAction.done,
                      //   ),
                      // ),
                      // Divider(thickness: 0.7, color: ColorResources.GREY),
                      // Visibility(
                      //   visible: false,
                      //   child: CustomTextField(
                      //     hintText:
                      //         getTranslated('ENTER_YOUR_LONGITUDE', context),
                      //     isPhoneNumber: true,
                      //     controller: _lngController,
                      //     textInputType: TextInputType.number,
                      //     readOnly: true,
                      //     textInputAction: TextInputAction.done,
                      //   ),
                      // ),
                      // Divider(thickness: 0.7, color: ColorResources.GREY),

                      SizedBox(height: 30),
                      Provider.of<ProfileProvider>(context)
                                  .addAddressErrorText !=
                              null
                          ? Text(
                              Provider.of<ProfileProvider>(context)
                                  .addAddressErrorText,
                              style: titilliumRegular.copyWith(
                                color: ColorResources.RED,
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_DEFAULT
                                    : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              ))
                          : SizedBox.shrink(),
                      SizedBox(height: 30),
                      Consumer<ProfileProvider>(
                        builder: (context, profileProvider, child) {
                          return profileProvider.isLoading
                              ? CircularProgressIndicator(
                                  key: Key(''),
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Theme.of(context).primaryColor))
                              : CustomButton(
                                  buttonText:
                                      getTranslated('SAVE_ADDRESS', context),
                                  onTap: () async {
                                    await _addAddress();
                                    setState(() {});
                                  },
                                );
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _addAddress() {
    if (Provider.of<ProfileProvider>(context, listen: false).addressType ==
        Provider.of<ProfileProvider>(context, listen: false)
            .addressTypeList[0]) {
      Provider.of<ProfileProvider>(context, listen: false)
          .setAddAddressErrorText(
              getTranslated('SELECT_ADDRESS_TYPE', context));
    } else if (_addressController.text.isEmpty) {
      Provider.of<ProfileProvider>(context, listen: false)
          .setAddAddressErrorText(
              getTranslated('ADDRESS_FIELD_MUST_BE_REQUIRED', context));
    } else if (_cityNameController.text.isEmpty) {
      Provider.of<ProfileProvider>(context, listen: false)
          .setAddAddressErrorText(
              getTranslated('CITY_FIELD_MUST_BE_REQUIRED', context));
      // } else if (_zipCodeController.text.isEmpty) {
      //   Provider.of<ProfileProvider>(context, listen: false)
      //       .setAddAddressErrorText(
      //           getTranslated('ZIPCODE_FIELD_MUST_BE_REQUIRED', context));
    } else if (_phoneController.text.isEmpty) {
      Provider.of<ProfileProvider>(context, listen: false)
          .setAddAddressErrorText(
              getTranslated('PHONE_MUST_BE_REQUIRED', context));
    } else {
      Provider.of<ProfileProvider>(context, listen: false)
          .setAddAddressErrorText(null);
      AddressModel addressModel = AddressModel();
      addressModel.contactPersonName =
          Provider.of<ProfileProvider>(context, listen: false)
                  .userInfoModel
                  .fName +
              ' ' +
              Provider.of<ProfileProvider>(context, listen: false)
                  .userInfoModel
                  .lName;
      addressModel.addressType =
          Provider.of<ProfileProvider>(context, listen: false).addressType;
      // addressModel.city = _cityNameController.text;
      // addressModel.address = _addressController.text;
      // addressModel.zip = _zipCodeController.text;
      // addressModel.zip = '0';
      // addressModel.phone = _phoneController.text;
      // addressModel.latitude = widget.locationResult.latLng.latitude.toString();
      // addressModel.longitude =
      //     widget.locationResult.latLng.longitude.toString();
      // addressModel.addressMapName =
      //     widget.locationResult.name + ' ' + widget.locationResult.locality;

      addressModel.city = _cityNameController.text;
      addressModel.address = _addressController.text;
      addressModel.zip = _zipCodeController.text;
      addressModel.zip = '0';
      addressModel.phone = _phoneController.text;
      addressModel.latitude = locationResult.latitude.toString();
      addressModel.longitude = locationResult.latitude.toString();
      addressModel.addressMapName = _addressController.text.toString() +
              ' ' +
              _cityNameController.text.toString() ??
          '';

      Provider.of<ProfileProvider>(context, listen: false)
          .addAddress(addressModel, route);
    }
  }

  route(bool isRoute, String message) {
    if (isRoute) {
      _cityNameController.clear();
      _zipCodeController.clear();
      Provider.of<ProfileProvider>(context, listen: false)
          .initAddressList(context);
      Navigator.pop(context);
    }
  }
}
