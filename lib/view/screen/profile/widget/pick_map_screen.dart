import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/address_model.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PickMapScreen extends StatefulWidget {
  final GoogleMapController googleMapController;
  final Function(AddressModel address) onPicked;
  PickMapScreen({
    this.googleMapController,
    this.onPicked,
  });

  @override
  State<PickMapScreen> createState() => _PickMapScreenState();
}

class _PickMapScreenState extends State<PickMapScreen> {
  // ignore: unused_field
  GoogleMapController _mapController;
  // ignore: unused_field
  CameraPosition _cameraPosition;
  LatLng _initialPosition = LatLng(0, 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Center(
          child: SizedBox(
            child: Stack(children: [
              GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: _initialPosition,
                  zoom: 17,
                ),
                myLocationButtonEnabled: false,
                onMapCreated: (GoogleMapController mapController) {
                  _mapController = mapController;
                },
                scrollGesturesEnabled: true,
                zoomControlsEnabled: false,
                onCameraMove: (CameraPosition cameraPosition) {
                  _cameraPosition = cameraPosition;
                },
                onCameraMoveStarted: () {},
                onCameraIdle: () {},
              ),
              // Center(
              //   child: CircularProgressIndicator(),
              // ),
              // Positioned(
              //   top: Dimensions.PADDING_SIZE_LARGE,
              //   left: Dimensions.PADDING_SIZE_SMALL,
              //   right: Dimensions.PADDING_SIZE_SMALL,
              //   child: SearchLocationWidget(
              //       mapController: _mapController,
              //       pickedAddress: locationController.pickAddress,
              //       isEnabled: null),
              // ),
              Positioned(
                bottom: 80,
                right: Dimensions.PADDING_SIZE_SMALL,
                child: FloatingActionButton(
                  child: Icon(Icons.my_location,
                      color: Theme.of(context).primaryColor),
                  mini: true,
                  backgroundColor: Theme.of(context).cardColor,
                  onPressed: () {},
                ),
              ),
              Positioned(
                bottom: Dimensions.PADDING_SIZE_LARGE,
                left: Dimensions.PADDING_SIZE_SMALL,
                right: Dimensions.PADDING_SIZE_SMALL,
                child: Center(child: CircularProgressIndicator()),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
