import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/address_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class AddressDeleteDialog extends StatelessWidget {
  final int index;
  final AddressModel addressModel;
  AddressDeleteDialog({@required this.addressModel, @required this.index});
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).highlightColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
              child: Text(getTranslated('REMOVE_ADDRESS', context),
                  style: titilliumSemiBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_LARGE_IPAD,
                  )),
            ),
            Center(
              child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: Dimensions.PADDING_SIZE_DEFAULT,
                  ),
                  height: Responsive.isMobile(context) ? 280 : 580,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: Responsive.isMobile(context) ? 150 : 350,
                        child: GoogleMap(
                          initialCameraPosition: CameraPosition(
                            target: LatLng(
                                double.parse(addressModel.latitude ?? '0'),
                                double.parse(addressModel.longitude ?? '0')),
                            zoom: 14.0,
                          ),
                          zoomControlsEnabled: false,
                          zoomGesturesEnabled: false,
                          scrollGesturesEnabled: false,
                          rotateGesturesEnabled: false,
                          onCameraMove: (CameraPosition newPosition) {
                            print(newPosition.target);
                          },
                          mapType: MapType.normal,
                          myLocationButtonEnabled: false,
                          myLocationEnabled: false,
                        ),
                      ),
                      Text(
                        addressModel.addressType.isNotEmpty
                            ? getTranslated(
                                addressModel.addressType ?? "", context)
                            : '',
                        style: titilliumBold.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                      Text(
                        getTranslated('Address', context) +
                                ' : ${addressModel.address}' ??
                            "",
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                      Text(
                        getTranslated('City', context) +
                                ' : ${addressModel.city}' ??
                            "",
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                      Text(
                        getTranslated('PHONE_NO', context) +
                                ' : ${addressModel.phone}' ??
                            "",
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                    ],
                  )),
            ),
            Divider(
                height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                color: ColorResources.HINT_TEXT_COLOR),
            Row(children: [
              Expanded(
                  child: TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(getTranslated('CANCEL', context),
                    style: robotoRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        color: ColorResources.BLACK)),
              )),
              Container(
                height: 50,
                padding: EdgeInsets.symmetric(
                    vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                child: VerticalDivider(
                    width: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                    color: Theme.of(context).hintColor),
              ),
              Expanded(
                  child: TextButton(
                onPressed: () {
                  Provider.of<ProfileProvider>(context, listen: false)
                      .removeAddressById(addressModel.id, index, context);
                },
                child: Text(getTranslated('REMOVE', context),
                    style: robotoRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        color: ColorResources.getSecondary(context))),
              )),
            ]),
          ]),
    );
  }
}
