import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/address_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AddressDialog extends StatelessWidget {
  final AddressModel addressModel;
  AddressDialog({@required this.addressModel});
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Theme.of(context).highlightColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
              child: Text(getTranslated('address', context),
                  style: titilliumSemiBold.copyWith(
                    fontSize: Responsive.isMobile(context)
                        ? Dimensions.FONT_SIZE_LARGE
                        : Dimensions.FONT_SIZE_LARGE_IPAD,
                  )),
            ),
            Center(
              child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: Dimensions.PADDING_SIZE_DEFAULT,
                  ),
                  height: Responsive.isTablet(context) ? 580 : 400,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: Responsive.isMobile(context) ? 150 : 350,
                        child: GoogleMap(
                          initialCameraPosition: CameraPosition(
                            target: LatLng(
                                double.parse(addressModel.latitude ?? '0'),
                                double.parse(addressModel.longitude ?? '0')),
                            zoom: 14.0,
                          ),
                          zoomControlsEnabled: false,
                          zoomGesturesEnabled: false,
                          scrollGesturesEnabled: false,
                          rotateGesturesEnabled: false,
                          onCameraMove: (CameraPosition newPosition) {
                            print(newPosition.target);
                          },
                          mapType: MapType.normal,
                          myLocationButtonEnabled: false,
                          myLocationEnabled: false,
                        ),
                      ),
                      Text(
                        addressModel.addressType.isNotEmpty
                            ? getTranslated(
                                addressModel.addressType ?? "", context)
                            : '',
                        style: titilliumBold.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                      Text(
                        getTranslated('Address', context) +
                                ' : ${addressModel.address}' ??
                            "",
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                      Text(
                        getTranslated('City', context) +
                                ' : ${addressModel.city}' ??
                            "",
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                      Text(
                        getTranslated('PHONE_NO', context) +
                                ' : ${addressModel.phone}' ??
                            "",
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_DEFAULT
                              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        ),
                      ),
                    ],
                  )),
            ),
            Divider(
                height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                color: ColorResources.HINT_TEXT_COLOR),
            Row(children: [
              Expanded(
                  child: TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(getTranslated('ok', context),
                    style: robotoRegular.copyWith(
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_DEFAULT
                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                        color: ColorResources.getSecondary(context))),
              )),
            ]),
          ]),
    );
  }
}
