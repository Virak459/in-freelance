import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/user_info_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/button/custom_button.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_password_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/textfield/custom_textfield.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/widget/code_picker_widget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final FocusNode _fNameFocus = FocusNode();
  final FocusNode _lNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  // ignore: unused_field
  final FocusNode _addressFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmPasswordFocus = FocusNode();

  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  File file;
  final picker = ImagePicker();
  final GlobalKey<ScaffoldMessengerState> _scaffoldKey =
      GlobalKey<ScaffoldMessengerState>();
  String _countryDialCode = "+855";
  void _choose() async {
    final pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        imageQuality: 70,
        maxHeight: 500,
        maxWidth: 500);
    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  _updateUserAccount() async {
    String _firstName = _firstNameController.text.trim();
    String _lastName = _firstNameController.text.trim();
    String _email = _emailController.text.trim();
    String _phoneNumber = _phoneController.text.trim();
    String _password = _passwordController.text.trim();
    String _confirmPassword = _confirmPasswordController.text.trim();

    if (Provider.of<ProfileProvider>(context, listen: false)
                .userInfoModel
                .fName ==
            _firstNameController.text &&
        Provider.of<ProfileProvider>(context, listen: false)
                .userInfoModel
                .lName ==
            _lastNameController.text &&
        Provider.of<ProfileProvider>(context, listen: false)
                .userInfoModel
                .email ==
            _emailController.text &&
        Provider.of<ProfileProvider>(context, listen: false)
                .userInfoModel
                .countryCode ==
            _countryDialCode &&
        Provider.of<ProfileProvider>(context, listen: false)
                .userInfoModel
                .phoneNumber ==
            _phoneController.text &&
        file == null &&
        _passwordController.text.isEmpty &&
        _confirmPasswordController.text.isEmpty) {
      //showCustomSnackBar('Change something to update', context);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            getTranslated('Change something to update', context),
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    } else if (_firstName.isEmpty || _lastName.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            getTranslated('NAME_FIELD_MUST_BE_REQUIRED', context),
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    } else if (_email.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            getTranslated('EMAIL_MUST_BE_REQUIRED', context),
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    } else if (_phoneNumber.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            getTranslated('PHONE_MUST_BE_REQUIRED', context),
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    } else if ((_password.isNotEmpty && _password.length < 6) ||
        (_confirmPassword.isNotEmpty && _confirmPassword.length < 6)) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            getTranslated('Password should be at least 6 character', context),
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    } else if (_password != _confirmPassword) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            getTranslated('PASSWORD_DID_NOT_MATCH', context),
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
          backgroundColor: ColorResources.RED));
    } else {
      UserInfoModel updateUserInfoModel =
          Provider.of<ProfileProvider>(context, listen: false).userInfoModel;
      updateUserInfoModel.method = 'put';
      updateUserInfoModel.fName = _firstNameController.text ?? "";
      updateUserInfoModel.lName = _lastNameController.text ?? "";
      updateUserInfoModel.email = _emailController.text ?? "";
      updateUserInfoModel.phone =
          _countryDialCode + _phoneController.text ?? '';
      String pass = _passwordController.text ?? '';

      await Provider.of<ProfileProvider>(context, listen: false)
          .updateUserInfo(
        updateUserInfoModel,
        pass,
        file,
        Provider.of<AuthProvider>(context, listen: false).getUserToken(),
      )
          .then((response) {
        if (response.isSuccess) {
          Provider.of<ProfileProvider>(context, listen: false)
              .getUserInfo(context);
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                getTranslated('Updated Successfully', context),
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                ),
              ),
              backgroundColor: Colors.green));
          _passwordController.clear();
          _confirmPasswordController.clear();
          setState(() {});
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                response.message,
                style: titilliumRegular.copyWith(
                  fontSize: Responsive.isMobile(context)
                      ? Dimensions.FONT_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                ),
              ),
              backgroundColor: Colors.red));
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('PROFILE', context)),
      ),
      body: Consumer<ProfileProvider>(
        builder: (context, profile, child) {
          _firstNameController.text = profile.userInfoModel.fName;
          _lastNameController.text = profile.userInfoModel.lName;
          _emailController.text = profile.userInfoModel.email;
          _phoneController.text = profile.userInfoModel.phoneNumber;
          if (profile.userInfoModel.countryCode.isNotEmpty) {
            _countryDialCode = profile.userInfoModel.countryCode;
          }

          return Stack(
            clipBehavior: Clip.none,
            children: [
              Image.asset(
                Images.toolbar_background,
                fit: BoxFit.fill,
                height: 500,
                color: Provider.of<ThemeProvider>(context).darkTheme
                    ? Colors.black
                    : ColorResources.getSecondary(context),
              ),
              Container(
                padding:
                    EdgeInsets.only(top: Dimensions.MARGIN_SIZE_EXTRA_LARGE),
                child: Column(
                  children: [
                    Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Theme.of(context).cardColor,
                            border: Border.all(color: Colors.white, width: 3),
                            shape: BoxShape.circle,
                          ),
                          child: Stack(
                            clipBehavior: Clip.none,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(500),
                                child: file == null
                                    ? CachedNetworkImage(
                                        width: Responsive.isMobile(context)
                                            ? 100
                                            : 250,
                                        height: Responsive.isMobile(context)
                                            ? 100
                                            : 250,
                                        fit: BoxFit.cover,
                                        imageUrl:
                                            '${Provider.of<SplashProvider>(context, listen: false).baseUrls.customerImageUrl}/${profile.userInfoModel.image}',
                                        placeholder: (c, o) => Image.asset(
                                            Images.placeholder,
                                            width: Responsive.isMobile(context)
                                                ? 100
                                                : 250,
                                            height: Responsive.isMobile(context)
                                                ? 100
                                                : 250,
                                            fit: BoxFit.cover),
                                      )
                                    : Image.file(file,
                                        width: Responsive.isMobile(context)
                                            ? 100
                                            : 250,
                                        height: Responsive.isMobile(context)
                                            ? 100
                                            : 250,
                                        fit: BoxFit.fill),
                              ),
                              Positioned(
                                bottom: 0,
                                right: -10,
                                child: CircleAvatar(
                                  backgroundColor:
                                      ColorResources.LIGHT_SKY_BLUE,
                                  radius:
                                      Responsive.isMobile(context) ? 14 : 34,
                                  child: IconButton(
                                    onPressed: _choose,
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit,
                                        color: ColorResources.WHITE,
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                            : Dimensions
                                                .ICON_SIZE_DEFAULT_IPAD),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Text(
                          '${profile.userInfoModel.fName} ${profile.userInfoModel.lName}',
                          style: titilliumSemiBold.copyWith(
                            color: ColorResources.WHITE,
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_LARGE
                                : Dimensions.FONT_SIZE_LARGE_IPAD,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: Dimensions.MARGIN_SIZE_DEFAULT),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            color: ColorResources.getIconBg(context),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(
                                  Dimensions.MARGIN_SIZE_DEFAULT),
                              topRight: Radius.circular(
                                  Dimensions.MARGIN_SIZE_DEFAULT),
                            )),
                        child: ListView(
                          physics: BouncingScrollPhysics(),
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.MARGIN_SIZE_DEFAULT,
                                  left: Dimensions.MARGIN_SIZE_DEFAULT,
                                  right: Dimensions.MARGIN_SIZE_DEFAULT),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.person,
                                            color: ColorResources.getSecondary(
                                                context),
                                            size: Responsive.isMobile(context)
                                                ? Dimensions.ICON_SIZE_DEFAULT
                                                : Dimensions
                                                    .ICON_SIZE_DEFAULT_IPAD,
                                          ),
                                          SizedBox(
                                              width: Dimensions
                                                  .MARGIN_SIZE_EXTRA_SMALL),
                                          Text(
                                              getTranslated(
                                                  'FIRST_NAME', context),
                                              style: titilliumRegular.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                              ))
                                        ],
                                      ),
                                      SizedBox(
                                          height: Dimensions.MARGIN_SIZE_SMALL),
                                      CustomTextField(
                                        textInputType: TextInputType.name,
                                        focusNode: _fNameFocus,
                                        nextNode: _lNameFocus,
                                        hintText:
                                            profile.userInfoModel.fName ?? '',
                                        controller: _firstNameController,
                                      ),
                                    ],
                                  )),
                                  SizedBox(width: 15),
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.person,
                                            color: ColorResources.getSecondary(
                                                context),
                                            size: Responsive.isMobile(context)
                                                ? Dimensions.ICON_SIZE_DEFAULT
                                                : Dimensions
                                                    .ICON_SIZE_DEFAULT_IPAD,
                                          ),
                                          SizedBox(
                                              width: Dimensions
                                                  .MARGIN_SIZE_EXTRA_SMALL),
                                          Text(
                                              getTranslated(
                                                  'LAST_NAME', context),
                                              style: titilliumRegular.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                              ))
                                        ],
                                      ),
                                      SizedBox(
                                          height: Dimensions.MARGIN_SIZE_SMALL),
                                      CustomTextField(
                                        textInputType: TextInputType.name,
                                        focusNode: _lNameFocus,
                                        nextNode: _emailFocus,
                                        hintText: profile.userInfoModel.lName,
                                        controller: _lastNameController,
                                      ),
                                    ],
                                  )),
                                ],
                              ),
                            ),

                            // for Email
                            Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.MARGIN_SIZE_DEFAULT,
                                  left: Dimensions.MARGIN_SIZE_DEFAULT,
                                  right: Dimensions.MARGIN_SIZE_DEFAULT),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.alternate_email,
                                        color: ColorResources.getSecondary(
                                            context),
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                      ),
                                      SizedBox(
                                        width:
                                            Dimensions.MARGIN_SIZE_EXTRA_SMALL,
                                      ),
                                      Text(getTranslated('EMAIL', context),
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          ))
                                    ],
                                  ),
                                  SizedBox(
                                      height: Dimensions.MARGIN_SIZE_SMALL),
                                  CustomTextField(
                                    textInputType: TextInputType.emailAddress,
                                    focusNode: _emailFocus,
                                    nextNode: _phoneFocus,
                                    hintText: profile.userInfoModel.email ?? '',
                                    controller: _emailController,
                                  ),
                                ],
                              ),
                            ),

                            //For Phone
                            Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.MARGIN_SIZE_DEFAULT,
                                  left: Dimensions.MARGIN_SIZE_DEFAULT,
                                  right: Dimensions.MARGIN_SIZE_DEFAULT),
                              child: Row(children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: CodePickerWidget(
                                    countryFilter: ['kh', 'th', 'vn', 'us'],
                                    onChanged: (CountryCode countryCode) {
                                      _countryDialCode = countryCode.dialCode;
                                    },
                                    initialSelection: _countryDialCode,
                                    favorite: [_countryDialCode],
                                    showDropDownButton: true,
                                    padding: EdgeInsets.zero,
                                    showFlagMain: true,
                                    textStyle: TextStyle(
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                      color: ColorResources.BLACK,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Expanded(
                                    child: CustomTextField(
                                  hintText: getTranslated(
                                      'ENTER_MOBILE_NUMBER', context),
                                  controller: _phoneController,
                                  focusNode: _phoneFocus,
                                  nextNode: _passwordFocus,
                                  isPhoneNumber: true,
                                  textInputAction: TextInputAction.next,
                                  textInputType: TextInputType.phone,
                                )),
                              ]),
                            ),

                            // for Password
                            Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.MARGIN_SIZE_DEFAULT,
                                  left: Dimensions.MARGIN_SIZE_DEFAULT,
                                  right: Dimensions.MARGIN_SIZE_DEFAULT),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.lock_open,
                                        color: ColorResources.getSecondary(
                                            context),
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                      ),
                                      SizedBox(
                                          width: Dimensions
                                              .MARGIN_SIZE_EXTRA_SMALL),
                                      Text(getTranslated('PASSWORD', context),
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          ))
                                    ],
                                  ),
                                  SizedBox(
                                      height: Dimensions.MARGIN_SIZE_SMALL),
                                  CustomPasswordTextField(
                                    controller: _passwordController,
                                    focusNode: _passwordFocus,
                                    nextNode: _confirmPasswordFocus,
                                    textInputAction: TextInputAction.next,
                                  ),
                                ],
                              ),
                            ),

                            // for  re-enter Password
                            Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.MARGIN_SIZE_DEFAULT,
                                  left: Dimensions.MARGIN_SIZE_DEFAULT,
                                  right: Dimensions.MARGIN_SIZE_DEFAULT),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.lock_open,
                                        color: ColorResources.getSecondary(
                                            context),
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                      ),
                                      SizedBox(
                                          width: Dimensions
                                              .MARGIN_SIZE_EXTRA_SMALL),
                                      Text(
                                          getTranslated(
                                              'RE_ENTER_PASSWORD', context),
                                          style: titilliumRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                          ))
                                    ],
                                  ),
                                  SizedBox(
                                      height: Dimensions.MARGIN_SIZE_SMALL),
                                  CustomPasswordTextField(
                                    controller: _confirmPasswordController,
                                    focusNode: _confirmPasswordFocus,
                                    textInputAction: TextInputAction.done,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: Dimensions.MARGIN_SIZE_LARGE,
                          vertical: Dimensions.MARGIN_SIZE_EXTRA_LARGE),
                      child: !Provider.of<ProfileProvider>(context).isLoading
                          ? CustomButton(
                              onTap: _updateUserAccount,
                              buttonText:
                                  getTranslated('UPDATE_ACCOUNT', context))
                          : Center(
                              child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Theme.of(context).primaryColor))),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
