import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/localization_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/basewidget/custom_app_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/not_loggedin_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/add_address_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/widget/address_delete_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/widget/address_dialog.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location_geocoder/location_geocoder.dart';
import 'package:place_picker/entities/localization_item.dart';
import 'package:place_picker/place_picker.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class AddressListScreen extends StatefulWidget {
  @override
  State<AddressListScreen> createState() => _AddressListScreenState();
}

class _AddressListScreenState extends State<AddressListScreen> {
  bool servicestatus = false;

  bool haspermission = false;

  LocationPermission permission;

  Position position;

  LatLng currentlocation;

  String googleApikey = "AIzaSyBTl7_ljked9gmS7JCWlt3i_4zeSQhv9vY";

  @override
  Widget build(BuildContext context) {
    bool isGuestMode =
        !Provider.of<AuthProvider>(context, listen: false).isLoggedIn();
    if (!isGuestMode) {
      Provider.of<ProfileProvider>(context, listen: false)
          .initAddressTypeList(context);
      Provider.of<ProfileProvider>(context, listen: false)
          .initAddressList(context);
      Provider.of<ProfileProvider>(context, listen: false).getUserInfo(context);
    }
    getLocation();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('ADDRESS_LIST', context)),
        elevation: 0,
      ),
      body: Column(
        children: [
          isGuestMode
              ? Expanded(child: NotLoggedInWidget())
              : Consumer<ProfileProvider>(
                  builder: (context, profileProvider, child) {
                    return profileProvider.addressList != null
                        ? profileProvider.addressList.length > 0
                            ? Expanded(
                                child: RefreshIndicator(
                                  backgroundColor:
                                      ColorResources.getSecondary(context),
                                  color: Colors.white,
                                  onRefresh: () async {
                                    Provider.of<ProfileProvider>(context,
                                            listen: false)
                                        .initAddressTypeList(context);
                                    await Provider.of<ProfileProvider>(context,
                                            listen: false)
                                        .initAddressList(context);
                                  },
                                  child: ListView.builder(
                                    padding: EdgeInsets.all(0),
                                    itemCount:
                                        profileProvider.addressList.length,
                                    itemBuilder: (context, index) => Column(
                                      children: [
                                        Card(
                                          child: ListTile(
                                            onTap: () {
                                              showAnimatedDialog(
                                                context,
                                                AddressDialog(
                                                  addressModel: profileProvider
                                                      .addressList[index],
                                                ),
                                              );
                                            },
                                            title: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  profileProvider
                                                          .addressList[index]
                                                          .addressType
                                                          .isNotEmpty
                                                      ? getTranslated(
                                                          profileProvider
                                                                  .addressList[
                                                                      index]
                                                                  .addressType ??
                                                              "",
                                                          context)
                                                      : '',
                                                  style: titilliumBold.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_DEFAULT
                                                        : Dimensions
                                                            .FONT_SIZE_DEFAULT_IPAD,
                                                  ),
                                                ),
                                                Text(
                                                  getTranslated('Address',
                                                              context) +
                                                          ' : ${profileProvider.addressList[index].address}' ??
                                                      "",
                                                  style:
                                                      titilliumRegular.copyWith(
                                                    fontSize: Responsive
                                                            .isMobile(context)
                                                        ? Dimensions
                                                            .FONT_SIZE_DEFAULT
                                                        : Dimensions
                                                            .FONT_SIZE_DEFAULT_IPAD,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            subtitle: Text(
                                              getTranslated('City', context) +
                                                  ' : ${profileProvider.addressList[index].city ?? ""}',
                                              style: titilliumRegular.copyWith(
                                                fontSize: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .FONT_SIZE_DEFAULT
                                                    : Dimensions
                                                        .FONT_SIZE_DEFAULT_IPAD,
                                              ),
                                            ),
                                            trailing: IconButton(
                                              icon: Icon(
                                                Icons.delete_forever,
                                                color: Colors.red,
                                                size: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions
                                                        .ICON_SIZE_DEFAULT
                                                    : Dimensions
                                                        .ICON_SIZE_DEFAULT_IPAD,
                                              ),
                                              onPressed: () {
                                                showAnimatedDialog(
                                                  context,
                                                  AddressDeleteDialog(
                                                    addressModel:
                                                        profileProvider
                                                            .addressList[index],
                                                    index: index,
                                                  ),
                                                );

                                                // showCustomModalDialog(
                                                //   context,
                                                //   title: getTranslated(
                                                //       'REMOVE_ADDRESS',
                                                //       context),
                                                //   content: profileProvider
                                                //       .addressList[index]
                                                //       .address,
                                                //   cancelButtonText:
                                                //       getTranslated(
                                                //           'CANCEL', context),
                                                //   submitButtonText:
                                                //       getTranslated(
                                                //           'REMOVE', context),
                                                //   submitOnPressed: () {
                                                //     Provider.of<ProfileProvider>(
                                                //             context,
                                                //             listen: false)
                                                //         .removeAddressById(
                                                //             profileProvider
                                                //                 .addressList[
                                                //                     index]
                                                //                 .id,
                                                //             index,
                                                //             context);
                                                //     Navigator.of(context).pop();
                                                //   },
                                                //   cancelOnPressed: () =>
                                                //       Navigator.of(context)
                                                //           .pop(),
                                                // );
                                              },
                                            ),
                                          ),
                                        ),
                                        Visibility(
                                            visible: index ==
                                                (profileProvider
                                                        .addressList.length -
                                                    1),
                                            child: SizedBox(
                                              height: 100,
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : Expanded(
                                child:
                                    NoInternetOrDataScreen(isNoInternet: false))
                        : Expanded(
                            child: Center(
                                child: CircularProgressIndicator(
                                    color: ColorResources.getSecondary(context),
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Theme.of(context).primaryColor))));
                  },
                ),
        ],
      ),
      floatingActionButton: isGuestMode
          ? null
          : FloatingActionButton(
              onPressed: () async {
                // showModalBottomSheet(
                //   context: context,
                //   isScrollControlled: true,
                //   backgroundColor: Colors.transparent,
                //   builder: (context) => AddAddressBottomSheet(),
                // );
                // await checkGps(context);
                await getLocation();
                // LocationResult result = await Navigator.of(context).push(
                //   MaterialPageRoute(
                //     builder: (context) => PlacePicker(
                //       "AIzaSyAWWG-m3cl19_mp4v59CnDO7noccaHdykw",
                //       displayLocation: currentlocation,
                //       localizationItem: LocalizationItem(
                //         languageCode: Provider.of<LocalizationProvider>(context,
                //                 listen: false)
                //             .locale
                //             .languageCode,
                //         nearBy: getTranslated('nearBy', context),
                //         findingPlace: getTranslated('findingPlace', context),
                //         noResultsFound:
                //             getTranslated('noResultsFound', context),
                //         unnamedLocation:
                //             getTranslated('unnamedLocation', context),
                //         tapToSelectLocation:
                //             getTranslated('tapToSelectLocation', context),
                //       ),
                //     ),
                //   ),
                // );

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddAddressScreen(
                        // locationResult: currentlocation,
                        ),
                  ),
                );
              },
              child: Icon(Icons.add, color: Theme.of(context).highlightColor),
              backgroundColor: ColorResources.getSecondary(context),
            ),
    );
  }

  checkGps(BuildContext context) async {
    servicestatus = await Geolocator.isLocationServiceEnabled();
    if (servicestatus) {
      permission = await Geolocator.checkPermission();

      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          showCustomSnackBar('Location permissions are denied', context,
              isError: false);
        } else if (permission == LocationPermission.deniedForever) {
          showCustomSnackBar(
              'Location permissions are permanently denied', context,
              isError: false);
        } else {
          haspermission = true;
        }
      } else {
        haspermission = true;
      }

      if (haspermission) {
        getLocation();
      }
    } else {
      showCustomSnackBar(
          'GPS Service is not enabled, turn on GPS location', context,
          isError: false);
    }
  }

  String name;
  String locality;
  String city;
  getLocation() async {
    position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentlocation = LatLng(position.longitude, position.latitude);
    print("virak ${currentlocation}");
  }
}
