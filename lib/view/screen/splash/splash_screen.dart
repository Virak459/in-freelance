import 'dart:async';
// ignore: unused_import
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/auth_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/maintenance/maintenance_screen.dart';
// ignore: unused_import
import 'package:flutter_sixvalley_ecommerce/view/screen/version/version_screen.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  GlobalKey<ScaffoldMessengerState> _globalKey = GlobalKey();
  StreamSubscription<ConnectivityResult> _onConnectivityChanged;

  @override
  void initState() {
    super.initState();

    bool _firstTime = false;
    _onConnectivityChanged = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (!_firstTime) {
        bool isNotConnected = result != ConnectivityResult.wifi &&
            result != ConnectivityResult.mobile;
        isNotConnected
            ? SizedBox()
            : ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: isNotConnected ? Colors.red : Colors.green,
          duration: Duration(seconds: isNotConnected ? 6000 : 3),
          content: Text(
            isNotConnected
                ? getTranslated('no_connection', context)
                : getTranslated('connected', context),
            textAlign: TextAlign.center,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
          ),
        ));
        if (!isNotConnected) {
          _route();
        }
      }
      _firstTime = false;
    });

    _route();
  }

  @override
  void dispose() {
    super.dispose();

    _onConnectivityChanged.cancel();
  }

  void _route() {
    Provider.of<SplashProvider>(context, listen: false)
        .initConfig(context)
        .then((bool isSuccess) {
      if (isSuccess) {
        Provider.of<SplashProvider>(context, listen: false)
            .initSharedPrefData();
        // if (Provider.of<SplashProvider>(context, listen: false)
        //     .configModel
        //     .allowAppUpdate) {
        //   if (Platform.isAndroid) {
        //     print(
        //         'version android : config ${Provider.of<SplashProvider>(context, listen: false).configModel.androidVersion}  App:${Provider.of<SplashProvider>(context, listen: false).packageInfo.version}');
        //     if (Provider.of<SplashProvider>(context, listen: false)
        //             .configModel
        //             .androidVersion !=
        //         Provider.of<SplashProvider>(context, listen: false)
        //             .packageInfo
        //             .version) {
        //       Navigator.of(context).pushReplacement(MaterialPageRoute(
        //           builder: (BuildContext context) => UpdateVersion()));

        //       // App.launchUrl(Provider.of<SplashProvider>(context, listen: false)
        //       //     .configModel
        //       //     .androidUrl);
        //     }
        //   } else if (Platform.isIOS) {
        //     if (Provider.of<SplashProvider>(context, listen: false)
        //             .configModel
        //             .iosVersion !=
        //         Provider.of<SplashProvider>(context, listen: false)
        //             .packageInfo
        //             .version) {
        //       Navigator.of(context).pushReplacement(MaterialPageRoute(
        //           builder: (BuildContext context) => UpdateVersion()));
        //       // App.launchUrl(Provider.of<SplashProvider>(context, listen: false)
        //       //     .configModel
        //       //     .iosUrl);
        //     }
        //   }
        // }
        Timer(Duration(seconds: 8), () async {
          if (Provider.of<SplashProvider>(context, listen: false)
              .configModel
              .maintenanceMode) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => MaintenanceScreen()));
          } else {
            setState(() {
              print("\n\nLog screen okay\n\n");
            });
          }
          if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
            try {
              await Provider.of<ProfileProvider>(context, listen: false)
                  .getUserInfo(context);

              await Provider.of<AuthProvider>(context, listen: false)
                  .updateToken(context);
            } catch (e) {
              Provider.of<AuthProvider>(context, listen: false)
                  .clearSharedData()
                  .then((condition) {
                Navigator.pop(context);
                Provider.of<ProfileProvider>(context, listen: false)
                    .clearHomeAddress();
                Provider.of<ProfileProvider>(context, listen: false)
                    .clearOfficeAddress();
                Provider.of<AuthProvider>(context, listen: false)
                    .clearSharedData();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => AuthScreen()),
                    (route) => false);
              });
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => AuthScreen()));
            }

            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => DashBoardScreen()));
          } else {
            if (Provider.of<SplashProvider>(context, listen: false)
                .showIntro()) {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => AuthScreen()));
              // Navigator.of(context).pushReplacement(MaterialPageRoute(
              //     builder: (BuildContext context) => OnBoardingScreen(
              //           indicatorColor: ColorResources.GREY,
              //           selectedIndicatorColor:
              //               Theme.of(context).primaryColor,
              //         )));
            } else {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => AuthScreen()));
            }
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //Provider.of<AuthProvider>(context, listen: false).clearSharedData();
    return Scaffold(
      key: _globalKey,
      backgroundColor: ColorResources.WHITE,
      body: Provider.of<SplashProvider>(context).hasConnection
          ? Stack(
              clipBehavior: Clip.none,
              children: [
                // Container(
                //   width: MediaQuery.of(context).size.width,
                //   height: MediaQuery.of(context).size.height,
                //   color: Provider.of<ThemeProvider>(context).darkTheme
                //       ? Colors.black
                //       : ColorResources.getPrimary(context),
                //   child: CustomPaint(
                //     painter: SplashPainter(),
                //   ),
                // ),

                Center(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: App.height(context) * 5,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        AuthScreen()));
                          },
                          child: Container(
                            padding: EdgeInsets.all(50),
                            child: Image.asset(
                              Images.splash_logo,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        // Container(
                        //   padding: EdgeInsets.all(5),
                        //   child: Image.asset(
                        //     Images.splash_image,
                        //     fit: BoxFit.fill,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          // Stack(
          //     clipBehavior: Clip.none,
          //     children: [
          //       Container(
          //         width: MediaQuery.of(context).size.width,
          //         height: MediaQuery.of(context).size.height,
          //         color: Provider.of<ThemeProvider>(context).darkTheme
          //             ? Colors.black
          //             : ColorResources.getPrimary(context),
          //         child: CustomPaint(
          //           painter: SplashPainter(),
          //         ),
          //       ),
          //       Center(
          //         child: Column(
          //           mainAxisSize: MainAxisSize.min,
          //           children: [
          //             Image.asset(Images.splash_logo,
          //                 height: App.height(context) * 60,
          //                 fit: BoxFit.scaleDown,
          //                 width: App.width(context) * 60),
          //           ],
          //         ),
          //       ),
          //     ],
          //   )
          : NoInternetOrDataScreen(isNoInternet: true, child: SplashScreen()),
    );
  }
}
