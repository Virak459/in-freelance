import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/setting/widget/currency_dialog.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<SplashProvider>(context, listen: false).setFromSetting(true);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorResources.getSecondary(context),
        title: Text(getTranslated('settings', context)),
      ),
      body: WillPopScope(
        onWillPop: () {
          Provider.of<SplashProvider>(context, listen: false)
              .setFromSetting(false);
          return Future.value(true);
        },
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          // Padding(
          //   padding: EdgeInsets.only(
          //       top: Dimensions.PADDING_SIZE_LARGE,
          //       left: Dimensions.PADDING_SIZE_LARGE),
          //   child: Text(getTranslated('settings', context),
          //       style: titilliumSemiBold.copyWith(
          //           fontSize: Dimensions.FONT_SIZE_LARGE)),
          // ),
          Expanded(
              child: ListView(
            physics: BouncingScrollPhysics(),
            padding: EdgeInsets.symmetric(
                horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
            children: [
              // SwitchListTile(
              //   value: Provider.of<ThemeProvider>(context).darkTheme,
              //   onChanged: (bool isActive) =>
              //       Provider.of<ThemeProvider>(context, listen: false)
              //           .toggleTheme(),
              //   title: Text(getTranslated('dark_theme', context),
              //       style: titilliumRegular.copyWith(
              //           fontSize: Dimensions.FONT_SIZE_LARGE)),
              // ),
              SizedBox(
                height: Dimensions.MARGIN_SIZE_DEFAULT,
              ),
              TitleButton(
                image: Images.language,
                title: getTranslated('choose_language', context),
                onTap: () => showAnimatedDialog(
                    context, CurrencyDialog(isCurrency: false)),
              ),
              TitleButton(
                image: Images.currency,
                title:
                    '${getTranslated('currency', context)} (${Provider.of<SplashProvider>(context).myCurrency.name})',
                onTap: () => showAnimatedDialog(context, CurrencyDialog()),
              ),
              /*TitleButton(
                  image: Images.preference,
                  title: Strings.preference,
                  onTap: () => showAnimatedDialog(context, PreferenceDialog()),
                ),*/
            ],
          )),
        ]),
      ),
    );
  }
}

class TitleButton extends StatelessWidget {
  final String image;
  final String title;
  final Function onTap;
  TitleButton(
      {@required this.image, @required this.title, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          bottom: Responsive.isMobile(context)
              ? 0
              : Dimensions.PADDING_SIZE_DEFAULT),
      child: ListTile(
        leading: Image.asset(image,
            width: Responsive.isMobile(context) ? 25 : 35,
            height: Responsive.isMobile(context) ? 25 : 35,
            fit: BoxFit.fill,
            color: ColorResources.getSecondary(context)),
        title: Text(title,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_LARGE
                  : Dimensions.FONT_SIZE_LARGE_IPAD,
            )),
        onTap: onTap,
      ),
    );
  }
}
