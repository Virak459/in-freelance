import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';

class CustomLoader extends StatefulWidget {
  const CustomLoader({Key key}) : super(key: key);

  @override
  State<CustomLoader> createState() => _CustomLoaderState();
}

class _CustomLoaderState extends State<CustomLoader> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        color: Colors.white,
        valueColor:
            AlwaysStoppedAnimation<Color>(ColorResources.getSecondary(context)),
      ),
    );
  }
}
