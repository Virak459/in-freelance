import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/product_details_screen.dart';
import 'package:provider/provider.dart';

class ProductWidget extends StatelessWidget {
  final Product productModel;
  final bool fromShop;
  ProductWidget({@required this.productModel, this.fromShop = false});

  @override
  Widget build(BuildContext context) {
    double _startingPrice;
    double _endingPrice;
    if (productModel.variation.length != 0) {
      List<double> _priceList = [];
      productModel.variation
          .forEach((variation) => _priceList.add(variation.price));
      _priceList.sort((a, b) => a.compareTo(b));
      _startingPrice = _priceList[0];
      if (_priceList[0] < _priceList[_priceList.length - 1]) {
        _endingPrice = _priceList[_priceList.length - 1];
      }
    } else {
      _startingPrice = productModel.unitPrice;
    }
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 1000),
              pageBuilder: (context, anim1, anim2) =>
                  ProductDetails(product: productModel, fromShop: fromShop),
            ));
      },
      child: Container(
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          // color: Theme.of(context).highlightColor,
          color: Color(0xFFEEE6FE),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 5)
          ],
        ),
        child: Stack(children: [
          Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            // Product Image
            Container(
              height: Responsive.isMobile(context) ? 130 : 230,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                  topRight: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                  topRight: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                ),
                child: CachedNetworkImage(
                  fit: BoxFit.fitHeight,
                  imageUrl:
                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${productModel.thumbnail}',
                  placeholder: (c, o) =>
                      Image.asset(Images.placeholder, fit: BoxFit.cover),
                ),
              ),
            ),
            Divider(
              color: Colors.grey.withOpacity(
                .2,
              ),
            ),
            // Product Details
            Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                  bottomRight: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                ),
              ),
              // height: App.height(context) * 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    productModel.name ?? '',
                    style: robotoRegular.copyWith(
                      height: 2,
                      fontSize: Responsive.isMobile(context)
                          ? Dimensions.FONT_SIZE_DEFAULT
                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  Container(
                    child: Text(
                      // PriceConverter.convertPrice(
                      //     context, productModel.unitPrice ?? 0.0,
                      //     discountType: productModel.discountType,
                      //     discount: productModel.discount),
                      '${PriceConverter.convertPrice(context, _startingPrice, discount: productModel.discount, discountType: productModel.discountType)}'
                      '${_endingPrice != null ? ' - ${PriceConverter.convertPrice(context, _endingPrice, discount: productModel.discount, discountType: productModel.discountType)}' : ''}',

                      style: robotoBold.copyWith(
                        color: Color.fromRGBO(147, 105, 251, 1),
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_LARGE
                            : Dimensions.FONT_SIZE_LARGE_IPAD,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  productModel.discount > 0 && productModel.discount != null
                      ? Container(
                          child: Text(
                            // PriceConverter.convertPrice(context, productModel.unitPrice),
                            '${PriceConverter.convertPrice(context, _startingPrice)}'
                            '${_endingPrice != null ? ' - ${PriceConverter.convertPrice(context, _endingPrice)}' : ''}',

                            style: robotoBold.copyWith(
                              color: Theme.of(context).hintColor,
                              decoration: TextDecoration.lineThrough,
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_SMALL_IPAD,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      : SizedBox.shrink(),
                ],
              ),
            ),
          ]),

          // Off

          productModel.discount >= 1
              ? Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    height: 25,
                    padding: EdgeInsets.symmetric(
                        horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                    decoration: BoxDecoration(
                      // color: ColorResources.getSecondary(context),
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                    ),
                    child: Center(
                      child: Text(
                        PriceConverter.percentageCalculation(
                            context,
                            productModel.unitPrice,
                            productModel.discount,
                            productModel.discountType),
                        style: robotoBold.copyWith(
                          color: Theme.of(context).highlightColor,
                          fontSize: Dimensions.FONT_SIZE_LARGE,
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ]),
      ),
    );
  }
}
