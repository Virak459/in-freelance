import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:provider/provider.dart';

class CustomPasswordTextField extends StatefulWidget {
  final TextEditingController controller;
  final String hintTxt;
  final FocusNode focusNode;
  final FocusNode nextNode;
  final TextInputAction textInputAction;

  CustomPasswordTextField(
      {this.controller,
      this.hintTxt,
      this.focusNode,
      this.nextNode,
      this.textInputAction});

  @override
  _CustomPasswordTextFieldState createState() =>
      _CustomPasswordTextFieldState();
}

class _CustomPasswordTextFieldState extends State<CustomPasswordTextField> {
  bool _obscureText = true;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).highlightColor,
        borderRadius: BorderRadius.circular(6),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 7,
              offset: Offset(0, 1)) // changes position of shadow
        ],
      ),
      child: TextFormField(
        style: titilliumRegular.copyWith(
            fontSize: Responsive.isMobile(context)
                ? Dimensions.FONT_SIZE_DEFAULT
                : Dimensions.FONT_SIZE_DEFAULT_IPAD),
        cursorColor: Theme.of(context).primaryColor,
        controller: widget.controller,
        obscureText: _obscureText,
        focusNode: widget.focusNode,
        textInputAction: widget.textInputAction ?? TextInputAction.next,
        onFieldSubmitted: (v) {
          setState(() {
            widget.textInputAction == TextInputAction.done
                ? FocusScope.of(context).consumeKeyboardToken()
                : FocusScope.of(context).requestFocus(widget.nextNode);
          });
        },
        validator: (value) {
          return null;
        },
        decoration: InputDecoration(
            suffixIcon: IconButton(
                icon: Icon(
                  _obscureText ? Icons.visibility_off : Icons.visibility,
                  size: Responsive.isMobile(context)
                      ? Dimensions.ICON_SIZE_DEFAULT
                      : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                ),
                onPressed: _toggle),
            hintText: widget.hintTxt ?? '',
            contentPadding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15),
            isDense: true,
            filled: true,
            fillColor:
                Provider.of<ThemeProvider>(context, listen: false).darkTheme
                    ? HexColor('ed1848')
                    : HexColor('eee6fe'),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white10),
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white10),
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            ),
            hintStyle: titilliumRegular.copyWith(
                color: Theme.of(context).hintColor,
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_DEFAULT
                    : Dimensions.FONT_SIZE_DEFAULT_IPAD),
            border: InputBorder.none),
      ),
    );
  }
}
