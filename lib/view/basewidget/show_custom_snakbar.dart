import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

void showCustomSnackBar(String message, BuildContext context,
    {bool isError = true}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    backgroundColor: isError ? ColorResources.getRed(context) : Colors.green,
    content: Text(
      message,
      style: titilliumRegular.copyWith(
        fontSize: Responsive.isMobile(context)
            ? Dimensions.FONT_SIZE_DEFAULT
            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
      ),
    ),
   duration: Duration(seconds: 2),));
}
