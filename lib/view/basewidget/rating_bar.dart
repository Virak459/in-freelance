import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

// ignore: must_be_immutable
class RatingBar extends StatelessWidget {
  final double rating;
  double size;
  final int number;
  RatingBar({@required this.rating, this.size, this.number = 5});

  @override
  Widget build(BuildContext context) {
    List<Widget> _starList = [];
    if (size == null) {
      size = Responsive.isMobile(context)
          ? Dimensions.ICON_SIZE_SMALL
          : Dimensions.ICON_SIZE_SMALL_IPAD;
    }

    int realNumber = rating.floor();
    int partNumber = ((rating - realNumber) * 10).ceil();

    for (int i = 0; i < number; i++) {
      if (i < realNumber) {
        _starList.add(Icon(Icons.star, color: Colors.orange, size: size));
      } else if (i == realNumber) {
        _starList.add(SizedBox(
          height: size,
          width: size,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Icon(Icons.star, color: Colors.orange, size: size),
              ClipRect(
                clipper: _Clipper(part: partNumber),
                child: Icon(Icons.star, color: Colors.grey, size: size),
              )
            ],
          ),
        ));
      } else {
        _starList.add(Icon(Icons.star, color: Colors.grey, size: size));
      }
    }

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: _starList,
    );
  }
}

class _Clipper extends CustomClipper<Rect> {
  final int part;

  _Clipper({@required this.part});

  @override
  Rect getClip(Size size) {
    return Rect.fromLTRB(
      (size.width / 10) * part,
      0.0,
      size.width,
      size.height,
    );
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) => true;
}
