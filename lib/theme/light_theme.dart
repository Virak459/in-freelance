import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';

ThemeData light = ThemeData(
  fontFamily: 'TitilliumWeb',
  primaryColor: HexColor("0082ca"),
  primaryColorLight: HexColor("ed1848"),
  brightness: Brightness.light,
  highlightColor: Colors.white,
  hintColor: Color.fromARGB(255, 0, 0, 0),
  pageTransitionsTheme: PageTransitionsTheme(builders: {
    TargetPlatform.android: ZoomPageTransitionsBuilder(),
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
  }),
);
