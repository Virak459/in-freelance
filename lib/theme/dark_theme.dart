import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';

ThemeData dark = ThemeData(
  fontFamily: 'TitilliumWeb',
  primaryColor: HexColor("0082ca"),
  primaryColorLight:  HexColor("ed1848"),
  brightness: Brightness.dark,
  highlightColor: Color(0xFF252525),
  hintColor: Color.fromARGB(255, 0, 0, 0),
  pageTransitionsTheme: PageTransitionsTheme(builders: {
    TargetPlatform.android: ZoomPageTransitionsBuilder(),
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
  }),
);
