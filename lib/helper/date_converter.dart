import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/localization_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DateConverter {
  static String formatDate(BuildContext context, DateTime dateTime) {
    return DateFormat(
            'yyyy-MM-dd hh:mm:ss',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime);
  }

  static String estimatedDate(BuildContext context, DateTime dateTime) {
    return DateFormat(
            'dd MMM yyyy',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime);
  }

  static DateTime convertStringToDatetime(
      BuildContext context, String dateTime) {
    return DateFormat(
            "yyyy-MM-dd hh:mm:ss",
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .parse(dateTime, true)
        .toLocal();
  }

  static DateTime isoStringToLocalDate(BuildContext context, String dateTime) {
    return DateFormat(
            'yyyy-MM-dd hh:mm:ss',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .parse(dateTime, true)
        .toLocal();
  }

  static String localDateToIsoStringAMPM(
      BuildContext context, DateTime dateTime) {
    return DateFormat(
            'h:mm a | d-MMM-yyyy ',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime.toLocal());
  }

  static String isoStringToLocalTimeOnly(
      BuildContext context, String dateTime) {
    return DateFormat(
            'hh:mm a',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(isoStringToLocalDate(context, dateTime));
  }

  static String isoStringToLocalDateOnly(
      BuildContext context, String dateTime) {
    return DateFormat(
            'dd/MMM/yyyy',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(isoStringToLocalDate(context, dateTime));
  }

  static String localDateToIsoString(BuildContext context, DateTime dateTime) {
    return DateFormat(
            'yyyy-MM-dd hh:mm:ss',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime.toUtc());
  }

  static String isoStringToLocalDateAndTime(
      BuildContext context, String dateTime) {
    return DateFormat(
            'dd-MMM-yyyy hh:mm a',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(isoStringToLocalDate(context, dateTime));
  }

  static String timeAgo(BuildContext context, String dateTime) {
    Duration diff = DateTime.now()
        .difference(isoStringToLocalDate(context, dateTime).toUtc());
    if (diff.inDays > 365)
      return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? getTranslated("year",context) : getTranslated("years",context)}";
    if (diff.inDays > 30)
      return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? getTranslated("month",context) : getTranslated("months",context)}";
    if (diff.inDays > 7)
      return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? getTranslated("week",context) : getTranslated("weeks",context)}";
    if (diff.inDays > 0)
      return "${diff.inDays} ${diff.inDays == 1 ? getTranslated("day",context) : getTranslated("days",context)}";
    if (diff.inHours > 0)
      return "${diff.inHours} ${diff.inHours == 1 ? getTranslated("hour",context) : getTranslated("hours",context)}";
    if (diff.inMinutes > 0)
      return "${diff.inMinutes} ${diff.inMinutes == 1 ? getTranslated("minute",context) :getTranslated("minutes",context)}";
    return getTranslated("just now", context);
  }
}
