/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.eshopkh.app;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.eshopkh.app";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "1.0.2";
}
